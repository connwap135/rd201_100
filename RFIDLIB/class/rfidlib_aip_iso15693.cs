using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace RFIDLIB
{
    class rfidlib_aip_iso15693
    {
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern UInt32 ISO15693_GetLibVersion(StringBuilder buf, UInt32 nSize);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern UIntPtr ISO15693_CreateInvenParam(UIntPtr hInvenParamSpecList, Byte AntennaID, Byte en_afi, Byte afi, Byte slot_type);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int ISO15693_ParseTagDataReport(UIntPtr hTagReport, ref UInt32 aip_id, ref UInt32 tag_id, ref UInt32 ant_id, ref Byte dsfid, Byte[] uid);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int  ISO15693_Connect(UIntPtr hr ,UInt32 tagType,Byte address_mode,Byte[] uid ,ref UIntPtr ht ) ;
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int   ISO15693_ReadSingleBlock(
									UIntPtr hr ,
									UIntPtr ht, 
									Byte readSecSta,
								  UInt32 blkAddr,
						Byte[] bufBlockDat, 
						UInt32 nSize ,
						ref UInt32 bytesBlkDatRead)  ;
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int   ISO15693_WriteSingleBlock(
									UIntPtr hr ,
									UIntPtr ht,
								   UInt32 blkAddr,
								Byte[] newBlkData ,
								UInt32 bytesToWrite)  ;
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int   ISO15693_LockBlock(
									 UIntPtr hr ,
									UIntPtr ht, 
									UInt32 blkAddr)  ;
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int  ISO15693_ReadMultiBlocks( UIntPtr hr ,
									UIntPtr ht, 
									Byte readSecSta ,
									 UInt32 blkAddr,
							UInt32 numOfBlksToRead,
							ref UInt32 numOfBlksRead,
							Byte[] bufBlocks,
							UInt32 nSize ,
							ref UInt32 bytesBlkDatRead)  ;
         [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
         public static extern int  ISO15693_WriteMultipleBlocks(UIntPtr hr ,
									UIntPtr ht, 
									 UInt32 blkAddr,
							UInt32 numOfBlks, 
							Byte[] newBlksData,
							UInt32 bytesToWrite) ;
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int    ISO15693_WriteAFI(UIntPtr hr ,
									UIntPtr ht,
								Byte afi) ;
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int    ISO15693_LockAFI(UIntPtr hr ,
									UIntPtr ht ) ;
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int   ISO15693_WriteDSFID(UIntPtr hr ,
									UIntPtr ht, 
									Byte dsfid)  ;
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int  ISO15693_LockDSFID(UIntPtr hr ,
									UIntPtr ht)  ;
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int ISO15693_GetSystemInfo(UIntPtr hr ,
									UIntPtr ht,
									Byte[] uid,	/* out: tag uid */
											ref Byte dsfid, /* out:DSFID */
											ref Byte afi,  /* out:AFI */
											ref UInt32 blkSize ,/* out: block size */
											ref UInt32 numOfBloks, /* out:number of total blocks */
											ref Byte icRef /* out:ic reference */) ;
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int   ISO15693_GetBlockSecStatus(UIntPtr hr ,
									UIntPtr ht,
										UInt32 blkAddr, 
										UInt32 numOfBlks,
										Byte[] bufBlkSecs,
                                        UInt32 nSize/* in: size of the buffer */,
                                        ref UInt32 bytesSecRead /*out:number of block status byte copied*/ );

        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int NXPICODESLI_EableEAS(UIntPtr hr, UIntPtr ht);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int NXPICODESLI_DisableEAS(UIntPtr hr, UIntPtr ht);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int NXPICODESLI_LockEAS(UIntPtr hr, UIntPtr ht);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int NXPICODESLI_EASCheck(UIntPtr hr, UIntPtr ht, ref  Byte EASStatus);


        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int NXPICODESLIX_EableEAS(UIntPtr hr, UIntPtr ht);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int NXPICODESLIX_DisableEAS(UIntPtr hr, UIntPtr ht);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int NXPICODESLIX_LockEAS(UIntPtr hr, UIntPtr ht);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int NXPICODESLIX_EASCheck(UIntPtr hr, UIntPtr ht,ref Byte EASStatus);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int NXPICODESLIX_GetRandomNum(UIntPtr hr, UIntPtr ht, ref UInt16 random);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int NXPICODESLIX_SetPassword(UIntPtr hr, UIntPtr ht, Byte pwdNo, UInt32 xor_pwd);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int NXPICODESLIX_WritePassword(UIntPtr hr, UIntPtr ht, Byte pwdNo, UInt32 pwd);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int NXPICODESLIX_LockPassword(UIntPtr hr, UIntPtr ht, Byte pwdNo);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int NXPICODESLIX_PasswordProtect(UIntPtr hr, UIntPtr ht, Byte bandType/* EAS=0 or AFI=1 */);


        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int TIHFIPLUS_Write2Blocks(UIntPtr hr,
                            UIntPtr ht,
                                    UInt32 blkAddr,
                                    Byte[] newTwoBlksData,
                                    UInt32 bytesToWrite
                                    );
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int TIHFIPLUS_Lock2Blocks(UIntPtr hr,
                            UIntPtr ht,
                                    UInt32 blkAddr);

        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int STM24LR16E_UpdatePassword(UIntPtr hr,
                                    UIntPtr ht,
                                    Byte pwdnum,
                                    UInt32 old_pwd,
                                UInt32 new_pwd);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int STM24LR16E_AuthPassword(UIntPtr hr,
                                    UIntPtr ht,
                                    Byte pwdnum,
                                    UInt32 pwd);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int STM24LR16E_ActivateSectorSecurity(UIntPtr hr,
                                    UIntPtr ht,
                                Byte sector_num,
                      Byte access_type,
                      Byte pwd_num_protect);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int STM24LR16E_ReadCFGByte(UIntPtr hr,
                                    UIntPtr ht,
                                  ref Byte cfgby);

        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int STM24LR16E_SetCFGEH(UIntPtr hr,
                                    UIntPtr ht,
                                Byte EnergyHarvesting,
                      Byte EHMode);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int STM24LR16E_SetCFGDO(UIntPtr hr,
                                    UIntPtr ht,
                                Byte cfg_do /* 0: RF busy mode(RF WIP/BUSY pin for RF busy) , 1: RF Write in progress( RF WIP/BUSY pin for) */
                                );
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int STM24LR16E_SetCtrlRegEHEN(UIntPtr hr,
                                    UIntPtr ht,
                                Byte enable
                                );
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int STM24LR16E_ReadCtrlReg(UIntPtr hr,
                                    UIntPtr ht,
                                    ref Byte ctrlreg);

        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int STM24LR64_UpdatePassword(UIntPtr hr,
                                    UIntPtr ht,
                                    Byte pwdnum,
                                    UInt32 old_pwd,
                                UInt32 new_pwd);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int STM24LR64_AuthPassword(UIntPtr hr,
                                    UIntPtr ht,
                                    Byte pwdnum,
                                    UInt32 pwd);
        [DllImport("rfidlib_aip_iso15693.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int STM24LR64_ActivateSectorSecurity(UIntPtr hr,
                                    UIntPtr ht,
                                Byte sector_num,
                      Byte access_type,
                      Byte pwd_num_protect);
    }
}
