using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace RFIDLIB
{
    class rfidlib_data_node
    {
         [DllImport("rfidlib_data_node.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
         public static extern UInt32 DNODE_GetLibVersion(StringBuilder buf, UInt32 nSize );
         [DllImport("rfidlib_data_node.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
         public static extern int DNODE_Destroy(UIntPtr dn);
    }
}
