using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;


namespace RFIDLIB
{
    
    class rfidlib_reader
    {
        
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern  uint  RDR_GetLibVersion(StringBuilder buf ,uint nSize )  ;
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_LoadReaderDrivers(string drvpath);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern uint RDR_GetLoadedReaderDriverCount();
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_GetLoadedReaderDriverOpt(uint idx, string option, StringBuilder valueBuffer, ref uint nSize);

        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern uint HID_Enum(string DevId);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int HID_GetEnumItem(uint indx, byte infType, StringBuilder infBuffer, ref uint nSize);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern uint  COMPort_Enum() ;
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int  COMPort_GetEnumItem(uint idx,StringBuilder nameBuf,uint nSize ) ;
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int  RDR_Open(string connStr ,ref UIntPtr hrOut) ;
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern  int RDR_Close(UIntPtr hr)  ;
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern UIntPtr RDR_CreateInvenParamSpecList();  
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_TagInventory(UIntPtr hr, byte AIType, byte AntennaCoun, byte[] AntennaIDs, UIntPtr InvenParamSpecList);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern uint RDR_GetTagDataReportCount(UIntPtr hr);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern UIntPtr RDR_GetTagDataReport(UIntPtr hr, byte seek);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_EnableAsyncTagReportOutput(UIntPtr hr, byte type, uint msg, UIntPtr hwnd, [In] RFIDLIB_EVENT_CALLBACK cb);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_DisableAsyncTagReportOutput(UIntPtr hr);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_TagDisconnect(UIntPtr hr,UIntPtr hTag);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_SetAcessAntenna(UIntPtr hr ,byte AntennaID) ;
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_OpenRFTransmitter(UIntPtr hr) ;
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_CloseRFTransmitter(UIntPtr hr);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_SetCommuImmeTimeout(UIntPtr hr);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_ResetCommuImmeTimeout(UIntPtr hr);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern uint RDR_GetAntennaInterfaceCount(UIntPtr hr);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_GetReaderInfor(UIntPtr hr,
									 byte Type ,
									 StringBuilder buffer,
									ref uint nSize) ;

        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int  RDR_ExeSpecialControlCmd(UIntPtr hr,string cmd ,string parameters,StringBuilder result,ref uint nSize) ;
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_ConfigBlockWrite(UIntPtr hr,uint cfgno ,byte[] cfgdata,uint nSize,uint mask) ;
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_ConfigBlockRead(UIntPtr hr, uint cfgno, byte[] cfgbuff, uint nSize);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_ConfigBlockSave(UIntPtr hr, uint cfgno);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_CreateRS485Node(UIntPtr hr, uint busAddr, ref UIntPtr ohrRS485Node);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_GetSupportedAirInterfaceProtocol(UIntPtr hr, uint index, ref uint AIPType);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_GetAirInterfaceProtName(UIntPtr hr, uint AIPType, StringBuilder namebuf, uint nSize);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_GetOutputCount(UIntPtr hreader, ref byte bcount);
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_GetOutputName(UIntPtr hreader, byte idxOut, StringBuilder bufName, ref uint nSize);

        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern UIntPtr RDR_CreateSetOutputOperations();
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_AddOneOutputOperation(UIntPtr hOutputOperations, byte outNo, byte outMode, uint outFrequency, uint outActiveDuration, uint outInactiveDuration);
 
        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_SetOutput(UIntPtr hreader, UIntPtr hOutputOperations);

        [DllImport("rfidlib_reader.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int RDR_DisconnectAllTags(UIntPtr hreader);

    }
}
