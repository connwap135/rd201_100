﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
namespace TabsApp
{
   public static class JsonClass
    {
        public static T DeserrializeJsonToObject<T>(string json) where T : class
        {
            try
            {
                JsonSerializer serializer = new JsonSerializer();
                StringReader reader = new StringReader(json);
                return serializer.Deserialize(new JsonTextReader(reader), typeof(T)) as T;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public static string SerializeObjectToJson(object obj)
        {
            var json = string.Empty;
            try
            {
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                json = JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.None, settings);
            }
            catch (Exception)
            {
                json = null;
            }
            return json;
        }
    }
}
