﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Text;

namespace TabsApp.form
{
    public partial class DesktopIcon : UserControl
    {
        private int Softness = 2;
        private int ShadowDepth = 4;
        private int Direction = 315;
        private int Opacity = 100;
        private Color Color=Color.Black;

        public Image Image { get
            {
                return pic.Image;
            }
            set
            {
              pic.Image = value;

            }
        }
        public string Caption
        {
            get
            {
                return lblCaption.Text;
            }
            set
            {
                lblCaption.Text = value;
            }
        }
        public Color CaptionForeColor
        {
            get
            {
                return lblCaption.ForeColor;
            }
            set
            {
                lblCaption.ForeColor = value;
            }
        }
        public Color CaptionBackColor
        {
            get
            {
                return lblCaption.BackColor;
            }
            set
            {
                lblCaption.BackColor = value;
            }
        }
        public DesktopIcon()
        {
            InitializeComponent();
            pic.BackColor = Color.Transparent;
            lblCaption.BackColor = Color.Transparent;
        }
        private GraphicsPath GetRoundedRectPath(Rectangle rect,int radius)
        {
            int diameter = radius;
            Rectangle arcRect = new Rectangle(rect.Location, new Size(diameter,diameter));
            GraphicsPath path = new GraphicsPath();
            path.AddArc(arcRect, 180, 90);
            arcRect.X = rect.Right - diameter;
            path.AddArc(arcRect, 270, 90);
            arcRect.Y = rect.Bottom - diameter;
            path.AddArc(arcRect, 0, 90);
            arcRect.X = rect.Left;
            path.AddArc(arcRect, 90, 90);
            path.CloseFigure();
            return path;
        }

        private void DesktopIcon_Paint(object sender, PaintEventArgs e)
        {
            //var FormPath = new GraphicsPath();
            //Rectangle rect = new Rectangle(0, 0, this.Width, this.Height);
            //FormPath = GetRoundedRectPath(rect,1);
            //this.Region = new Region(FormPath);
        }

        private void pic_DoubleClick(object sender, EventArgs e)
        {
            this.OnDoubleClick(e);
        }

        private void pic_MouseClick(object sender, MouseEventArgs e)
        {
            CloseRenameBox();
            var e1 = new MouseEventArgs(e.Button,e.Clicks,e.X + (sender as Control).Left,e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseClick(e1);
        }

        private void pic_MouseDown(object sender, MouseEventArgs e)
        {
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseDown(e1);
        }

        private void pic_MouseMove(object sender, MouseEventArgs e)
        {
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseMove(e1);
        }

        private void pic_MouseUp(object sender, MouseEventArgs e)
        {
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseUp(e1);
        }

        private void lblCaption_DoubleClick(object sender, EventArgs e)
        {
            this.OnDoubleClick(e);
        }

        private void lblCaption_MouseClick(object sender, MouseEventArgs e)
        {
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseClick(e1);
        }

        private void lblCaption_MouseDown(object sender, MouseEventArgs e)
        {
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseDown(e1);
        }

        private void lblCaption_MouseMove(object sender, MouseEventArgs e)
        {
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseMove(e1);
        }

        private void lblCaption_MouseUp(object sender, MouseEventArgs e)
        {
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseUp(e1);
        }

        private void lblCaption_Paint(object sender, PaintEventArgs e)
        {
            Graphics screenGraphics = e.Graphics;
            Bitmap shadowBitmap = new Bitmap(Math.Max(Convert.ToInt32((Convert.ToSingle(this.lblCaption.Width) / this.Softness)), 1), Math.Max(Convert.ToInt32((Convert.ToSingle(this.lblCaption.Height) / this.Softness)), 1));
            StringFormat sf = StringFormat.GenericTypographic;
            switch (this.lblCaption.TextAlign)
            {
                 case ContentAlignment.TopLeft:
                    sf.Alignment = StringAlignment.Near;
                    sf.LineAlignment = StringAlignment.Near;
                    break;
                case ContentAlignment.TopCenter:
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Near;
                    break;
                case ContentAlignment.TopRight:
                    sf.Alignment = StringAlignment.Far;
                    sf.LineAlignment = StringAlignment.Near;
                    break;
                case ContentAlignment.MiddleLeft:
                    sf.Alignment = StringAlignment.Near;
                    sf.LineAlignment = StringAlignment.Center;
                    break;
                case ContentAlignment.MiddleCenter:
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    break;
                case ContentAlignment.MiddleRight:
                    sf.Alignment = StringAlignment.Far;
                    sf.LineAlignment = StringAlignment.Center;
                    break;
                case ContentAlignment.BottomLeft:
                    sf.Alignment = StringAlignment.Near;
                    sf.LineAlignment = StringAlignment.Far;
                    break;
                case ContentAlignment.BottomCenter:
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Far;
                    break;
                case ContentAlignment.BottomRight:
                    sf.Alignment = StringAlignment.Far;
                    sf.LineAlignment = StringAlignment.Far;
                    break;
                default:
                    break;
            }
            using (Graphics imageGraphics = Graphics.FromImage(shadowBitmap))
            {
                imageGraphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
                Matrix transformMatrix = new Matrix();
                transformMatrix.Scale((1f / this.Softness), (1f / this.Softness));
                transformMatrix.Translate(Convert.ToSingle((this.ShadowDepth * Math.Cos(Convert.ToDouble(this.Direction)))), Convert.ToSingle((this.ShadowDepth * Math.Sin(Convert.ToDouble(this.Direction)))));
                imageGraphics.Transform = transformMatrix;
                imageGraphics.DrawString(this.lblCaption.Text, this.lblCaption.Font, new SolidBrush(Color.FromArgb(this.Opacity, this.Color)), this.lblCaption.DisplayRectangle, sf);
            }

            screenGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            screenGraphics.DrawImage(shadowBitmap, this.lblCaption.ClientRectangle, 0, 0, shadowBitmap.Width, shadowBitmap.Height, GraphicsUnit.Pixel);
            //screenGraphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            //screenGraphics.DrawString(this.lblCaption.Text, this.lblCaption.Font, new SolidBrush(this.lblCaption.ForeColor), this.lblCaption.DisplayRectangle, sf);
        }

        public void ShowRenameBox(string title)
        {
            lblCaption.Visible = false;
            TextBox tb = new TextBox();
            tb.Location = new Point(6,55);
            tb.Name = "tb";
            tb.Text = title;
            //tb.Multiline = true;
            tb.Size = new Size(82, 28);
            tb.TabIndex = 0;
            Controls.Add(tb);
            tb.KeyDown += tb_KeyDown;
            tb.Focus();
            tb.SelectAll();
        }

        /// <summary>
        /// 关闭重命名
        /// </summary>
        public void CloseRenameBox()
        {
            foreach (Control item in Controls)
            {
                if (item.Name.Equals("tb"))
                {
                    lblCaption.Text = item.Text;
                    lblCaption.Visible = true;
                    Controls.Remove(item);
                    break;
                }
            }
        }

        private void tb_KeyDown(object sender, KeyEventArgs e)
        {
                if (e.KeyCode == Keys.Enter)
                {
                CloseRenameBox();
                }
        }

        private void DesktopIcon_Click(object sender, EventArgs e)
        {
            CloseRenameBox();
        }

        private void DesktopIcon_Leave(object sender, EventArgs e)
        {
            CloseRenameBox();
        }
    }
}
