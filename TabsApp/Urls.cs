﻿namespace TabsApp
{
    public class Urls
    {
        private static readonly string HTTP = "http://";
        private static readonly string HTTPS = "https://";
        private static readonly string URL_UNDERLINE = "_";
        private static readonly string URL_SPLITTER = "/";
        private static readonly string HOST = "192.168.1.240:5000";
        public static readonly string HOST_IP = "192.168.1.240";
        public static readonly string URL_API_HOST = HTTP + HOST + URL_SPLITTER;
        public static readonly string API_PSAPI = URL_API_HOST + "api/psapi";
        public static readonly string API_PDAPI = URL_API_HOST + "API/PDAPI";
        public static readonly string API_LOGIN = URL_API_HOST + "ES/GET";
        public static readonly string API_ORDER = URL_API_HOST + "WO/GET";
        public static readonly string API_PSDETAILS = URL_API_HOST + "ps/GetDetails";
        public static readonly string API_SCAN_RECORD = URL_API_HOST + "PD/GET";
        public static readonly string API_SOFT_UPDATE = "http://192.168.1.238:88/wxproject/zhifuappxml.aspx?id=8";
     
    }
}
