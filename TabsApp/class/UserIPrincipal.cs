﻿using System.Collections.Generic;
using System.Security.Principal;

namespace TabsApp
{
    /// <summary>
    /// 用户对象
    /// </summary>
    public class UserIPrincipal : IPrincipal
    {
        private UserIIdentity identityValue;
        public List<string> PermissionList { get; }

        /// <summary>
        /// 用户对象
        /// </summary>
        public UserIPrincipal(string name, string password)
        {
            identityValue = new UserIIdentity(name, password);
        }

        public UserIPrincipal()
        {
        }

        /// <summary>
        /// 获取当前用户的标识
        /// </summary>
        public IIdentity Identity => identityValue;

        /// <summary>
        /// 确定当前用户是否属于指定的角色
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public bool IsInRole(string role)
        {
            return identityValue.Role.ToString().Equals(role);
        }
    }
}
