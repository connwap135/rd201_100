﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;

namespace TabsApp
{
    class UserIIdentity : IIdentity
    {
        public UserIIdentity(string username,string password)
        {
            if(IsValidNameAndPassword(username, password))
            {
                Name = getUserName(username);
                IsAuthenticated = true;
                AuthenticationType = getAuthenticatedType(username);
                Role = getRole(username);
            }
            else
            {
                Name = null;
                IsAuthenticated = false;
                AuthenticationType = null;
                Role = WindowsBuiltInRole.Guest;
            }
        }

        private WindowsBuiltInRole getRole(string name)
        {
            return WindowsBuiltInRole.Administrator;
        }

        private string getUserName(string username)
        {
            return username;
        }

        private string getAuthenticatedType(string name)
        {
            return "admin";
        }

        private bool IsValidNameAndPassword(string name, string password)
        {
            List<User> list = new List<User>();
            list.Add(new User() {Id=1, Name="admin", PassWord="12345"});
            var result= list.Where(x=> x.Name.Equals("admin")).Where(x => x.PassWord.Equals("12345")).FirstOrDefault();
            if (result == null) { return false; }
            return true;
        }

        /// <summary>
        /// 获取当前用户的名称
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// 获取所使用的身份验证的类型。
        /// </summary>
        public string AuthenticationType { get; }

        /// <summary>
        /// 是否验证了用户
        /// </summary>
        public bool IsAuthenticated { get; }

        public WindowsBuiltInRole Role { get; }
    }
}
