﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace TabsApp.form
{
    
    public partial class MDIMain : Form
    {
        public UserIPrincipal User { get; set; }
        public MDIMain()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            UserIPrincipal user = new UserIPrincipal("admin", "12345");
            if (user.Identity.IsAuthenticated)
            {
               this.User = user;
                Form childForm = new Form();
                childForm.MdiParent = this;
                childForm.Text = "窗口 " ;
                childForm.Show();
            }
           
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "文本文件(*.txt)|*.txt|所有文件(*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "文本文件(*.txt)|*.txt|所有文件(*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void MDIMain_Load(object sender, EventArgs e)
        {
            //var childForm = new Desktop
            //{
            //    MdiParent = this
            //};


            //List<DesktopIconInfo> list = new List<DesktopIconInfo>();

            //for (int i = 0; i < 35; i++)
            //{
            //    DesktopIconInfo item = new DesktopIconInfo();
            //    item.cmd = "exec:ddfsd" + i;
            //    item.title = "标题标题" + i;
            //    item.left = 10 * i;
            //    item.top = i * 90;
            //    list.Add(item);
            //}

            //childForm.DataSource = list;
            //childForm.Show();

            Thread InvenThread = new Thread(AddIcon_Invoke);
            InvenThread.Start();
        }

   

        private delegate void MyInvoke();

        private void AddIcon_Invoke()
        {
            var myinvoke = new MyInvoke(AddIcon);
            Invoke(myinvoke);
        }

        private void AddIcon()
        {
            var childForm = new Desktop
            {
                MdiParent = this
            };
            childForm.Show();
            childForm.DesktopEnterEvent += dfeww4r;
            for (int i = 0; i < 12; i++)
            {
                childForm.AddDesktopIcon("sfdsf" + i, "dfsfdds" + i);
            }
            childForm.AddDesktopIcon("登录", "LoginForm");
            ToolStripMenuItem md_newoptItem = new ToolStripMenuItem();
            md_newoptItem.Name = "mm_res";
            md_newoptItem.Text = "重置桌面";
            md_newoptItem.Click += optionMenu_Click;

            childForm.DesktopMenuAdd(md_newoptItem);
            
        }

        private void optionMenu_Click(object sender, EventArgs e)
        {
            MessageBox.Show("hello");
        }

        private void dfeww4r(object sender, DesktopIconEventArgs e)
        {
            
            if (User.Identity.IsAuthenticated)
            {
                switch (e.cmd)
                {
                    case "dfsfdds1":
                        Form frm = new Form();
                        frm.MdiParent = this;
                        frm.Show();
                        break;
                    default:
                        break;
                }
                //MessageBox.Show(e.cmd);


                string formName = "TabsApp." + e.cmd;
                System.Reflection.Assembly assmbly = System.Reflection.Assembly.GetExecutingAssembly();
                if (!(assmbly.CreateInstance(formName) is Form form)) { return; }
                foreach (Form openForm in MdiChildren) //仅止FORM多开，相同的仅打开一个
                {
                    if (openForm.Name == form.Name)
                    {
                        openForm.Activate();
                        form.Dispose();
                        return;
                    }
                }

                form.MdiParent = this;
                form.Show();
            }
        }

        private void MDIMain_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void helpToolStripButton_Click(object sender, EventArgs e)
        {

        }

       
    }
}
