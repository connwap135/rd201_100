﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace XKControlLib.Desktop
{

    public partial class DesktopIcon : UserControl
    {
        private ToolTip toolTip1 = new ToolTip();
        /// <summary>
        /// 图标
        /// </summary>
        [Category("杂项")]
        [Description("图标")]
        public Image Image { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        [Category("杂项")]
        [Description("标题")]
        public string Caption { get; set; }

        /// <summary>
        /// 前景色
        /// </summary>
        [Category("杂项")]
        [Description("前景色")]
        public Color CaptionForeColor { get; set; }
        /// <summary>
        /// 背景色
        /// </summary>
        [Category("杂项")]
        [Description("背景色")]
        public Color CaptionBackColor { get; set; }

        public DesktopIcon()
        {
            InitializeComponent();
            toolTip1.InitialDelay = 2000;
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.UpdateStyles();
        }

        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        var cp = base.CreateParams;
        //        cp.ExStyle = 0x20;
        //        return cp;
        //    }
        //}

        private void pic_DoubleClick(object sender, EventArgs e)
        {
            this.OnDoubleClick(e);
        }

        private void pic_MouseClick(object sender, MouseEventArgs e)
        {
            CloseReNameBox();
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseClick(e1);
        }

        private void pic_MouseDown(object sender, MouseEventArgs e)
        {
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseDown(e1);
        }

        private void pic_MouseMove(object sender, MouseEventArgs e)
        {
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseMove(e1);
        }

        private void pic_MouseUp(object sender, MouseEventArgs e)
        {
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseUp(e1);
        }

        private void lblCaption_DoubleClick(object sender, EventArgs e)
        {
            this.OnDoubleClick(e);
        }

        private void lblCaption_MouseClick(object sender, MouseEventArgs e)
        {
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseClick(e1);
        }

        private void lblCaption_MouseDown(object sender, MouseEventArgs e)
        {
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseDown(e1);
        }

        private void lblCaption_MouseMove(object sender, MouseEventArgs e)
        {
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseMove(e1);
        }

        private void lblCaption_MouseUp(object sender, MouseEventArgs e)
        {
            var e1 = new MouseEventArgs(e.Button, e.Clicks, e.X + (sender as Control).Left, e.Y + (sender as Control).Top, e.Delta);
            this.OnMouseUp(e1);
        }

        private readonly string LabelClassName = "ReName_Class_Str";
        public void ShowReNameBox(string title)
        {
            //lblCaption.Visible = false;
            var tb = new TextBox
            {
                Location = new Point(6, 54),
                Name = LabelClassName,
                Text = title,
                Size = new Size(82, 36),
                TabIndex = 0
            };
            tb.Multiline = true;
            tb.Dock = DockStyle.Bottom;
            Controls.Add(tb);
            tb.KeyDown += Tb_KeyDown;
            tb.Focus();
            tb.SelectAll();
        }

        /// <summary>
        /// 关闭重命名
        /// </summary>
        public void CloseReNameBox()
        {
            foreach (Control item in Controls)
            {
                if (item.Name.Equals(LabelClassName))
                {
                    Caption = item.Text;
                    //lblCaption.Visible = true;
                    Controls.Remove(item);
                    return;
                }
            }
        }

        private void Tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CloseReNameBox();
            }
        }

        private void DesktopIcon_Click(object sender, EventArgs e)
        {
            CloseReNameBox();
        }

        private void DesktopIcon_Leave(object sender, EventArgs e)
        {
            CloseReNameBox();
        }

        private void pic_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void lblCaption_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {

            Rectangle rectCaption = new Rectangle(6, 55, 82, 28);
            Rectangle rectImage = new Rectangle(24, 6, 48, 48);
            Graphics draw = e.Graphics;
            Font font = new Font("宋体", 9.0f);
            var StrFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };
            Brush brush = new SolidBrush(CaptionForeColor);
            draw.FillRectangle(new SolidBrush(CaptionBackColor), draw.ClipBounds);
            var _caption = this.Caption ?? "";
            var _len = _caption.Length;
            var _size = draw.MeasureString(_caption, font, rectCaption.Location, StrFormat);
            while (_size.Width > rectCaption.Width * 2)//2行文字,多余省略
            {
                _len--;
                _caption = _caption.Substring(0, _len) + "...";
                _size = draw.MeasureString(_caption, font, rectCaption.Location, StrFormat);
            }
            toolTip1.SetToolTip(this, Caption);
            draw.DrawString(_caption, font, brush, rectCaption, StrFormat);
            if (Image != null)
            {
                draw.DrawImage(Image, rectImage);
            }

            base.OnPaint(e);
        }
    }
}
