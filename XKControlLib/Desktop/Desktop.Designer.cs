﻿namespace XKControlLib.Desktop
{
    partial class Desktop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Desktop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "Desktop";
            this.Text = "工作台 ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Desktop_FormClosing);
            this.Load += new System.EventHandler(this.Desktop_Load);
            this.Click += new System.EventHandler(this.Desktop_Click);
            this.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.Desktop_ControlAdded);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Desktop_Paint);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Desktop_MouseClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Desktop_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Desktop_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Desktop_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion
    }
}