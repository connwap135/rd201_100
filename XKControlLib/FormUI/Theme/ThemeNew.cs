﻿using System;
using System.Drawing;
using System.Windows.Forms.MyGraphics;

namespace System.Windows.Forms
{
    public class ThemeNew : ThemeBase
    {
        public ThemeNew()
            : base()
        {
            // about theme
            ThemeName = "A New Look Theme";

            UseDefaultTopRoundingFormRegion = false;
            BorderWidth = 2;
            CaptionHeight = 36;
            IconSize = new Size(24, 24);
            ControlBoxOffset = new Point(6, 9);
            ControlBoxSpace = 2;
            MaxBoxSize = MinBoxSize = CloseBoxSize = new Size(32, 18);
            SideResizeWidth = 4;

            CaptionBackColorBottom = Color.LightSlateGray;
            CaptionBackColorTop = ColorHelper.GetLighterColor(CaptionBackColorBottom, 40);
            
            RoundedStyle = RoundStyle.None;

            FormBorderOutterColor = Color.Black;
            FormBorderInnerColor = Color.FromArgb(200, Color.White);
            SetClientInset = false;
            //自己改的
            CloseBoxColor = SelfRenderControl.ButtonColorTable.GetColorTableVs2013Theme();
            MaxBoxColor = MinBoxColor = CloseBoxColor;
        }
    }

    public class ThemeDevExpress : ThemeBase
    {
        public ThemeDevExpress()
            : base()
        {
            ThemeName = "DevExpress Default";

            BorderWidth = 2;
            CaptionHeight = 30;
            IconSize = new Size(16, 16);
            ControlBoxOffset = new Point(8, 8);
            ControlBoxSpace = 2;            
            SideResizeWidth = 4;
            UseDefaultTopRoundingFormRegion = false;

            CaptionBackColorBottom = Color.White;
            CaptionBackColorTop = Color.White;
            
            RoundedStyle = RoundStyle.None;

            FormBorderOutterColor = Color.FromArgb(0, 144, 198);
            FormBorderInnerColor = Color.White;
            SetClientInset = false;

            CaptionTextCenter = false;
            CaptionTextColor = Color.FromArgb(102, 102, 102);
            FormBackColor = Color.White;
            //下面自己改的
            CloseBoxColor = SelfRenderControl.ButtonColorTable.GetColorTableVs2013Theme();
            MaxBoxColor = MinBoxColor = CloseBoxColor;
        }
    }

    public class ThemeVS2013 : ThemeBase
    {
        public ThemeVS2013()
            : base()
        {
            ThemeName = "A VS2013 Look";

            BorderWidth = 1;
            CaptionHeight = 38;
            IconSize = new Size(22, 22);
            CloseBoxSize = MaxBoxSize = MinBoxSize = new Size(34, 27);
            ControlBoxOffset = new Point(1, 1);
            ControlBoxSpace = 1;
            SideResizeWidth = 6;
            UseDefaultTopRoundingFormRegion = false;

            CaptionBackColorBottom = CaptionBackColorTop = Color.FromArgb(214, 219, 233);
            
            RoundedStyle = RoundStyle.None;

            FormBorderOutterColor = Color.FromArgb(0, 0, 0);            
            SetClientInset = false;
            
            CaptionTextColor = Color.FromArgb(0, 0, 0);
            FormBackColor = Color.FromArgb(42, 58, 86);

            CloseBoxColor = SelfRenderControl.ButtonColorTable.GetColorTableVs2013Theme();
            MaxBoxColor = MinBoxColor = CloseBoxColor;            
        }
    }
}
