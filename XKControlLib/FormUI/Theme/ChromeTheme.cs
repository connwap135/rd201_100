﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms.MyGraphics;
using System.Windows.Forms.SelfRenderControl;

namespace System.Windows.Forms
{
   public class ChromeTheme: ThemeBase
    {
        public ChromeTheme()
          : base()
        {
            base.ThemeName = "chrome_theme";

            base.BorderWidth = 2;
            base.SideResizeWidth = 4;
            base.IconLeftMargin =10;
            base.FormBorderOutterColor = Color.FromArgb(89, 89, 89);
            base.FormBorderInnerColor = Color.FromArgb(60, 255, 255, 255);
            base.FormBorderInmostColor = Color.Transparent;
            base.DrawCaptionIcon = false;
            base.DrawCaptionText = false;
            //base.UseDefaultTopRoundingFormRegion = false;

            base.RoundedStyle = RoundStyle.Top;
            base.Radius = 8;
            base.SetClientInset = false;
            base.FormBackColor = Color.FromArgb(66, 133, 201);

            base.CaptionBackColorTop = Color.FromArgb(97, 146, 211);
            base.CaptionBackColorBottom = Color.FromArgb(66, 133, 201);
            base.CaptionHeight = 38;

            base.CloseBoxSize = new Size(44, 18);
            base.MinBoxSize = base.MaxBoxSize = new Size(26, 18);
            base.ControlBoxOffset = new Point(4, 1);
            base.ControlBoxSpace = -1;

            //base.CloseBoxBackImageNormal = Properties.Resources.close_normal;
            //base.CloseBoxBackImageHover = Properties.Resources.close_hover;
            //base.CloseBoxBackImagePressed = Properties.Resources.close_pressed;

            //base.MinBoxBackImageNormal = Properties.Resources.min_normal;
            //base.MinBoxBackImageHover = Properties.Resources.min_hover;
            //base.MinBoxBackImagePressed = Properties.Resources.min_pressed;

            //base.MaxBoxBackImageNormal = Properties.Resources.max_normal;
            //base.MaxBoxBackImageHover = Properties.Resources.max_hover;
            //base.MaxBoxBackImagePressed = Properties.Resources.max_pressed;

            //base.ResBoxBackImageNormal = Properties.Resources.restore_normal;
            //base.ResBoxBackImageHover = Properties.Resources.restore_hover;
            //base.ResBoxBackImagePressed = Properties.Resources.restore_pressed;

            base.Mdi_ShowNewTabBtn = false;
            base.Mdi_TabActiveBackColorTop = Color.White;
            base.Mdi_TabActiveBackColorBottom = Color.FromArgb(248, 248, 248);
            base.Mdi_TabDeactiveBackColorTop = Color.FromArgb(163, 200, 244);
            base.Mdi_TabDeactiveBackColorBottom = Color.FromArgb(155, 193, 240);

            base.Mdi_BarBottomRegionBackColor = Color.FromArgb(246, 246, 246);
            base.Mdi_TabInnerBorderColor = Color.FromArgb(60, Color.White);
            base.Mdi_TabOutterBorderColor = Color.FromArgb(109, 136, 169);
            base.Mdi_TabTopSpace = 0;
            base.Mdi_BarMargin = new Padding(0, 15, 0, 0);

            base.Mdi_ListAllBtnAlign = BarButtonAlignmentType.AfterLastTab;
            base.Mdi_AlwaysShowListAllBtn = false;
            base.Mdi_BarBottomRegionHeight = 0;
            base.Mdi_NewTabBtnBottomSpace = base.Mdi_BarBottomRegionHeight + 4;
            base.Mdi_ListAllBtnBottomSpace = base.Mdi_NewTabBtnBottomSpace;
            base.Mdi_UseMsgToActivateChild = true;


            // new-tab-btn color, list-all-btn color
            var table = new ButtonColorTable
            {
                BackColorNormal = Color.FromArgb(159, 196, 243),
                BackColorHover = Color.FromArgb(182, 210, 246),
                BackColorPressed = Color.FromArgb(131, 158, 201)//(97, 132, 185);
            };
            table.ForeColorNormal = table.ForeColorHover = ColorHelper.GetLighterColor(Color.Black, 30);
            table.ForeColorPressed = ColorHelper.GetLighterColor(Color.Black, 80);
            base.Mdi_NewTabBtnColor = table;
            base.Mdi_ListAllBtnColor = table;
        }
    }
}
