﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XKControlLib.DesignControl
{
    public abstract class DesignerBase
    {
        protected Control _ParentObject;
        protected Control _SelectedObject;
        protected ConCollection _SelectedObjects = new ConCollection();
        public virtual Control ParentObject { get; set; }
        public Control SelectedObject { get; set; }
        public ConCollection SelectedObjects =>_SelectedObjects;

        public void AlignLeft()
        {
            if(SelectedObject is null) { return; }
            foreach (Control item in SelectedObjects)
            {
                item.Left = SelectedObject.Left;
            }
        }

        public void AlignRight()
        {
            if (SelectedObject is null) { return; }
            foreach (Control item in SelectedObjects)
            {
                item.Left = SelectedObject.Left + SelectedObject.Width - item.Width;
            }
        }

        public void AlignTop()
        {
            if (SelectedObject is null) { return; }
            foreach (Control item in SelectedObjects)
            {
                item.Top = SelectedObject.Top;
            }
        }

        public void AlignBottom()
        {
            if (SelectedObject is null) { return; }
            foreach (Control item in SelectedObjects)
            {
                item.Top = SelectedObject.Top + SelectedObject.Height - item.Height;
            }
        }

        public void OffsetLeft(int value)
        {
            foreach (Control item in SelectedObjects)
            {
                item.Left += value;
            }
        }
         
        public void OffsetTop(int value)
        {
            foreach (Control item in SelectedObjects)
            {
                item.Top += value;
            }
        }

        public void  OffsetWidth(int value)
        {
            foreach (Control item in SelectedObjects)
            {
                item.Width += value;
            }
        }

        public void OffsetHeight(int value)
        {
            foreach (Control item in SelectedObjects)
            {
                item.Height += value;
            }
        }

        public void SetFont(Font font)
        {
            foreach (Control item in SelectedObjects)
            {
                item.Font = font;
                foreach (Control con in item.Controls)
                {
                    con.Font = font;
                }
            }
        }

        public void SelectedNo()
        {
            this.SelectedObject = null;
            this.SelectedObjects.Clear();
            SignChooseControls();
        }

        public abstract void SignChooseControls(); 

        public void DesignByKeyDown(KeyEventArgs e)
        {
            int OffsetInterval_Max = 10;
            int OffsetInterval_Min = 1;
            switch (e.KeyCode)
            {
                case Keys.Up:
                    if (e.Control)
                    {
                        OffsetTop(0 - OffsetInterval_Min);
                    }else if (!e.Shift)
                    {
                        OffsetTop(0 - OffsetInterval_Max);
                    }
                    else if (e.Shift)
                    {
                        OffsetHeight(0 - OffsetInterval_Min);
                    }
                    SignChooseControls();
                    break;
                case Keys.Down:
                    if (e.Control)
                    {
                        OffsetTop(OffsetInterval_Min);
                    }
                    else if (!e.Shift)
                    {
                        OffsetTop(OffsetInterval_Max);
                    }
                    else if (e.Shift)
                    {
                        OffsetHeight(OffsetInterval_Min);
                    }
                    SignChooseControls();
                    break;
                case Keys.Left:
                    if (e.Control)
                    {
                        OffsetLeft(0 - OffsetInterval_Min);
                    }
                    else if (!e.Shift)
                    {
                        OffsetLeft(0 - OffsetInterval_Max);
                    }
                    else if (e.Shift)
                    {
                        OffsetWidth(0 - OffsetInterval_Min);
                    }
                    SignChooseControls();
                    break;
                case Keys.Right:
                    if (e.Control)
                    {
                        OffsetLeft(OffsetInterval_Min);
                    }
                    else if (!e.Shift)
                    {
                        OffsetLeft(OffsetInterval_Max);
                    }
                    else if (e.Shift)
                    {
                        OffsetWidth(OffsetInterval_Min);
                    }
                    SignChooseControls();
                    break;
                case Keys.A:
                    if (e.Control)
                    {
                        SelectedNo();
                        foreach (Control con in ParentObject.Controls)
                        {
                            if (con.Visible)
                            {
                                SelectedObjects.Add(con);
                            }
                        }
                        if (SelectedObjects.ExistItem())
                        {
                            SelectedObject = this.SelectedObjects[0];
                        }
                    }
                    SignChooseControls();
                    break;
                default:
                    break;
            }
        }
    }

    public class ConCollection: System.Collections.ObjectModel.Collection<Control>
    {
        public bool ExistItem()
        {
            if (this.Count == 0) { return false; }
            return true;
        }
    }
}
