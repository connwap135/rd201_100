﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static System.Windows.Forms.ProToolstripRenderer;

namespace XKControlLib
{
    public partial class EditText : TextBox
    {
        public EditText() : base()
        {
            BorderStyle = BorderStyle.FixedSingle;
            SetStyles();
        }
        private void SetStyles()
        {
            base.SetStyle(
                ControlStyles.UserPaint |
                ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.ResizeRedraw |
                ControlStyles.SupportsTransparentBackColor, true);
            base.UpdateStyles();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (IsEnter)
            {
                DrawTextBoxSelected(e.Graphics, e.ClipRectangle);
                return;
            }
            DrawTextBoxUnselected(e.Graphics, e.ClipRectangle);
            base.OnPaint(e);
        }

        public void DrawTextBoxSelected(Graphics g, Rectangle bounds)
        {
            using (GraphicsPath path = RoundRectangle(bounds, 3))
            {
                using (SolidBrush b = new SolidBrush(SystemColors.Window))
                {
                    //g.FillPath(b, path);
                    g.FillRectangle(b, bounds);
                }

                using (Pen p = new Pen(TextBoxBorder))
                {
                    //g.DrawPath(p, path);
                    g.DrawRectangle(p, bounds);
                }
            }
        }

        public Color TextBoxUnselectedBg = FromHexStr("#EAF2FB");
        public Color TextBoxBorder = FromHexStr("#ABC1DE");
        private static Color FromHexStr(string hex)
        {
            if (hex.StartsWith("#"))
                hex = hex.Substring(1);

            switch (hex.Length)
            {
                case 6:
                    return Color.FromArgb(
                        int.Parse(hex.Substring(0, 2), NumberStyles.HexNumber),
                        int.Parse(hex.Substring(2, 2), NumberStyles.HexNumber),
                        int.Parse(hex.Substring(4, 2), NumberStyles.HexNumber));

                case 8:
                    return Color.FromArgb(
                        int.Parse(hex.Substring(0, 2), NumberStyles.HexNumber),
                        int.Parse(hex.Substring(2, 2), NumberStyles.HexNumber),
                        int.Parse(hex.Substring(4, 2), NumberStyles.HexNumber),
                        int.Parse(hex.Substring(6, 2), NumberStyles.HexNumber));

                default:
                    throw new Exception("Color not valid");
            }
        }
        public static GraphicsPath RoundRectangle(Rectangle r, int radius)
        {
            return RoundRectangle(r, radius, Corners.All);
        }

        /// <summary>
        /// Creates a rectangle with the specified corners rounded
        /// </summary>
        /// <param name="r"></param>
        /// <param name="radius"></param>
        /// <param name="corners"></param>
        /// <returns></returns>
        public static GraphicsPath RoundRectangle(Rectangle r, int radius, Corners corners)
        {
            GraphicsPath path = new GraphicsPath();
            int d = radius * 2;

            int nw = (corners & Corners.NorthWest) == Corners.NorthWest ? d : 0;
            int ne = (corners & Corners.NorthEast) == Corners.NorthEast ? d : 0;
            int se = (corners & Corners.SouthEast) == Corners.SouthEast ? d : 0;
            int sw = (corners & Corners.SouthWest) == Corners.SouthWest ? d : 0;

            path.AddLine(r.Left + nw, r.Top, r.Right - ne, r.Top);

            if (ne > 0)
            {
                path.AddArc(Rectangle.FromLTRB(r.Right - ne, r.Top, r.Right, r.Top + ne),
                     -90, 90);
            }

            path.AddLine(r.Right, r.Top + ne, r.Right, r.Bottom - se);

            if (se > 0)
            {
                path.AddArc(Rectangle.FromLTRB(r.Right - se, r.Bottom - se, r.Right, r.Bottom),
                     0, 90);
            }

            path.AddLine(r.Right - se, r.Bottom, r.Left + sw, r.Bottom);

            if (sw > 0)
            {
                path.AddArc(Rectangle.FromLTRB(r.Left, r.Bottom - sw, r.Left + sw, r.Bottom),
                     90, 90);
            }

            path.AddLine(r.Left, r.Bottom - sw, r.Left, r.Top + nw);

            if (nw > 0)
            {
                path.AddArc(Rectangle.FromLTRB(r.Left, r.Top, r.Left + nw, r.Top + nw),
                     180, 90);
            }

            path.CloseFigure();

            return path;
        }
        public void DrawTextBoxUnselected(Graphics g, Rectangle bounds)
        {

            //using (SolidBrush b = new SolidBrush(TextBoxUnselectedBg))
            //{
            //    g.FillRectangle(b, bounds);
            //    StringFormat sf = CAlignToSF(TextAlign);
            //    g.DrawString(Text, Font, new SolidBrush(ForeColor), bounds,sf);
            //}

            using (Pen p = new Pen(TextBoxBorder))
            {
                g.DrawRectangle(p, bounds);
            }

        }
        private bool IsEnter;
        protected override void OnMouseEnter(EventArgs e)
        {
            IsEnter = true;
            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            IsEnter = false;
            base.OnMouseLeave(e);
        }
        private StringFormat CAlignToSF(HorizontalAlignment align)
        {
            StringFormat format = new StringFormat();
            switch (align)
            {
   
                case HorizontalAlignment.Right:
                    format.Alignment = StringAlignment.Far;
                    format.LineAlignment = StringAlignment.Center;
                    break;
                case HorizontalAlignment.Left:
                    format.Alignment = StringAlignment.Near;
                    format.LineAlignment = StringAlignment.Center;
                    break;
                case HorizontalAlignment.Center:
                    format.Alignment = StringAlignment.Center;
                    format.LineAlignment = StringAlignment.Center;
                    break;
            }
            return format;
        }
    }
}
