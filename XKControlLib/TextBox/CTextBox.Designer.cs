﻿namespace XKControlLib
{
    partial class CTextBox
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CTextBox));
            this.PicDate = new System.Windows.Forms.PictureBox();
            this.picZoom = new System.Windows.Forms.PictureBox();
            this.Pic = new System.Windows.Forms.PictureBox();
            this.lblLine = new System.Windows.Forms.Label();
            this.txt = new BaseTextBox();
            this.lblCaption = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PicDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picZoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pic)).BeginInit();
            this.SuspendLayout();
            // 
            // PicDate
            // 
            this.PicDate.Image = ((System.Drawing.Image)(resources.GetObject("PicDate.Image")));
            this.PicDate.Location = new System.Drawing.Point(145, 56);
            this.PicDate.Name = "PicDate";
            this.PicDate.Size = new System.Drawing.Size(39, 23);
            this.PicDate.TabIndex = 23;
            this.PicDate.TabStop = false;
            this.PicDate.Visible = false;
            // 
            // picZoom
            // 
            this.picZoom.Image = ((System.Drawing.Image)(resources.GetObject("picZoom.Image")));
            this.picZoom.Location = new System.Drawing.Point(76, 56);
            this.picZoom.Name = "picZoom";
            this.picZoom.Size = new System.Drawing.Size(39, 23);
            this.picZoom.TabIndex = 22;
            this.picZoom.TabStop = false;
            this.picZoom.Visible = false;
            // 
            // Pic
            // 
            this.Pic.Location = new System.Drawing.Point(165, 5);
            this.Pic.Name = "Pic";
            this.Pic.Size = new System.Drawing.Size(33, 25);
            this.Pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.Pic.TabIndex = 21;
            this.Pic.TabStop = false;
            this.Pic.Visible = false;
            this.Pic.VisibleChanged += new System.EventHandler(this.Pic_VisibleChanged);
            this.Pic.Click += new System.EventHandler(this.Pic_Click);
            // 
            // lblLine
            // 
            this.lblLine.BackColor = System.Drawing.Color.Black;
            this.lblLine.Location = new System.Drawing.Point(57, 29);
            this.lblLine.Name = "lblLine";
            this.lblLine.Size = new System.Drawing.Size(100, 1);
            this.lblLine.TabIndex = 20;
            // 
            // txt
            // 
            this.txt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt.Location = new System.Drawing.Point(59, 14);
            this.txt.Name = "txt";
            this.txt.Size = new System.Drawing.Size(100, 14);
            this.txt.TabIndex = 19;
            this.txt.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_KeyDown);
            // 
            // lblCaption
            // 
            this.lblCaption.AutoSize = true;
            this.lblCaption.BackColor = System.Drawing.Color.Transparent;
            this.lblCaption.Location = new System.Drawing.Point(8, 14);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(35, 12);
            this.lblCaption.TabIndex = 18;
            this.lblCaption.Text = "Title";
            this.lblCaption.SizeChanged += new System.EventHandler(this.lblCaption_SizeChanged);
            // 
            // CTextBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.PicDate);
            this.Controls.Add(this.picZoom);
            this.Controls.Add(this.Pic);
            this.Controls.Add(this.lblLine);
            this.Controls.Add(this.txt);
            this.Controls.Add(this.lblCaption);
            this.Name = "CTextBox";
            this.Size = new System.Drawing.Size(125, 23);
            this.SizeChanged += new System.EventHandler(this.CTextBox_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.PicDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picZoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.PictureBox PicDate;
        protected System.Windows.Forms.PictureBox picZoom;
        protected System.Windows.Forms.PictureBox Pic;
        protected System.Windows.Forms.Label lblLine;
        protected BaseTextBox txt;
        protected System.Windows.Forms.Label lblCaption;
    }
}
