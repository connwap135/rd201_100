﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XKControlLib
{
    public partial class CLabel : Label
    {
        public CLabel()
        {
            InitializeComponent();
        }

        public CLabel(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public int Softness => 2;

        public int Direction => 315;

        public int ShadowDepth => 4;

        public int Opacity => 100;

        public Color Color => Color.Black;

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics screenGraphics = e.Graphics;
            Bitmap shadowBitmap = new Bitmap(Width/Softness, Height/Softness);
            StringFormat sf = StringFormat.GenericTypographic;
            switch (this.TextAlign)
            {
                case ContentAlignment.TopLeft:
                    sf.Alignment = StringAlignment.Near;
                    sf.LineAlignment = StringAlignment.Near;
                    break;
                case ContentAlignment.TopCenter:
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Near;
                    break;
                case ContentAlignment.TopRight:
                    sf.Alignment = StringAlignment.Far;
                    sf.LineAlignment = StringAlignment.Near;
                    break;
                case ContentAlignment.MiddleLeft:
                    sf.Alignment = StringAlignment.Near;
                    sf.LineAlignment = StringAlignment.Center;
                    break;
                case ContentAlignment.MiddleCenter:
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    break;
                case ContentAlignment.MiddleRight:
                    sf.Alignment = StringAlignment.Far;
                    sf.LineAlignment = StringAlignment.Center;
                    break;
                case ContentAlignment.BottomLeft:
                    sf.Alignment = StringAlignment.Near;
                    sf.LineAlignment = StringAlignment.Far;
                    break;
                case ContentAlignment.BottomCenter:
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Far;
                    break;
                case ContentAlignment.BottomRight:
                    sf.Alignment = StringAlignment.Far;
                    sf.LineAlignment = StringAlignment.Far;
                    break;
                default:
                    break;
            }
            using (Graphics imageGraphics = Graphics.FromImage(shadowBitmap))
            {
                imageGraphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
                Matrix transformMatrix = new Matrix();
                transformMatrix.Scale((1f / this.Softness), (1f / this.Softness));
                transformMatrix.Translate(Convert.ToSingle((this.ShadowDepth * Math.Cos(Convert.ToDouble(this.Direction)))), Convert.ToSingle((this.ShadowDepth * Math.Sin(Convert.ToDouble(this.Direction)))));
                imageGraphics.Transform = transformMatrix;
                imageGraphics.DrawString(this.Text, this.Font, new SolidBrush(Color.FromArgb(this.Opacity, this.Color)), this.DisplayRectangle, sf);
            }

            screenGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            screenGraphics.DrawImage(shadowBitmap, this.ClientRectangle, 0, 0, shadowBitmap.Width, shadowBitmap.Height, GraphicsUnit.Pixel);
            screenGraphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            screenGraphics.DrawString(this.Text, this.Font, new SolidBrush(this.ForeColor), this.DisplayRectangle, sf);
        }
    }
}
