﻿namespace XKControlLib.Print
{
    partial class frmChooseBiaoTi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cl_显示 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.txtFontSize = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.txtWidth = new System.Windows.Forms.TextBox();
            this.cboFormat = new System.Windows.Forms.ComboBox();
            this.cboDefinition = new System.Windows.Forms.ComboBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.cl_项目名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboAlign = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnSelectNo = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.cl_项目别名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnLast = new System.Windows.Forms.Button();
            this.dg1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dg1)).BeginInit();
            this.SuspendLayout();
            // 
            // cl_显示
            // 
            this.cl_显示.DataPropertyName = "显示";
            this.cl_显示.HeaderText = "显示";
            this.cl_显示.Name = "cl_显示";
            this.cl_显示.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cl_显示.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cl_显示.Width = 45;
            // 
            // txtFontSize
            // 
            this.txtFontSize.Location = new System.Drawing.Point(378, 417);
            this.txtFontSize.Name = "txtFontSize";
            this.txtFontSize.Size = new System.Drawing.Size(94, 21);
            this.txtFontSize.TabIndex = 51;
            this.txtFontSize.TextChanged += new System.EventHandler(this.txtFontSize_TextChanged);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(376, 401);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(59, 12);
            this.Label5.TabIndex = 50;
            this.Label5.Text = "字    号:";
            // 
            // txtWidth
            // 
            this.txtWidth.Location = new System.Drawing.Point(378, 339);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.Size = new System.Drawing.Size(94, 21);
            this.txtWidth.TabIndex = 48;
            this.txtWidth.TextChanged += new System.EventHandler(this.txtWidth_TextChanged);
            // 
            // cboFormat
            // 
            this.cboFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFormat.FormattingEnabled = true;
            this.cboFormat.Items.AddRange(new object[] {
            "无",
            "0",
            "0.0",
            "0.00",
            "0.000",
            "0.0000",
            "000,000",
            "000,000.0",
            "000,000.00",
            "000,000.000",
            "000,000.0000"});
            this.cboFormat.Location = new System.Drawing.Point(378, 300);
            this.cboFormat.Name = "cboFormat";
            this.cboFormat.Size = new System.Drawing.Size(94, 20);
            this.cboFormat.TabIndex = 46;
            this.cboFormat.TextChanged += new System.EventHandler(this.cboFormat_TextChanged);
            // 
            // cboDefinition
            // 
            this.cboDefinition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDefinition.FormattingEnabled = true;
            this.cboDefinition.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4"});
            this.cboDefinition.Location = new System.Drawing.Point(378, 262);
            this.cboDefinition.Name = "cboDefinition";
            this.cboDefinition.Size = new System.Drawing.Size(94, 20);
            this.cboDefinition.TabIndex = 42;
            this.cboDefinition.TextChanged += new System.EventHandler(this.cboDefinition_TextChanged);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(376, 285);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(83, 12);
            this.Label2.TabIndex = 43;
            this.Label2.Text = "格式化字符串:";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(377, 362);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(59, 12);
            this.Label4.TabIndex = 45;
            this.Label4.Text = "对    齐:";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(377, 323);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(59, 12);
            this.Label3.TabIndex = 47;
            this.Label3.Text = "宽    度:";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(377, 246);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(71, 12);
            this.Label1.TabIndex = 44;
            this.Label1.Text = "精确小数位:";
            // 
            // cl_项目名称
            // 
            this.cl_项目名称.DataPropertyName = "项目名称";
            this.cl_项目名称.HeaderText = "项目名称";
            this.cl_项目名称.Name = "cl_项目名称";
            this.cl_项目名称.ReadOnly = true;
            this.cl_项目名称.Width = 130;
            // 
            // cboAlign
            // 
            this.cboAlign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAlign.FormattingEnabled = true;
            this.cboAlign.Items.AddRange(new object[] {
            "左",
            "右",
            "居中"});
            this.cboAlign.Location = new System.Drawing.Point(378, 378);
            this.cboAlign.Name = "cboAlign";
            this.cboAlign.Size = new System.Drawing.Size(94, 20);
            this.cboAlign.TabIndex = 49;
            this.cboAlign.TextChanged += new System.EventHandler(this.cboAlign_TextChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(387, 211);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 41;
            this.btnCancel.Text = "取消(&C)";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(387, 184);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 40;
            this.btnOK.Text = "确定(&O)";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Location = new System.Drawing.Point(387, 102);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(75, 23);
            this.btnPrevious.TabIndex = 37;
            this.btnPrevious.Text = "上移(&P)";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // btnSelectNo
            // 
            this.btnSelectNo.Location = new System.Drawing.Point(387, 43);
            this.btnSelectNo.Name = "btnSelectNo";
            this.btnSelectNo.Size = new System.Drawing.Size(75, 23);
            this.btnSelectNo.TabIndex = 35;
            this.btnSelectNo.Text = "全清(&D)";
            this.btnSelectNo.UseVisualStyleBackColor = true;
            this.btnSelectNo.Click += new System.EventHandler(this.btnSelectNo_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(387, 125);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 38;
            this.btnNext.Text = "下移(&N)";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // btnFirst
            // 
            this.btnFirst.Location = new System.Drawing.Point(387, 79);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(75, 23);
            this.btnFirst.TabIndex = 36;
            this.btnFirst.Text = "置顶(&T)";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(387, 16);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(75, 23);
            this.btnSelectAll.TabIndex = 33;
            this.btnSelectAll.Text = "全选(&A)";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // cl_项目别名
            // 
            this.cl_项目别名.DataPropertyName = "项目别名";
            this.cl_项目别名.HeaderText = "项目别名";
            this.cl_项目别名.Name = "cl_项目别名";
            this.cl_项目别名.Width = 150;
            // 
            // btnLast
            // 
            this.btnLast.Location = new System.Drawing.Point(387, 148);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(75, 23);
            this.btnLast.TabIndex = 39;
            this.btnLast.Text = "置底(&B)";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // dg1
            // 
            this.dg1.AllowUserToAddRows = false;
            this.dg1.BackgroundColor = System.Drawing.Color.White;
            this.dg1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dg1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cl_项目名称,
            this.cl_项目别名,
            this.cl_显示});
            this.dg1.Location = new System.Drawing.Point(12, 12);
            this.dg1.Name = "dg1";
            this.dg1.RowHeadersVisible = false;
            this.dg1.RowTemplate.Height = 23;
            this.dg1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dg1.Size = new System.Drawing.Size(359, 442);
            this.dg1.TabIndex = 34;
            this.dg1.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg1_CellEnter);
            this.dg1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg1_CellValueChanged);
            // 
            // frmChooseBiaoTi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 465);
            this.Controls.Add(this.txtFontSize);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.txtWidth);
            this.Controls.Add(this.cboFormat);
            this.Controls.Add(this.cboDefinition);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.cboAlign);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.btnSelectNo);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnFirst);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.btnLast);
            this.Controls.Add(this.dg1);
            this.Name = "frmChooseBiaoTi";
            this.Text = "表体设置";
            this.Load += new System.EventHandler(this.frmChooseBiaoTi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dg1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.DataGridViewCheckBoxColumn cl_显示;
        internal System.Windows.Forms.TextBox txtFontSize;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.TextBox txtWidth;
        internal System.Windows.Forms.ComboBox cboFormat;
        internal System.Windows.Forms.ComboBox cboDefinition;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn cl_项目名称;
        internal System.Windows.Forms.ComboBox cboAlign;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOK;
        internal System.Windows.Forms.Button btnPrevious;
        internal System.Windows.Forms.Button btnSelectNo;
        internal System.Windows.Forms.Button btnNext;
        internal System.Windows.Forms.Button btnFirst;
        internal System.Windows.Forms.Button btnSelectAll;
        internal System.Windows.Forms.DataGridViewTextBoxColumn cl_项目别名;
        internal System.Windows.Forms.Button btnLast;
        internal System.Windows.Forms.DataGridView dg1;
    }
}