﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XKControlLib.Print
{
    public partial class frmPreview : Form
    {
        private AnTemplateInfo anTemplateInfo;
        private bool v;
        private Draw Draw;
        private int PageIndex;

        public frmPreview()
        {
            InitializeComponent();
        }

        public frmPreview(AnTemplateInfo anTemplateInfo, bool isPrint = false)
        {
            InitializeComponent();
            this.anTemplateInfo = anTemplateInfo;
            this.v = isPrint;
        }

        private void frmPreview_Load(object sender, EventArgs e)
        {
            PrintPreviewControl1.Document = PrintDocument1;
            setPrintPreviewPage();
            PrintPreviewControl1_StartPageChanged(sender, e);
            Draw = new Draw(anTemplateInfo);
            if (v)
            {
                Hide();
                PrintDocument1.Print();
            }
        }

        private void setPrintPreviewPage()
        {
            if (anTemplateInfo.TemplateInfos != null)
            {
                string cPageName = anTemplateInfo.TemplateInfos.A04_cPageName;
                int iPageWage = anTemplateInfo.TemplateInfos.A05_iPageWay;
                foreach (PaperSize PS in PrintDocument1.PrinterSettings.PaperSizes)
                {
                    if (PS.PaperName == cPageName)
                    {
                        PrintDocument1.DefaultPageSettings.PaperSize = PS;
                        break;
                    }
                }
                PrintDocument1.DocumentName = getPageTotal().ToString();
                PrintDocument1.DefaultPageSettings.Landscape = iPageWage == 0 ? false : true;
            }
            else
            {
                MessageBox.Show("打印模板错误", "提示");
            }
        }

        private int getPageTotal()
        {
            if (!ReportMember.ExistBiaoTi())
            {
                return 1;
            }
            if (ReportMember.dtBiaoTi.Rows.Count == 0)
            {
                return 1;
            }
            int iHeaderHeight = anTemplateInfo.TemplateInfos.A10_iDataHeaderHeight;
            int iRowHeight = anTemplateInfo.TemplateInfos.A11_iDataRowHeight;
            int iDGHeight = anTemplateInfo.TemplateInfos.A09_iDataGridHeight;
            iDGHeight = iDGHeight - iHeaderHeight;
            iDGHeight -= iDGHeight % iRowHeight;
            int RowNum = ReportMember.dtBiaoTi.Rows.Count;
            int PageTotal = RowNum * iRowHeight / iDGHeight;
            if (RowNum * iRowHeight % iDGHeight != 0)
            {
                PageTotal += 1;
            }
            return PageTotal;
        }

        private void PrintPreviewControl1_StartPageChanged(object sender, EventArgs e)
        {
            TlblShow.Text = "第" + (PrintPreviewControl1.StartPage + 1) + "页 共" + getPageTotal() + "页";
        }

        private void TcboSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            int value = int.Parse(TcboSize.Text);
            PrintPreviewControl1.Zoom = (double)value / 100;
        }

        private void PrintDocument1_EndPrint(object sender, PrintEventArgs e)
        {
            if (v)
            {
                this.Close();
            }
        }

        private void PrintDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            if (PrintDocument1.PrinterSettings.PrintRange == PrintRange.SomePages)
            {
                PageIndex = PrintDocument1.PrinterSettings.FromPage - 1;
            }
            Graphics g = e.Graphics;
            //'-------------画页码标记-----------
            var DrawStringInfo = new DrawStringInfo();
            DrawStringInfo.Color = Color.Black;
            DrawStringInfo.cText = "第" + (PageIndex + 1) + "页,共" + getPageTotal() + "页";
            DrawStringInfo.iLineAlign = 2;
            DrawStringInfo.iAlign = 2;
            DrawStringInfo.iFontSize = 9;
            DrawStringInfo.iHeight = 30;
            DrawStringInfo.iLeft = 0;
            DrawStringInfo.iTop = e.PageBounds.Bottom - 50;
            DrawStringInfo.iWidth = e.PageBounds.Right;
            Draw.DrawString(g, DrawStringInfo);

            Draw.DrawBiaoTouLine(g);
            if (ReportMember.ExistBiaoTou())
            {
                Draw.DrawBiaoTou(g, ReportMember.dtBiaoTou);
            }

            if (ReportMember.ExistBiaoTi())
            {
                DataTable dtPageBiaoTi = getPageBiaoTi(PageIndex);
                Draw.DrawBiaoTi(g, dtPageBiaoTi);
                Draw.DrawBiaoTiLine(g, dtPageBiaoTi);
                if (PageIndex < getPageTotal_byToPage() - 1)
                {
                    PageIndex += 1;
                    e.HasMorePages = true;
                }
                else
                {
                    PageIndex = 0;
                }
            }
        }

        private int getPageTotal_byToPage()
        {
            var i = getPageTotal();
            if (PrintDocument1.PrinterSettings.PrintRange == PrintRange.SomePages)
            {
                return PrintDocument1.PrinterSettings.ToPage;
            }
            else
            {
                return i;
            }
        }

        private DataTable getPageBiaoTi(int pageIndex)
        {
            int PageRowTotal = getPageRowTotal();
            int StartRowIndex = PageRowTotal * pageIndex;
            int EndRowIndex = StartRowIndex + PageRowTotal;
            EndRowIndex = EndRowIndex > getRowTotal() ? getRowTotal() : EndRowIndex;
            EndRowIndex -= 1;
            int iRowHeight = anTemplateInfo.TemplateInfos.A11_iDataRowHeight;
            var dt = ReportMember.dtBiaoTi.Clone();
            for (int i = StartRowIndex; i <= EndRowIndex; i++)
            {
                var dr = ReportMember.dtBiaoTi.Rows[i];
                dt.Rows.Add(dr.ItemArray);
            }
            return dt;
        }

        private int getRowTotal()
        {
            if (ReportMember.ExistBiaoTi())
            {
                return ReportMember.dtBiaoTi.Rows.Count;
            }
            return 0;
        }

        private int getPageRowTotal()
        {
            int iRowHeight = anTemplateInfo.TemplateInfos.A11_iDataRowHeight;
            int iDGHeight = anTemplateInfo.TemplateInfos.A09_iDataGridHeight;
            iDGHeight -= iDGHeight % iRowHeight;
            iDGHeight = iDGHeight - iRowHeight;
            return iDGHeight / iRowHeight;
        }

        private void TS_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "TbtnPrint":
                    var pDlg = new PrintDialog();
                    pDlg.Document = PrintDocument1;
                    pDlg.AllowSomePages = true;
                    if (pDlg.PrinterSettings.FromPage == 0) { pDlg.PrinterSettings.FromPage = 1; }
                    if (pDlg.PrinterSettings.ToPage == 0) { pDlg.PrinterSettings.ToPage = getPageTotal(); }
                    pDlg.UseEXDialog = true;
                    if (pDlg.ShowDialog() == DialogResult.OK)
                    {
                        PrintDocument1.Print();
                    }
                    break;
                case "TbtnFirst":
                    PrintPreviewControl1.StartPage = 0;
                    break;
                case "TbtnPrevious":
                    if (PrintPreviewControl1.StartPage > 0)
                    {
                        PrintPreviewControl1.StartPage -= 1;
                    }
                    break;
                case "TbtnNext":
                    PrintPreviewControl1.StartPage += 1;
                    break;
                case "TbtnLast":
                    PrintPreviewControl1.StartPage = getPageTotal();
                    break;
                case "TBtnCols":
                    if (PrintPreviewControl1.Columns == 1)
                    {
                        PrintPreviewControl1.Columns = 2;
                        PrintPreviewControl1.Rows = 2;
                    }
                    else
                    {
                        PrintPreviewControl1.Columns = 1;
                        PrintPreviewControl1.Rows = 1;
                    }
                    PrintPreviewControl1.AutoZoom = true;
                    break;
                case "TbtnExit":
                    Close();
                    break;
                default:
                    break;
            }
        }

        private void frmPreview_FormClosing(object sender, FormClosingEventArgs e)
        {
            ReportMember.reportFile = null;
            ReportMember.dtBiaoTou = null;
            ReportMember.dtBiaoTi = null;
        }
    }
}
