﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XKControlLib.Print
{
    public class TemplateInfo
    {
        internal Panel pnl = new Panel();
        private int _a06_iDataGridLeft;
        private int _a07_iDataGridTop;
        private int _a08_iDataGridWidth;
        private int _a09_iDataGridHeight;

        public int A01_TemplateId { get; set; }
        public string A02_cReportId { get; set; }
        public string A03_cTemplateName { get; set; }
        public string A04_cPageName { get; set; }
        public int A05_iPageWay { get; set; }
        public int A06_iDataGridLeft
        {
            get => _a06_iDataGridLeft;
            set
            {
                _a06_iDataGridLeft = value;
                pnl.Left = value;
            }
        }
        public int A07_iDataGridTop
        {
            get => _a07_iDataGridTop;
            set
            {
                _a07_iDataGridTop = value;
                pnl.Top = value;
            }
        }
        public int A08_iDataGridWidth
        {
            get => _a08_iDataGridWidth;
            set
            {
                _a08_iDataGridWidth = value;
                pnl.Width = value;
            }
        }
        public int A09_iDataGridHeight
        {
            get => _a09_iDataGridHeight;
            set
            {
                _a09_iDataGridHeight = value;
                pnl.Height = value;
            }
        }
        public int A10_iDataHeaderHeight { get; set; }
        public int A11_iDataRowHeight { get; set; }
        public bool A12_bTaoDa { get; set; }

        public TemplateInfo()
        {
            pnl.LocationChanged += Pnl_LocationChanged;
            pnl.SizeChanged += Pnl_SizeChanged;
            pnl.BorderStyle = BorderStyle.FixedSingle;
            pnl.Name = "表体" + A01_TemplateId;
            pnl.Top = 200;
            pnl.Left = 50;
            pnl.Width = 600;
            pnl.Height = 400;
            pnl.Visible = true;
        }

        private void Pnl_SizeChanged(object sender, EventArgs e)
        {
            A08_iDataGridWidth = pnl.Width;
            A09_iDataGridHeight = pnl.Height;
        }

        private void Pnl_LocationChanged(object sender, EventArgs e)
        {
            A06_iDataGridLeft = pnl.Left;
            A07_iDataGridTop = pnl.Top;
        }
    }
}
