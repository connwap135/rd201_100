﻿using System.Drawing;
using System.Windows.Forms;

namespace XKControlLib.Print
{
    public class BiaoTiInfo
    {
        public int A01_Id { get; set; }
        public int A02_TemplateId { get; set; }
        public string A03_cColumnName { get; set; }
        private string _A04_cColumnByName;
        public string A04_cColumnByName
        {
            get => _A04_cColumnByName;
            set
            {
                _A04_cColumnByName = value;
                lbl.Text = value;
            }
        }
        public int A05_iLeft
        {
            get => _a05_iLeft;
            set
            {
                _a05_iLeft = value;
                lbl.Left = value;
            }
        }
        public int A06_iWidth
        {
            get => _a06_iWidth;
            set
            {
                _a06_iWidth = value; lbl.Width = value;
            }
        }
        public int A07_iFontSize
        {
            get => _a07_iFontSize;
            set
            {
                _a07_iFontSize = value;
                var font = new Font("宋体", value);
                lbl.Font = font;
            }
        }
        public int A08_iAlign { get; set; }
        public string A09_iNumberDefinition { get; set; }
        public string A10_cNumberFormat { get; set; }
        public bool A11_bShow
        {
            get => _a11_bShow;
            set
            {
                _a11_bShow = value;
                lbl.Visible = value;
            }
        }
        public bool A12_bDouble { get; set; }
        internal Label lbl = new Label();
        private int _a05_iLeft;
        private int _a06_iWidth;
        private int _a07_iFontSize;
        private bool _a11_bShow;

        internal PropertyNuke Nuke => new PropertyNuke(this);
        internal static PropertyName PN => new PropertyName(true);
        internal static PropertyName PM => new PropertyName(false);
        public BiaoTiInfo()
        {
            lbl.FontChanged += Lbl_FontChanged;
            lbl.VisibleChanged += Lbl_VisibleChanged;
            lbl.AutoSize = false;
            lbl.Visible = A11_bShow;
        }

        private void Lbl_VisibleChanged(object sender, System.EventArgs e)
        {
            _a11_bShow = lbl.Visible;
        }

        private void Lbl_FontChanged(object sender, System.EventArgs e)
        {
            A07_iFontSize =(int) lbl.Font.Size;
        }

        internal class PropertyName
        {
            private readonly bool v;

            public PropertyName(bool v)
            {
                this.v = v;
            }

            public string A03_cColumnName => v ? "A03_cColumnName" : "列名";
            public string A04_cColumnByName => v ? "A04_cColumnByName" : "列别名";
            public string A11_bShow => v ? "A11_bShow" : "是否显示";
        }

        internal class PropertyNuke
        {
            private readonly BiaoTiInfo mdl;

            public PropertyNuke(BiaoTiInfo v)
            {
                this.mdl = v;
            }
        }
    }
}