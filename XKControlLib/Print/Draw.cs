﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

namespace XKControlLib.Print
{
    public class Draw
    {
        private AnTemplateInfo anTemplateInfo;

        public Draw(AnTemplateInfo anTemplateInfo)
        {
            this.anTemplateInfo = anTemplateInfo;
        }

        public void DrawString(Graphics g, DrawStringInfo mdl)
        {
            //    '-----------字体---------
            var fStyle = FontStyle.Regular;
            if (mdl.bBold)
            {
                fStyle |= FontStyle.Bold;
            }
            if (mdl.bItalic)
            {
                fStyle |= FontStyle.Italic;
            }
            if (mdl.bUnderLine)
            {
                fStyle |= FontStyle.Underline;
            }
            var font = new Font("宋体", mdl.iFontSize, fStyle);
            //'-------------区域--------------
            var Rect = new RectangleF(mdl.iLeft, mdl.iTop, mdl.iWidth, mdl.iHeight);
            //'-------------对齐--------------
            var StrFormat = new StringFormat();
            if (mdl.iLineAlign == 0)
            {
                StrFormat.LineAlignment = StringAlignment.Near;
            }
            else if (mdl.iLineAlign == 1)
            {
                StrFormat.LineAlignment = StringAlignment.Far;
            }
            else if (mdl.iLineAlign == 2)
            {
                StrFormat.LineAlignment = StringAlignment.Center;
            }

            if (mdl.iAlign == 0)
            {
                StrFormat.Alignment = StringAlignment.Near;
            }
            else if (mdl.iAlign == 1)
            {
                StrFormat.Alignment = StringAlignment.Far;
            }
            else if (mdl.iAlign == 2)
            {
                StrFormat.Alignment = StringAlignment.Center;
            }
            var p = new Pen(mdl.Color);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            g.DrawString(mdl.cText, font, p.Brush, Rect, StrFormat);
        }

        public void DrawBiaoTouLine(Graphics g)
        {
            if (anTemplateInfo.LineInfos == null)
            {
                return;
            }
            var drawLineInfos = new List<DrawLineInfo>();
            foreach (LineInfo mdl in anTemplateInfo.LineInfos)
            {
                var drawLineInfo = new DrawLineInfo();
                drawLineInfo.x1 = mdl.A03_x1;
                drawLineInfo.y1 = mdl.A04_y1;
                drawLineInfo.x2 = mdl.A05_x2;
                drawLineInfo.y2 = mdl.A06_y2;
                drawLineInfos.Add(drawLineInfo);
            }
            DrawLine(g, drawLineInfos);
        }

        private void DrawLine(Graphics g, List<DrawLineInfo> mdl)
        {
            foreach (var m in mdl)
            {
                DrawLine(g, m);
            }
        }

        private void DrawLine(Graphics g, DrawLineInfo mdl)
        {
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            g.DrawLine(Pens.Black, mdl.x1, mdl.y1, mdl.x2, mdl.y2);
        }

        public void DrawBiaoTou(Graphics g, DataTable dtBiaoTou)
        {
            var mdls = anTemplateInfo.BiaoTouInfos;
            if (mdls == null || dtBiaoTou.Rows.Count == 0) { return; }

            var DrawStringInfos = new List<DrawStringInfo>();
            var dr = dtBiaoTou.Rows[0];
            bool bTaoDa = anTemplateInfo.TemplateInfos.A12_bTaoDa;
            foreach (var mdl in mdls)
            {
                var cColumnName = mdl.A03_cColumnName;
                if (dtBiaoTou.Columns.Contains(cColumnName))
                {
                    var DrawStringInfo = new DrawStringInfo();
                    var cText = dr[cColumnName].ToString();
                    if (dtBiaoTou.Columns[cColumnName].DataType == typeof(double) && !string.IsNullOrEmpty(cText))
                    {
                        if (mdl.A14_cNumberFormat == "大写金额")
                        {
                            //cText = FormatNumber(Todbl(cText), mdl.A13_iNumberDefinition)
                            cText = Common.ConvertData.UpperMoney(cText);
                        }
                        else
                        {
                            cText = Convert.ToDouble(cText).ToString();
                            //cText = FormatNumber(Todbl(cText), mdl.A13_iNumberDefinition, mdl.A14_cNumberFormat)
                        }
                    }
                    cText = mdl.A04_cColumnByName + " " + cText;
                    DrawStringInfo.Color = Color.Black;
                    DrawStringInfo.bBold = mdl.A10_bBold;
                    DrawStringInfo.bItalic = mdl.A11_bItalic;
                    DrawStringInfo.bUnderLine = mdl.A12_bUnderLine;
                    DrawStringInfo.cText = cText;
                    DrawStringInfo.iLineAlign = 0;
                    DrawStringInfo.iAlign = 0;
                    DrawStringInfo.iFontSize = mdl.A09_iFontSize;
                    DrawStringInfo.iHeight = mdl.A08_iHeight;
                    DrawStringInfo.iLeft = mdl.A05_iLeft;
                    DrawStringInfo.iTop = mdl.A06_iTop;
                    DrawStringInfo.iWidth = mdl.A07_iWidth;
                    DrawStringInfos.Add(DrawStringInfo);
                    if (bTaoDa)
                    {
                        DrawStringInfo = DrawStringInfo.Clone();
                        DrawStringInfo.cText = mdl.A04_cColumnByName;
                        DrawStringInfo.Color = Color.White;
                        DrawStringInfos.Add(DrawStringInfo);
                    }
                }
            }
            DrawString(g, DrawStringInfos);
        }

        private void DrawString(Graphics g, List<DrawStringInfo> mdl)
        {
            foreach (var item in mdl)
            {
                DrawString(g, item);
            }
        }

        public void DrawBiaoTi(Graphics g, DataTable dtBiaoTi)
        {
            var mdls = anTemplateInfo.BiaoTiInfos;
            if (mdls == null) { return; }
            //------------画列标题--------
            var DrawStringInfos = new List<DrawStringInfo>();
            var iHeaderHeight = anTemplateInfo.TemplateInfos.A10_iDataHeaderHeight;
            var iRowHeight = anTemplateInfo.TemplateInfos.A11_iDataRowHeight;
            var iDGLeft = anTemplateInfo.TemplateInfos.A06_iDataGridLeft;
            var iDGTop = anTemplateInfo.TemplateInfos.A07_iDataGridTop;
            var bTaoDa = anTemplateInfo.TemplateInfos.A12_bTaoDa;
            if (!bTaoDa)
            {
                foreach (var mdl in mdls)
                {
                    var cColumnName = mdl.A03_cColumnName;
                    foreach (DataRow dr in dtBiaoTi.Rows)
                    {
                        if (dtBiaoTi.Columns.Contains(cColumnName))
                        {
                            var DrawStringInfo = new DrawStringInfo();
                            DrawStringInfo.Color = Color.Black;
                            DrawStringInfo.cText = mdl.A04_cColumnByName;
                            DrawStringInfo.iLineAlign = 2;
                            DrawStringInfo.iAlign = 2;
                            DrawStringInfo.iFontSize = mdl.A07_iFontSize;
                            DrawStringInfo.iHeight = iHeaderHeight - 4;
                            DrawStringInfo.iLeft = iDGLeft + mdl.A05_iLeft + 2;
                            DrawStringInfo.iTop = iDGTop + 2;
                            DrawStringInfo.iWidth = mdl.A06_iWidth - 4;
                            DrawStringInfos.Add(DrawStringInfo);
                        }
                    }
                }
            }
            foreach (var mdl in mdls)
            {
                var cColumnName = mdl.A03_cColumnName;
                foreach (DataRow dr in dtBiaoTi.Rows)
                {
                    var RowIndex = dtBiaoTi.Rows.IndexOf(dr);
                    if (dtBiaoTi.Columns.Contains(cColumnName))
                    {
                        var cText = dr[cColumnName].ToString();
                        if (dtBiaoTi.Columns[cColumnName].DataType == typeof(double) && !string.IsNullOrEmpty(cText))
                        {
                            cText = Convert.ToDouble(cText).ToString();// FormatNumber(Todbl(cText), mdl.A09_iNumberDefinition, mdl.A10_cNumberFormat);
                        }
                        var DrawStringInfo = new DrawStringInfo();
                        DrawStringInfo.Color = Color.Black;
                        DrawStringInfo.cText = cText;
                        DrawStringInfo.iLineAlign = 2;
                        DrawStringInfo.iAlign = mdl.A08_iAlign;
                        DrawStringInfo.iFontSize = mdl.A07_iFontSize;
                        DrawStringInfo.iHeight = iRowHeight - 4;
                        DrawStringInfo.iLeft = iDGLeft + mdl.A05_iLeft + 2;
                        DrawStringInfo.iTop = iDGTop + iHeaderHeight + RowIndex * iRowHeight + 2;
                        DrawStringInfo.iWidth = mdl.A06_iWidth - 4;
                        DrawStringInfos.Add(DrawStringInfo);
                    }
                }
            }
            DrawString(g, DrawStringInfos);
        }

        public void DrawBiaoTiLine(Graphics g, DataTable dtBiaoTi)
        {
            var mdls = anTemplateInfo.BiaoTiInfos;
            if (mdls == null) { return; }
            var bTaoDa = anTemplateInfo.TemplateInfos.A12_bTaoDa;
            if (bTaoDa) { return; }
            //------------画横线---------
            var DrawLineInfos = new List<DrawLineInfo>();
            var iHeaderHeight = anTemplateInfo.TemplateInfos.A10_iDataHeaderHeight;
            var iRowHeight = anTemplateInfo.TemplateInfos.A11_iDataRowHeight;
            var iDGLeft = anTemplateInfo.TemplateInfos.A06_iDataGridLeft;
            var iDGTop = anTemplateInfo.TemplateInfos.A07_iDataGridTop;
            if (mdls.Count != 0)
            {
                var mdl = mdls[0];
                var mdl2 = mdls[mdls.Count - 1];
                for (int i = 0; i <= dtBiaoTi.Rows.Count + 1; i++)
                {
                    var DrawLineInfo = new DrawLineInfo();
                    DrawLineInfo.x1 = iDGLeft + mdl.A05_iLeft;
                    if (i == 0)
                    {
                        DrawLineInfo.y1 = iDGTop;
                    }
                    else if (i == 1)
                    {
                        DrawLineInfo.y1 = iDGTop + iHeaderHeight;
                    }
                    else if (i > 1)
                    {
                        DrawLineInfo.y1 = iDGTop + iHeaderHeight + (i - 1) * iRowHeight;
                    }
                    DrawLineInfo.x2 = iDGLeft + mdl2.A05_iLeft + mdl2.A06_iWidth;
                    DrawLineInfo.y2 = DrawLineInfo.y1;
                    DrawLineInfos.Add(DrawLineInfo);
                }
            }
            //-----------画竖线--------------
            var y2 = iDGTop + iHeaderHeight + (dtBiaoTi.Rows.Count) * iRowHeight;
            foreach (var mdl in mdls)
            {
                var DrawLineInfo = new DrawLineInfo();
                DrawLineInfo.x1 = iDGLeft + mdl.A05_iLeft;
                DrawLineInfo.y1 = iDGTop;
                DrawLineInfo.x2 = DrawLineInfo.x1;
                DrawLineInfo.y2 = y2;
                DrawLineInfos.Add(DrawLineInfo);
                var Index = mdls.IndexOf(mdl);
                if (Index == mdls.Count - 1)
                {
                    var DrawLineInfo1 = new DrawLineInfo();
                    DrawLineInfo1.x1 = iDGLeft + mdl.A05_iLeft + mdl.A06_iWidth;
                    DrawLineInfo1.y1 = iDGTop;
                    DrawLineInfo1.x2 = DrawLineInfo1.x1;
                    DrawLineInfo1.y2 = y2;
                    DrawLineInfos.Add(DrawLineInfo1);
                }
            }
            DrawLine(g, DrawLineInfos);
        }
    }
}