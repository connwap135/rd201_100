﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Windows.Forms;
using XKControlLib.DesignControl;

namespace XKControlLib.Print
{
    public partial class frmDesignTemplate : Form
    {
        private readonly Designer Des = new();
        private readonly List<LineInfo> lines = new();
        private readonly frmPageSetting frmPageSetting = new();
        private readonly frmChooseBiaoTou frmChooseBiaoTou = new();
        private readonly frmChooseBiaoTi frmChooseBiaoTi = new();

        public frmDesignTemplate() => InitializeComponent();

        public frmDesignTemplate(AnTemplateInfo template)
        {
            InitializeComponent();
            for (int i = 0; i < 50; i++)
            {
                LineInfo line = new();
                lines.Add(line);
                PnlMain.Controls.Add(line.Line);
            }
            AnTemplateInfo mdl = template;
            if (mdl.TemplateInfos != null)
            {
                //this.frmPageSetting.TemplateInfo = mdl.TemplateInfos;
                this.frmPageSetting.TemplateInfo.A01_TemplateId = mdl.TemplateInfos.A01_TemplateId;
                this.frmPageSetting.TemplateInfo.A02_cReportId = mdl.TemplateInfos.A02_cReportId;
                this.frmPageSetting.TemplateInfo.A03_cTemplateName = mdl.TemplateInfos.A03_cTemplateName;
                this.frmPageSetting.TemplateInfo.A04_cPageName = mdl.TemplateInfos.A04_cPageName;
                this.frmPageSetting.TemplateInfo.A05_iPageWay = mdl.TemplateInfos.A05_iPageWay;
                this.frmPageSetting.TemplateInfo.A06_iDataGridLeft = mdl.TemplateInfos.A06_iDataGridLeft;
                this.frmPageSetting.TemplateInfo.A07_iDataGridTop = mdl.TemplateInfos.A07_iDataGridTop;
                this.frmPageSetting.TemplateInfo.A08_iDataGridWidth = mdl.TemplateInfos.A08_iDataGridWidth;
                this.frmPageSetting.TemplateInfo.A09_iDataGridHeight = mdl.TemplateInfos.A09_iDataGridHeight;
                this.frmPageSetting.TemplateInfo.A10_iDataHeaderHeight = mdl.TemplateInfos.A10_iDataHeaderHeight;
                this.frmPageSetting.TemplateInfo.A11_iDataRowHeight = mdl.TemplateInfos.A11_iDataRowHeight;
                this.frmPageSetting.TemplateInfo.A12_bTaoDa = mdl.TemplateInfos.A12_bTaoDa;
            }
            if (ReportMember.ExistBiaoTou() && mdl.BiaoTouInfos != null)
            {
                foreach (BiaoTouInfo m in mdl.BiaoTouInfos)
                {
                    SetBiaoTou(m);
                }
            }
            if (ReportMember.ExistBiaoTi() && mdl.BiaoTiInfos != null)
            {
                foreach (BiaoTiInfo m in mdl.BiaoTiInfos)
                {
                    SetBiaoTi(m);
                }
            }
            if (mdl.LineInfos != null)
            {
                foreach (LineInfo m in mdl.LineInfos)
                {
                    ShowAnLine(m);
                }
            }
        }

        private void ShowAnLine(LineInfo mdl)
        {
            foreach (LineInfo item in this.lines)
            {
                if (item.Visible == false)
                {
                    item.Visible = true;
                    item.Line.Visible = true;
                    item.A03_x1 = mdl.A03_x1;
                    item.A04_y1 = mdl.A04_y1;
                    item.A05_x2 = mdl.A05_x2;
                    item.A06_y2 = mdl.A06_y2;
                    return;
                }
            }
        }

        private void SetBiaoTi(BiaoTiInfo mdl)
        {
            BiaoTiInfo Item = this.frmChooseBiaoTi.BiaoTiInfos.Where(x => x.A03_cColumnName.Equals(mdl.A03_cColumnName)).First();
            if (Item == null) { return; }
            Item.A04_cColumnByName = mdl.A04_cColumnByName;
            Item.A05_iLeft = mdl.A05_iLeft;
            Item.A06_iWidth = mdl.A06_iWidth;
            Item.A07_iFontSize = mdl.A07_iFontSize;
            Item.A08_iAlign = mdl.A08_iAlign;
            Item.A09_iNumberDefinition = mdl.A09_iNumberDefinition;
            Item.A10_cNumberFormat = mdl.A10_cNumberFormat;
            Item.A11_bShow = true;
            int index = this.frmChooseBiaoTi.BiaoTiInfos.Count;
            this.frmChooseBiaoTi.BiaoTiInfos.Remove(Item);
            this.frmChooseBiaoTi.BiaoTiInfos.Insert(index - 1, Item);

        }

        private void SetBiaoTou(BiaoTouInfo mdl)
        {
            var Item = this.frmChooseBiaoTou.BiaoTouInfos.Where(x => x.A03_cColumnName.Equals(mdl.A03_cColumnName)).First();
            if (Item == null) { return; }
            Item.A04_cColumnByName = mdl.A04_cColumnByName;
            Item.A05_iLeft = mdl.A05_iLeft;
            Item.A06_iTop = mdl.A06_iTop;
            Item.A07_iWidth = mdl.A07_iWidth;
            Item.A08_iHeight = mdl.A08_iHeight;
            Item.A09_iFontSize = mdl.A09_iFontSize;
            Item.A10_bBold = mdl.A10_bBold;
            Item.A11_bItalic = mdl.A11_bItalic;
            Item.A12_bUnderLine = mdl.A12_bUnderLine;
            Item.A13_iNumberDefinition = mdl.A13_iNumberDefinition;
            Item.A14_cNumberFormat = mdl.A14_cNumberFormat;
            Item.A15_bShow = true;

        }

        private void FrmDesignTemplate_Load(object sender, EventArgs e)
        {
            this.TbtnBiaoTou.Visible = ReportMember.ExistBiaoTou();
            this.TbtnBiaoTi.Visible = ReportMember.ExistBiaoTi();
            this.TbtnGrid.Visible = ReportMember.ExistBiaoTi();
            this.Des.ParentObject = this.PnlMain;
            PageSettingChanged();
            AddControl();
        }

        private void AddControl()
        {
            PnlMain.Controls.Add(frmPageSetting.TemplateInfo.pnl);
            frmPageSetting.TemplateInfo.pnl.Visible = ReportMember.ExistBiaoTi();
            foreach (BiaoTouInfo mdl in frmChooseBiaoTou.BiaoTouInfos)
            {
                PnlMain.Controls.Add(mdl.lbl);
            }
            foreach (BiaoTiInfo mdl in frmChooseBiaoTi.BiaoTiInfos)
            {
                frmPageSetting.TemplateInfo.pnl.Controls.Add(mdl.lbl);
            }
        }

        private void PageSettingChanged()
        {
            bool bTaoDa = frmPageSetting.TemplateInfo.A12_bTaoDa;
            foreach (BiaoTouInfo mdl in this.frmChooseBiaoTou.BiaoTouInfos)
            {
                mdl.lbl.ForeColor = bTaoDa ? Color.Gray : Color.Black;
            }
            foreach (BiaoTiInfo mdl in frmChooseBiaoTi.BiaoTiInfos)
            {
                mdl.lbl.ForeColor = bTaoDa ? Color.Gray : Color.Black;
                mdl.lbl.Height = frmPageSetting.TemplateInfo.A10_iDataHeaderHeight;
            }
        }

        private void PnlMain_Paint(object sender, PaintEventArgs e)
        {
            DrawRule();
        }

        private void DrawRule()
        {
            Graphics g = PnlMain.CreateGraphics();
            g.Clear(PnlMain.BackColor);
            int Interval = GetAnCMInterval();
            int WidthCMTotal = GetCMTotal(PnlMain.Width);
            int WidthCMTotal2 = GetCMTotal(PnlMain.HorizontalScroll.Value + PnlMain.Width);
            int HeightCMTotal = GetCMTotal(PnlMain.Height);
            int HeightCMTotal2 = GetCMTotal(PnlMain.VerticalScroll.Value + PnlMain.Height);
            int WidthStartCM = WidthCMTotal2 - WidthCMTotal;
            int HeightStartCM = HeightCMTotal2 - HeightCMTotal;
            g.DrawString("┌", PnlMain.Font, Brushes.Black, 0, 0);
            for (int i = 1; i <= WidthCMTotal; i++)
            {
                g.DrawString((WidthStartCM + i).ToString(), Font, Brushes.Black, i * Interval, 0);
            }
            for (int i = 1; i <= HeightCMTotal; i++)
            {
                g.DrawString((HeightStartCM + i).ToString(), Font, Brushes.Black, 0, i * Interval);
            }
            string PageName = frmPageSetting.TemplateInfo.A04_cPageName;
            PageName = string.IsNullOrEmpty(PageName) ? "A4" : PageName;
            PrintDocument pd = new();
            foreach (PaperSize Page in pd.PrinterSettings.PaperSizes)
            {
                if (Page.PaperName.StartsWith(PageName))
                {
                    int iwidth = frmPageSetting.TemplateInfo.A05_iPageWay == 0 ? Page.Width : Page.Height;
                    int iheight = frmPageSetting.TemplateInfo.A05_iPageWay == 0 ? Page.Height : Page.Width;
                    iwidth -= PnlMain.HorizontalScroll.Value;
                    iheight -= PnlMain.VerticalScroll.Value;
                    g.DrawLine(Pens.Gray, iwidth, 0, iwidth, iheight);
                    g.DrawLine(Pens.Gray, 0, iheight, iwidth, iheight);
                }
            }
        }

        private int GetCMTotal(int Display)
        {
            int Interval = GetAnCMInterval();
            return Display / Interval;
        }

        private int GetAnCMInterval()
        {
            return PrinterUnitConvert.Convert(100, PrinterUnit.TenthsOfAMillimeter, PrinterUnit.Display);
        }

        private AnTemplateInfo GetAnTemplateInfo()
        {
            PnlMain.AutoScrollPosition = new Point(0, 0);
            var Anplt = new AnTemplateInfo();
            TemplateInfo plt = frmPageSetting.TemplateInfo;
            plt.A02_cReportId = ReportMember.cReportId;
            Anplt.TemplateInfos = plt;
            List<BiaoTouInfo> BiaoTous = new();
            foreach (BiaoTouInfo mdl in frmChooseBiaoTou.BiaoTouInfos)
            {
                if (mdl.lbl.Visible)
                {
                    BiaoTous.Add(mdl);
                }
            }
            Anplt.BiaoTouInfos = BiaoTous;
            List<BiaoTiInfo> BiaoTis = new();
            foreach (BiaoTiInfo mdl in frmChooseBiaoTi.BiaoTiInfos)
            {
                if (mdl.lbl.Visible)
                {
                    BiaoTis.Add(mdl);
                }
            }
            Anplt.BiaoTiInfos = BiaoTis;
            var Ls = new List<LineInfo>();
            foreach (LineInfo mdl in this.lines)
            {
                if (mdl.Line.Visible)
                {
                    Ls.Add(mdl);
                }
            }
            Anplt.LineInfos = Ls;
            return Anplt;
        }

        private void FrmDesignTemplate_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void FrmDesignTemplate_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Delete:
                    TS_ItemClicked(TS, new ToolStripItemClickedEventArgs(TbtnDelete));
                    break;
                default:
                    break;
            }
            Des.DesignByKeyDown(e);
        }

        private void TS_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "TbtnSave":
                    PrintSet.SaveTemplate(GetAnTemplateInfo(), ReportMember.reportFile);
                    break;
                case "TbtnPageSetting":
                    Des.SelectedNo();
                    if (frmPageSetting.ShowDialog() == DialogResult.OK) { PageSettingChanged(); }
                    break;
                case "TbtnBiaoTou":
                    Des.SelectedNo();
                    frmChooseBiaoTou.ShowDialog();
                    break;
                case "TbtnBiaoTi":
                    Des.SelectedNo();
                    frmChooseBiaoTi.ShowDialog();
                    break;
                case "TbtnGrid":
                    var frm = new frmGridSize(frmPageSetting.TemplateInfo.pnl);
                    frm.ShowDialog();
                    break;
                case "TbtnLine":
                    foreach (var item in this.lines)
                    {
                        if (!item.Line.Visible)
                        {
                            item.Line.Visible = true;
                            break;
                        }
                    }
                    break;
                case "TbtnFont":
                    if (Des.SelectedObjects.ExistItem())
                    {
                        FontDialog f = new()
                        {
                            Font = Des.SelectedObject.Font
                        };
                        if (f.ShowDialog() == DialogResult.OK)
                        {
                            Des.SetFont(f.Font);
                        }
                    }
                    break;
                case "TbtnDelete":
                    foreach (var item in Des.SelectedObjects)
                    {
                        if (item != frmPageSetting.TemplateInfo.pnl)
                        {
                            item.Visible = false;
                        }
                    }
                    Des.SelectedNo();
                    break;
                case "TbtnExit":
                    ReportMember.reportFile = null;
                    ReportMember.dtBiaoTou = null;
                    ReportMember.dtBiaoTi = null;
                    this.Close();
                    break;
                default:
                    break;
            }
        }

        private void TMenuAlignLeft_Click(object sender, EventArgs e)
        {
            Des.AlignLeft();
        }

        private void TMenuAlignRight_Click(object sender, EventArgs e)
        {
            Des.AlignRight();
        }

        private void TMenuAlignTop_Click(object sender, EventArgs e)
        {
            Des.AlignTop();
        }

        private void TMenuAlignBottom_Click(object sender, EventArgs e)
        {
            Des.AlignBottom();
        }

        private void TbtnAlign_DropDownOpening(object sender, EventArgs e)
        {
            foreach (ToolStripItem item in TbtnAlign.DropDownItems)
            {
                item.Enabled = Des.SelectedObjects.Count >= 2;
            }
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            DrawRule();
        }
    }
}
