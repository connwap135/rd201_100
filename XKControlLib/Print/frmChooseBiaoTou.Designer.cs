﻿namespace XKControlLib.Print
{
    partial class frmChooseBiaoTou
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.cboFormat = new System.Windows.Forms.ComboBox();
            this.cboDefinition = new System.Windows.Forms.ComboBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.btnSelectNo = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.dg1 = new System.Windows.Forms.DataGridView();
            this.cl_项目名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cl_项目别名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cl_显示 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dg1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(385, 134);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 27;
            this.btnCancel.Text = "取消(&C)";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(385, 105);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 26;
            this.btnOK.Text = "确定(&O)";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // cboFormat
            // 
            this.cboFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFormat.FormattingEnabled = true;
            this.cboFormat.Items.AddRange(new object[] {
            "无",
            "0",
            "0.0",
            "0.00",
            "0.000",
            "0.0000",
            "000,000",
            "000,000.0",
            "000,000.00",
            "000,000.000",
            "000,000.0000",
            "大写金额"});
            this.cboFormat.Location = new System.Drawing.Point(379, 225);
            this.cboFormat.Name = "cboFormat";
            this.cboFormat.Size = new System.Drawing.Size(94, 20);
            this.cboFormat.TabIndex = 24;
            this.cboFormat.TextChanged += new System.EventHandler(this.cboFormat_TextChanged);
            // 
            // cboDefinition
            // 
            this.cboDefinition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDefinition.FormattingEnabled = true;
            this.cboDefinition.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4"});
            this.cboDefinition.Location = new System.Drawing.Point(379, 187);
            this.cboDefinition.Name = "cboDefinition";
            this.cboDefinition.Size = new System.Drawing.Size(94, 20);
            this.cboDefinition.TabIndex = 25;
            this.cboDefinition.TextChanged += new System.EventHandler(this.cboDefinition_TextChanged);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(378, 210);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(83, 12);
            this.Label2.TabIndex = 22;
            this.Label2.Text = "格式化字符串:";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(378, 171);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(71, 12);
            this.Label1.TabIndex = 23;
            this.Label1.Text = "精确小数位:";
            // 
            // btnSelectNo
            // 
            this.btnSelectNo.Location = new System.Drawing.Point(385, 58);
            this.btnSelectNo.Name = "btnSelectNo";
            this.btnSelectNo.Size = new System.Drawing.Size(75, 23);
            this.btnSelectNo.TabIndex = 20;
            this.btnSelectNo.Text = "全清(&D)";
            this.btnSelectNo.UseVisualStyleBackColor = true;
            this.btnSelectNo.Click += new System.EventHandler(this.btnSelectNo_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(385, 29);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(75, 23);
            this.btnSelectAll.TabIndex = 21;
            this.btnSelectAll.Text = "全选(&A)";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // dg1
            // 
            this.dg1.AllowUserToAddRows = false;
            this.dg1.BackgroundColor = System.Drawing.Color.White;
            this.dg1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dg1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cl_项目名称,
            this.cl_项目别名,
            this.cl_显示});
            this.dg1.Location = new System.Drawing.Point(12, 12);
            this.dg1.Name = "dg1";
            this.dg1.RowHeadersVisible = false;
            this.dg1.RowTemplate.Height = 23;
            this.dg1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dg1.Size = new System.Drawing.Size(359, 358);
            this.dg1.TabIndex = 19;
            this.dg1.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg1_CellEnter);
            // 
            // cl_项目名称
            // 
            this.cl_项目名称.DataPropertyName = "项目名称";
            this.cl_项目名称.HeaderText = "项目名称";
            this.cl_项目名称.Name = "cl_项目名称";
            this.cl_项目名称.ReadOnly = true;
            this.cl_项目名称.Width = 130;
            // 
            // cl_项目别名
            // 
            this.cl_项目别名.DataPropertyName = "项目别名";
            this.cl_项目别名.HeaderText = "项目别名";
            this.cl_项目别名.Name = "cl_项目别名";
            this.cl_项目别名.Width = 150;
            // 
            // cl_显示
            // 
            this.cl_显示.DataPropertyName = "显示";
            this.cl_显示.HeaderText = "显示";
            this.cl_显示.Name = "cl_显示";
            this.cl_显示.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cl_显示.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cl_显示.Width = 75;
            // 
            // frmChooseBiaoTou
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 380);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.cboFormat);
            this.Controls.Add(this.cboDefinition);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.btnSelectNo);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.dg1);
            this.Name = "frmChooseBiaoTou";
            this.Text = "表头设置";
            this.Load += new System.EventHandler(this.frmChooseBiaoTou_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dg1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOK;
        internal System.Windows.Forms.ComboBox cboFormat;
        internal System.Windows.Forms.ComboBox cboDefinition;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button btnSelectNo;
        internal System.Windows.Forms.Button btnSelectAll;
        internal System.Windows.Forms.DataGridView dg1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cl_项目名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn cl_项目别名;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cl_显示;
    }
}