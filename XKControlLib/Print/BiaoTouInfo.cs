﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XKControlLib.Print
{
    public class BiaoTouInfo
    {
        public int A01_Id { get; set; }
        public int A02_TemplateId { get; set; }
        public string A03_cColumnName { get; set; }
        private string _A04_cColumnByName;
        public string A04_cColumnByName
        {
            get => _A04_cColumnByName;
            set
            {
                _A04_cColumnByName = value;
                lbl.Text = value;
            }
        }
        private int _A05_iLeft;
        public int A05_iLeft
        {
            get => _A05_iLeft;
            set
            {
                _A05_iLeft = value;
                lbl.Left = value;
            }
        }
        private int _A06_iTop;
        public int A06_iTop
        {
            get => _A06_iTop;
            set
            {
                _A06_iTop = value;
                lbl.Top = value;
            }
        }
        private int _A07_iWidth;
        public int A07_iWidth
        {
            get => _A07_iWidth;
            set
            {
                _A07_iWidth = value;
                lbl.Width = value;
            }
        }
        private int _A08_iHeight;
        public int A08_iHeight
        {
            get => _A08_iHeight;
            set
            {
                _A08_iHeight = value;
                lbl.Height = value;
            }
        }
        private int _A09_iFontSize;
        public int A09_iFontSize
        {
            get => _A09_iFontSize;
            set
            {
                _A09_iFontSize = value;
                ChangeFont();
            }
        }
        private void ChangeFont()
        {
            var FS = A09_iFontSize;
            FontStyle FontStyle = FontStyle.Regular;
            if (A10_bBold)
            {
                FontStyle |= FontStyle.Bold;
            }
            if (A11_bItalic)
            {
                FontStyle |= FontStyle.Italic;
            }
            if (A12_bUnderLine)
            {
                FontStyle |= FontStyle.Underline;
            }
            var font = new Font("宋体", FS, FontStyle);
            lbl.Font = font;
        }
        private bool _A10_bBold;
        public bool A10_bBold
        {
            get => _A10_bBold;
            set
            {
                _A10_bBold = value;
                ChangeFont();
            }
        }
        private bool _A11_bItalic;
        public bool A11_bItalic
        {
            get => _A11_bItalic;
            set
            {
                _A11_bItalic = value;
                ChangeFont();
            }
        }
        private bool _A12_bUnderLine;
        public bool A12_bUnderLine
        {
            get => _A12_bUnderLine;
            set
            {
                _A12_bUnderLine = value;
                ChangeFont();
            }
        }
        public string A13_iNumberDefinition { get; set; }
        public string A14_cNumberFormat { get; set; }
        private bool _A15_bShow;
        public bool A15_bShow
        {
            get => _A15_bShow;
            set
            {
                _A15_bShow = value;
                lbl.Visible = value;
            }
        }
        public bool A16_bDouble { get; set; }

        internal Label lbl = new Label();
        internal static PropertyName PN => new PropertyName(true);
        internal static PropertyName PM => new PropertyName(false);
        internal PropertyNuke Nuke => new PropertyNuke(this);

        public BiaoTouInfo()
        {
            lbl.FontChanged += Lbl_FontChanged;
            lbl.LocationChanged += Lbl_LocationChanged;
            lbl.SizeChanged += Lbl_SizeChanged;
            lbl.VisibleChanged += Lbl_VisibleChanged;
            lbl.Name = "表头" + A01_Id;
        }

        private void Lbl_VisibleChanged(object sender, EventArgs e)
        {
            _A15_bShow = lbl.Visible;
        }

        private void Lbl_SizeChanged(object sender, EventArgs e)
        {
            _A07_iWidth = lbl.Width;
            _A08_iHeight = lbl.Height;
        }

        private void Lbl_LocationChanged(object sender, EventArgs e)
        {
            _A05_iLeft = lbl.Left;
            _A06_iTop = lbl.Top;
        }

        private void Lbl_FontChanged(object sender, EventArgs e)
        {
            _A09_iFontSize = (int)lbl.Font.Size;
            _A10_bBold = lbl.Font.Bold;
            _A11_bItalic = lbl.Font.Italic;
            _A12_bUnderLine = lbl.Font.Underline;
        }

        internal class PropertyName
        {
            private readonly bool v;
            public PropertyName(bool v)
            {
                this.v = v;
            }
            public string A01_Id => v ? "A01_Id" : "编号";
            public string A02_TemplateId => v ? "A02_TemplateId" : "模板标识";
            public string A03_cColumnName => v ? "A03_cColumnName" : "列名";
            public string A04_cColumnByName => v ? "A04_cColumnByName" : "别名";
            public string A05_iLeft => v ? "A05_iLeft" : "左坐标";
            public string A06_iTop => v ? "A06_iTop" : "上坐标";
            public string A07_iWidth => v ? "A07_iWidth" : "宽度";
            public string A08_iHeight => v ? "A08_iHeight" : "高度";
            public string A09_iFontSize => v ? "A09_iFontSize" : "字号";
            public string A10_bBold => v ? "A10_bBold" : "是否粗体";
            public string A11_bItatlic => v ? "A11_bItatlic" : "是否斜体";
            public string A12_bUnderLine => v ? "A12_bUnderLine" : "是否下划线";
            public string A13_iNumberDefinition => v ? "A13_iNumberDefinition" : "精确小数位";
            public string A14_cNumberFormat => v ? "A14_cNumberFormat" : "格式化字符串";
            public string A15_bShow => v ? "A15_bShow" : "是否显示";
            public string A16_bDouble => v ? "A16_bDouble" : "是否Double类型";
        }

        internal class PropertyNuke
        {
            private BiaoTouInfo mdl;

            public PropertyNuke(BiaoTouInfo v)
            {
                this.mdl = v;
            }

            public int A01_Id
            {
                get => mdl.A01_Id;
                set
                {
                    mdl.A01_Id = value;
                }
            }

        }


    }
}
