﻿namespace XKControlLib.Print
{
    partial class frmDesignTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDesignTemplate));
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.TbtnDelete = new System.Windows.Forms.ToolStripButton();
            this.TbtnFont = new System.Windows.Forms.ToolStripButton();
            this.TMenuAlignBottom = new System.Windows.Forms.ToolStripMenuItem();
            this.TMenuAlignTop = new System.Windows.Forms.ToolStripMenuItem();
            this.TMenuAlignRight = new System.Windows.Forms.ToolStripMenuItem();
            this.TMenuAlignLeft = new System.Windows.Forms.ToolStripMenuItem();
            this.TbtnExit = new System.Windows.Forms.ToolStripButton();
            this.TbtnAlign = new System.Windows.Forms.ToolStripDropDownButton();
            this.TbtnGrid = new System.Windows.Forms.ToolStripButton();
            this.TbtnBiaoTi = new System.Windows.Forms.ToolStripButton();
            this.TbtnBiaoTou = new System.Windows.Forms.ToolStripButton();
            this.TbtnPageSetting = new System.Windows.Forms.ToolStripButton();
            this.TbtnSave = new System.Windows.Forms.ToolStripButton();
            this.TS = new System.Windows.Forms.ToolStrip();
            this.TbtnLine = new System.Windows.Forms.ToolStripButton();
            this.PnlMain = new System.Windows.Forms.Panel();
            this.TS.SuspendLayout();
            this.SuspendLayout();
            // 
            // Timer1
            // 
            this.Timer1.Enabled = true;
            this.Timer1.Interval = 1000;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // TbtnDelete
            // 
            this.TbtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("TbtnDelete.Image")));
            this.TbtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnDelete.Name = "TbtnDelete";
            this.TbtnDelete.Size = new System.Drawing.Size(69, 21);
            this.TbtnDelete.Text = "删除(&D)";
            // 
            // TbtnFont
            // 
            this.TbtnFont.Image = ((System.Drawing.Image)(resources.GetObject("TbtnFont.Image")));
            this.TbtnFont.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnFont.Name = "TbtnFont";
            this.TbtnFont.Size = new System.Drawing.Size(66, 21);
            this.TbtnFont.Text = "字体(&F)";
            // 
            // TMenuAlignBottom
            // 
            this.TMenuAlignBottom.Image = ((System.Drawing.Image)(resources.GetObject("TMenuAlignBottom.Image")));
            this.TMenuAlignBottom.Name = "TMenuAlignBottom";
            this.TMenuAlignBottom.Size = new System.Drawing.Size(112, 22);
            this.TMenuAlignBottom.Text = "下对齐";
            this.TMenuAlignBottom.Click += new System.EventHandler(this.TMenuAlignBottom_Click);
            // 
            // TMenuAlignTop
            // 
            this.TMenuAlignTop.Image = ((System.Drawing.Image)(resources.GetObject("TMenuAlignTop.Image")));
            this.TMenuAlignTop.Name = "TMenuAlignTop";
            this.TMenuAlignTop.Size = new System.Drawing.Size(112, 22);
            this.TMenuAlignTop.Text = "上对齐";
            this.TMenuAlignTop.Click += new System.EventHandler(this.TMenuAlignTop_Click);
            // 
            // TMenuAlignRight
            // 
            this.TMenuAlignRight.Image = ((System.Drawing.Image)(resources.GetObject("TMenuAlignRight.Image")));
            this.TMenuAlignRight.Name = "TMenuAlignRight";
            this.TMenuAlignRight.Size = new System.Drawing.Size(112, 22);
            this.TMenuAlignRight.Text = "右对齐";
            this.TMenuAlignRight.Click += new System.EventHandler(this.TMenuAlignRight_Click);
            // 
            // TMenuAlignLeft
            // 
            this.TMenuAlignLeft.Image = ((System.Drawing.Image)(resources.GetObject("TMenuAlignLeft.Image")));
            this.TMenuAlignLeft.Name = "TMenuAlignLeft";
            this.TMenuAlignLeft.Size = new System.Drawing.Size(112, 22);
            this.TMenuAlignLeft.Text = "左对齐";
            this.TMenuAlignLeft.Click += new System.EventHandler(this.TMenuAlignLeft_Click);
            // 
            // TbtnExit
            // 
            this.TbtnExit.Image = ((System.Drawing.Image)(resources.GetObject("TbtnExit.Image")));
            this.TbtnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnExit.Name = "TbtnExit";
            this.TbtnExit.Size = new System.Drawing.Size(70, 21);
            this.TbtnExit.Text = "退出(&Q)";
            // 
            // TbtnAlign
            // 
            this.TbtnAlign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.TbtnAlign.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TMenuAlignLeft,
            this.TMenuAlignRight,
            this.TMenuAlignTop,
            this.TMenuAlignBottom});
            this.TbtnAlign.Image = ((System.Drawing.Image)(resources.GetObject("TbtnAlign.Image")));
            this.TbtnAlign.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnAlign.Name = "TbtnAlign";
            this.TbtnAlign.Size = new System.Drawing.Size(45, 21);
            this.TbtnAlign.Text = "对齐";
            this.TbtnAlign.DropDownOpening += new System.EventHandler(this.TbtnAlign_DropDownOpening);
            // 
            // TbtnGrid
            // 
            this.TbtnGrid.Image = ((System.Drawing.Image)(resources.GetObject("TbtnGrid.Image")));
            this.TbtnGrid.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnGrid.Name = "TbtnGrid";
            this.TbtnGrid.Size = new System.Drawing.Size(91, 21);
            this.TbtnGrid.Text = "表格大小(&S)";
            // 
            // TbtnBiaoTi
            // 
            this.TbtnBiaoTi.Image = ((System.Drawing.Image)(resources.GetObject("TbtnBiaoTi.Image")));
            this.TbtnBiaoTi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnBiaoTi.Name = "TbtnBiaoTi";
            this.TbtnBiaoTi.Size = new System.Drawing.Size(93, 21);
            this.TbtnBiaoTi.Text = "表体项目(&G)";
            // 
            // TbtnBiaoTou
            // 
            this.TbtnBiaoTou.Image = ((System.Drawing.Image)(resources.GetObject("TbtnBiaoTou.Image")));
            this.TbtnBiaoTou.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnBiaoTou.Name = "TbtnBiaoTou";
            this.TbtnBiaoTou.Size = new System.Drawing.Size(93, 21);
            this.TbtnBiaoTou.Text = "表头项目(&H)";
            // 
            // TbtnPageSetting
            // 
            this.TbtnPageSetting.Image = ((System.Drawing.Image)(resources.GetObject("TbtnPageSetting.Image")));
            this.TbtnPageSetting.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnPageSetting.Name = "TbtnPageSetting";
            this.TbtnPageSetting.Size = new System.Drawing.Size(91, 21);
            this.TbtnPageSetting.Text = "页面设置(&P)";
            // 
            // TbtnSave
            // 
            this.TbtnSave.Image = ((System.Drawing.Image)(resources.GetObject("TbtnSave.Image")));
            this.TbtnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnSave.Name = "TbtnSave";
            this.TbtnSave.Size = new System.Drawing.Size(67, 21);
            this.TbtnSave.Text = "保存(&S)";
            // 
            // TS
            // 
            this.TS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TbtnSave,
            this.TbtnPageSetting,
            this.TbtnBiaoTou,
            this.TbtnBiaoTi,
            this.TbtnGrid,
            this.TbtnLine,
            this.TbtnAlign,
            this.TbtnFont,
            this.TbtnDelete,
            this.TbtnExit});
            this.TS.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.TS.Location = new System.Drawing.Point(0, 0);
            this.TS.Name = "TS";
            this.TS.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.TS.Size = new System.Drawing.Size(767, 24);
            this.TS.TabIndex = 4;
            this.TS.Text = "ToolStrip1";
            this.TS.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.TS_ItemClicked);
            // 
            // TbtnLine
            // 
            this.TbtnLine.Image = ((System.Drawing.Image)(resources.GetObject("TbtnLine.Image")));
            this.TbtnLine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnLine.Name = "TbtnLine";
            this.TbtnLine.Size = new System.Drawing.Size(66, 21);
            this.TbtnLine.Text = "直线(&L)";
            // 
            // PnlMain
            // 
            this.PnlMain.AutoScroll = true;
            this.PnlMain.BackColor = System.Drawing.Color.White;
            this.PnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PnlMain.Location = new System.Drawing.Point(0, 24);
            this.PnlMain.Name = "PnlMain";
            this.PnlMain.Size = new System.Drawing.Size(767, 426);
            this.PnlMain.TabIndex = 5;
            this.PnlMain.Paint += new System.Windows.Forms.PaintEventHandler(this.PnlMain_Paint);
            // 
            // frmDesignTemplate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 450);
            this.Controls.Add(this.PnlMain);
            this.Controls.Add(this.TS);
            this.Name = "frmDesignTemplate";
            this.Text = "打印模板设计";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmDesignTemplate_FormClosing);
            this.Load += new System.EventHandler(this.FrmDesignTemplate_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmDesignTemplate_KeyDown);
            this.TS.ResumeLayout(false);
            this.TS.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Timer Timer1;
        internal System.Windows.Forms.ToolStripButton TbtnDelete;
        internal System.Windows.Forms.ToolStripButton TbtnFont;
        internal System.Windows.Forms.ToolStripMenuItem TMenuAlignBottom;
        internal System.Windows.Forms.ToolStripMenuItem TMenuAlignTop;
        internal System.Windows.Forms.ToolStripMenuItem TMenuAlignRight;
        internal System.Windows.Forms.ToolStripMenuItem TMenuAlignLeft;
        internal System.Windows.Forms.ToolStripButton TbtnExit;
        internal System.Windows.Forms.ToolStripDropDownButton TbtnAlign;
        internal System.Windows.Forms.ToolStripButton TbtnGrid;
        internal System.Windows.Forms.ToolStripButton TbtnBiaoTi;
        internal System.Windows.Forms.ToolStripButton TbtnBiaoTou;
        internal System.Windows.Forms.ToolStripButton TbtnPageSetting;
        internal System.Windows.Forms.ToolStripButton TbtnSave;
        internal System.Windows.Forms.ToolStrip TS;
        internal System.Windows.Forms.ToolStripButton TbtnLine;
        internal System.Windows.Forms.Panel PnlMain;
    }
}