﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XKControlLib.Print
{
    public partial class frmChooseBiaoTi : Form
    {
        public frmChooseBiaoTi()
        {
            InitializeComponent();
        }
        private List<BiaoTiInfo> BK = new List<BiaoTiInfo>();
        public List<BiaoTiInfo> BiaoTiInfos { get; set; } = GetBiaoTiInfoCollection();
        private static List<BiaoTiInfo> GetBiaoTiInfoCollection() {
            var mdls = new List<BiaoTiInfo>();
            if (!ReportMember.ExistBiaoTi()) { return mdls; }
            var dtBiaoTi = ReportMember.dtBiaoTi;
            foreach (DataColumn col in dtBiaoTi.Columns)
            {
                var Index = dtBiaoTi.Columns.IndexOf(col);
                var mdl = new BiaoTiInfo();
                mdl.A03_cColumnName = col.ColumnName;
                mdl.A04_cColumnByName = col.ColumnName;
                mdl.A05_iLeft = Index * 100;
                mdl.A06_iWidth = 100;
                mdl.A07_iFontSize = 9;
                mdl.A08_iAlign = 0;
                mdl.A09_iNumberDefinition = "4";
                mdl.A10_cNumberFormat = "";
                mdl.A11_bShow = false;
                mdl.A12_bDouble = col.DataType == typeof(double);
                mdl.lbl.Left = Index * 100;
                mdl.lbl.Width = 100;
                mdl.lbl.BorderStyle = BorderStyle.FixedSingle;
                mdl.lbl.TextAlign = ContentAlignment.MiddleCenter;
                mdls.Add(mdl);
            }
            return mdls;
        }
        private void RefreshData()
        {
            dg1.AutoGenerateColumns = false;
            BiaoTiInfo.PropertyName PN = BiaoTiInfo.PN;
            cl_项目名称.DataPropertyName = PN.A03_cColumnName;
            cl_项目别名.DataPropertyName = PN.A04_cColumnByName;
            cl_显示.DataPropertyName = PN.A11_bShow;
            dg1.DataSource = BiaoTiInfos;
        }
        private void BackUp()
        {
            BK.Clear();
            foreach (var mdl in BiaoTiInfos)
            {
                var m = new BiaoTiInfo();
                m.A03_cColumnName = mdl.A03_cColumnName;
                m.A04_cColumnByName = mdl.A04_cColumnByName;
                m.A05_iLeft = mdl.A05_iLeft;
                m.A06_iWidth = mdl.A06_iWidth;
                m.A07_iFontSize = mdl.A07_iFontSize;
                m.A08_iAlign = mdl.A08_iAlign;
                m.A09_iNumberDefinition = mdl.A09_iNumberDefinition;
                m.A10_cNumberFormat = mdl.A10_cNumberFormat;
                m.A11_bShow = mdl.A11_bShow;
                BK.Add(m);
            }
        }
        
        private void ComeBack()
        {
            for (int i = 0; i < BiaoTiInfos.Count; i++)
            {
                var mdl = BiaoTiInfos[i];
                var mdl2  = BK[i];
                mdl.A03_cColumnName = mdl2.A03_cColumnName;
                mdl.A04_cColumnByName = mdl2.A04_cColumnByName;
                mdl.A05_iLeft = mdl2.A05_iLeft;
                mdl.A06_iWidth = mdl2.A06_iWidth;
                mdl.A07_iFontSize = mdl2.A07_iFontSize;
                mdl.A08_iAlign = mdl2.A08_iAlign;
                mdl.A09_iNumberDefinition = mdl2.A09_iNumberDefinition;
                mdl.A10_cNumberFormat = mdl2.A10_cNumberFormat;
                mdl.A11_bShow = mdl2.A11_bShow;
            }
        }

        private void frmChooseBiaoTi_Load(object sender, EventArgs e)
        {
            BackUp();
            RefreshData();
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (var mdl in BiaoTiInfos)
            {
                mdl.A11_bShow = true;
            }
            LayoutLabel();
            dg1.Refresh();
        }

        private void LayoutLabel()
        {
            for (int i = 0; i < BiaoTiInfos.Count; i++)
            {
                var mdl = BiaoTiInfos[i];
                mdl.A05_iLeft = getWidthTotal(i) - mdl.A06_iWidth;
            }
        }

        private int getWidthTotal(int Modelindex)
        {
            int Interval = 0;
            for (int i  = 0; i  <= Modelindex; i ++)
            {
                if (BiaoTiInfos[i].A11_bShow)
                {
                    Interval += BiaoTiInfos[i].A06_iWidth;
                }
            }
            return Interval;
        }

        private void btnSelectNo_Click(object sender, EventArgs e)
        {
            foreach (var mdl in BiaoTiInfos)
            {
                mdl.A11_bShow = false;
            }
            LayoutLabel();
            dg1.Refresh();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ComeBack();
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void dg1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dg1.CurrentRow == null) { return; }
            BiaoTiInfo mdl = (BiaoTiInfo)dg1.CurrentRow.DataBoundItem;
            cboDefinition.Enabled = mdl.A12_bDouble;
            cboFormat.Enabled = mdl.A12_bDouble;
            cboDefinition.Text = mdl.A09_iNumberDefinition;
            if (string.IsNullOrEmpty(mdl.A10_cNumberFormat))
            {
                cboFormat.Text = "无";
            }
            else
            {
                cboFormat.Text = mdl.A10_cNumberFormat;
            }
            txtWidth.Text = mdl.A06_iWidth.ToString();
            cboAlign.SelectedIndex = mdl.A08_iAlign;
            txtFontSize.Text = mdl.A07_iFontSize.ToString();
        }

        private void cboDefinition_TextChanged(object sender, EventArgs e)
        {
            if (dg1.CurrentRow == null) { return; }
            BiaoTiInfo mdl = (BiaoTiInfo)dg1.CurrentRow.DataBoundItem;
            if (int.TryParse(cboDefinition.Text ,out int result))
            {
                mdl.A09_iNumberDefinition = cboDefinition.Text;
            }
            else
            {
                mdl.A09_iNumberDefinition = "4";
            }
        }

        private void cboFormat_TextChanged(object sender, EventArgs e)
        {
            if (dg1.CurrentRow == null) { return; }
            BiaoTiInfo mdl = (BiaoTiInfo)dg1.CurrentRow.DataBoundItem;
            if (cboFormat.Text == "无")
            {
                mdl.A10_cNumberFormat = "";
            }
            else
            {
                mdl.A10_cNumberFormat = cboFormat.Text;
            }
        }

        private void txtWidth_TextChanged(object sender, EventArgs e)
        {
            if(int.TryParse(txtWidth.Text,out int result))
            {
                if(result < 0 || result > 1000)
                {
                    txtWidth.Text = "100";
                    txtWidth.SelectAll();
                }
            }
            else
            {
                txtWidth.Text = "100";
                txtWidth.SelectAll();
            }
            if (dg1.CurrentRow == null) { return; }
            BiaoTiInfo mdl = (BiaoTiInfo)dg1.CurrentRow.DataBoundItem;
            mdl.A06_iWidth = int.Parse(txtWidth.Text);
            LayoutLabel();
        }

        private void cboAlign_TextChanged(object sender, EventArgs e)
        {
            if (dg1.CurrentRow == null) { return; }
            BiaoTiInfo mdl = (BiaoTiInfo)dg1.CurrentRow.DataBoundItem;
            mdl.A08_iAlign = cboAlign.SelectedIndex;
        }

        private void dg1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dg1.Columns[e.ColumnIndex] == cl_显示) {
                LayoutLabel();
            }
        }

        private void txtFontSize_TextChanged(object sender, EventArgs e)
        {
            if (int.TryParse(txtFontSize.Text, out int result))
            {
                if (result < 1 || result > 50)
                {
                    txtFontSize.Text = "9";
                    txtFontSize.SelectAll();
                }
            }
            else
            {
                txtFontSize.Text = "9";
                txtFontSize.SelectAll();
            }
            if (dg1.CurrentRow == null) { return; }
            BiaoTiInfo mdl = (BiaoTiInfo)dg1.CurrentRow.DataBoundItem;
            mdl.A07_iFontSize = int.Parse(txtFontSize.Text);
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            if (dg1.CurrentRow == null) { return; }
            var RowIndex = dg1.CurrentRow.Index;
            var mdl = BiaoTiInfos[RowIndex];
            var mdl2 = BK[RowIndex];
            switch (((CButton)sender).Name)
            {
                case "btnFirst":
                    BiaoTiInfos.Remove(mdl);
                    BiaoTiInfos.Insert(0, mdl);
                    BK.Remove(mdl2);
                    BK.Insert(0, mdl2);
                    break;
                case "btnPrevious":
                    int index = BiaoTiInfos.IndexOf(mdl);
                    if (index > 0)
                    {
                        index -= 1;
                        BiaoTiInfos.Remove(mdl);
                        BiaoTiInfos.Insert(index, mdl);
                        BK.Remove(mdl2);
                        BK.Insert(index, mdl2);
                    }
                    break;
                case "btnNext":
                    int index1 = BiaoTiInfos.IndexOf(mdl);
                    if( index1<BiaoTiInfos.Count - 1)
                    {
                        index1 += 1;
                        if(index1 == BiaoTiInfos.Count - 1)
                        {
                            btnFirst_Click(btnLast, new System.EventArgs());
                        }
                        else
                        {
                            BiaoTiInfos.Remove(mdl);
                            BiaoTiInfos.Insert(index1, mdl);
                            BK.Remove(mdl2);
                            BK.Insert(index1, mdl2);
                        }
                    }
                    break;
                case "btnLast":
                    BiaoTiInfos.Remove(mdl);
                    BiaoTiInfos.Add(mdl);
                    BK.Remove(mdl2);
                    BK.Add(mdl2);
                    break;
                default:
                    break;
            }
            int ColumnIndex = dg1.CurrentCell.ColumnIndex;
            dg1.DataSource = null;
            dg1.DataSource = BiaoTiInfos;
            foreach (DataGridViewRow dr in dg1.Rows)
            {
                if (dr.DataBoundItem == mdl)
                {
                    dg1.CurrentCell = dr.Cells[ColumnIndex];
                }
            }
            LayoutLabel();
        }
    }
}
