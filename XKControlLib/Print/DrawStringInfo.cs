﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace XKControlLib.Print
{
    public class DrawStringInfo
    {
        public string cText { get; set; }
        public Color Color { get; set; }
        public int iLineAlign { get; set; }
        public int iAlign { get; set; }
        public int iFontSize { get; set; }
        public int iHeight { get; set; }
        public int iLeft { get; set; }
        public int iTop { get; set; }
        public int iWidth { get; set; }
        public bool bBold { get; set; }
        public bool bItalic { get; set; }
        public bool bUnderLine { get; set; }
        public DrawStringInfo Clone()
        {
            return MemberwiseClone() as DrawStringInfo;
        }
    }
}
