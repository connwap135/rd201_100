﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace XKControlLib.Print
{
    public class LineInfo
    {
        internal Label Line = new Label();
        private int _a03_x1;
        private int _a04_y1;
        private int _a05_x2;
        private int _a06_y2;

        public int A01_Id { get; set; }
        public int A02_TemplateId { get; set; }
        public int A03_x1
        {
            get => _a03_x1;
            set
            {
                _a03_x1 = value;
                Line.Left = value;
            }
        }
        public int A04_y1
        {
            get => _a04_y1;
            set
            {
                _a04_y1 = value;
                Line.Top = value;
            }
        }
        public int A05_x2
        {
            get => _a05_x2;
            set
            {
                _a05_x2 = value;
                Line.Width = _a05_x2 - _a03_x1;
            }
        }
        public int A06_y2
        {
            get => _a06_y2; set
            {
                _a06_y2 = value;
                Line.Height = 1;
            }
        }
        public bool Visible { get; set; }

        public LineInfo()
        {
            Line.LocationChanged += Line_LocationChanged;
            Line.SizeChanged += Line_LocationChanged;
            Line.Name = "Line" + A01_Id;
            Line.Visible = false;
            Line.Text = "";
            Line.Width = 100;
            Line.Height = 1;
            Line.BackColor = Color.Black;
            Line.Left = Rnd() + 50;
            Line.Top = Rnd() + 50;
        }

        private void Line_LocationChanged(object sender, EventArgs e)
        {
            if (Line.Height != 1)
            {
                Line.Height = 1;
            }
            A03_x1 = Line.Left;
            A04_y1 = Line.Top;
            A05_x2 = Line.Left + Line.Width;
            A06_y2 = Line.Top;
        }

        private int Rnd()
        {
            return new Random().Next(0, 100);
        }
    }
}