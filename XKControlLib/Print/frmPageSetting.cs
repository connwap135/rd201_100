﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XKControlLib.Print
{
    public partial class frmPageSetting : Form
    {
        private PrintDocument pd = new PrintDocument();
        private TemplateInfo BK = new TemplateInfo();

        public frmPageSetting()
        {
            InitializeComponent();
            FullPrintPages();
            TemplateInfo.A03_cTemplateName = "打印模板";
            TemplateInfo.A04_cPageName = "A4";
            TemplateInfo.A10_iDataHeaderHeight = 22;
            TemplateInfo.A11_iDataRowHeight = 22;
        }

        private void FullPrintPages()
        {
            cboPages.Items.Clear();
            foreach (PaperSize Page in pd.PrinterSettings.PaperSizes)
            {
                cboPages.Items.Add(Page.PaperName);
            }

        }

        public readonly TemplateInfo TemplateInfo = new TemplateInfo();

        private void frmPageSetting_Load(object sender, EventArgs e)
        {
            txtDataHeaderHeight.Enabled = ReportMember.ExistBiaoTi();
            txtDataRowHeight.Enabled = ReportMember.ExistBiaoTi();
            BackUp();
            RefreshData();
        }

        private void RefreshData()
        {
            cboPages.Text = TemplateInfo.A04_cPageName;
            if (TemplateInfo.A05_iPageWay == 0) {
                rdo1.Checked = true;
                rdo2.Checked = false;
            }
            else
            {
                rdo1.Checked = false;
                rdo2.Checked = true;
            }
            txtDataHeaderHeight.Text = TemplateInfo.A10_iDataHeaderHeight.ToString();
            txtDataRowHeight.Text = TemplateInfo.A11_iDataRowHeight.ToString();
            chkTaoDa.Checked = TemplateInfo.A12_bTaoDa;
        }

        private void BackUp()
        {
            BK.A04_cPageName = TemplateInfo.A04_cPageName;
            BK.A05_iPageWay = TemplateInfo.A05_iPageWay;
            BK.A11_iDataRowHeight = TemplateInfo.A11_iDataRowHeight;
            BK.A12_bTaoDa = TemplateInfo.A12_bTaoDa;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ComeBack();
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void ComeBack()
        {
            TemplateInfo.A12_bTaoDa = BK.A12_bTaoDa;
            TemplateInfo.A04_cPageName = BK.A04_cPageName;
            TemplateInfo.A11_iDataRowHeight = BK.A11_iDataRowHeight;
            TemplateInfo.A05_iPageWay = BK.A05_iPageWay;
        }

        private void Preview()
        {
            string cPageName = cboPages.Text;
            Preview(cPageName);
        }

        private void Preview(string cPageName)
        {
            foreach (PaperSize Page in pd.PrinterSettings.PaperSizes)
            {
                if (Page.PaperName.Equals(cPageName, StringComparison.CurrentCultureIgnoreCase))
                {
                    pnlPage.Visible = true;
                    Preview(Page);
                    return;
                }
            }
            pnlPage.Visible = false;
        }

        private void Preview(PaperSize Page)
        {
            this.pnlPage.Width = this.rdo1.Checked? Page.Width / 10: Page.Height / 10;
            this.pnlPage.Height = this.rdo1.Checked? Page.Height / 10: Page.Width / 10;
            this.pnlPage.Left = (Grp1.Width - this.pnlPage.Width) / 2;
            this.pnlPage.Top = (Grp1.Height - this.pnlPage.Height) / 2;
        }

        private void cboPages_TextChanged(object sender, EventArgs e)
        {
            this.TemplateInfo.A04_cPageName = cboPages.Text;
            this.Preview();
        }

        private void rdo1_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo1.Checked)
            {
                this.TemplateInfo.A05_iPageWay = 0;
            }
            else
            {
                this.TemplateInfo.A05_iPageWay = 1;
            }
            this.Preview();
        }

        private void txtDataHeaderHeight_TextChanged(object sender, EventArgs e)
        {
            TextBox txt = sender as TextBox;
            if (int.TryParse(txt.Text, out int result))
            {
                if(int.Parse(txt.Text)<1 || int.Parse(txt.Text) > 500)
                {
                    txt.Text = "18";
                    txt.SelectAll();
                }
            }
            else {
                txt.Text = "18";
                txt.SelectAll();
            }
            if(txt == txtDataHeaderHeight)
            {
                TemplateInfo.A10_iDataHeaderHeight = int.Parse(txt.Text);
            }
            else
            {
                TemplateInfo.A11_iDataRowHeight = int.Parse(txt.Text);
            }
        }

        private void chkTaoDa_CheckedChanged(object sender, EventArgs e)
        {
            TemplateInfo.A12_bTaoDa = chkTaoDa.Checked;
        }
    }
}
