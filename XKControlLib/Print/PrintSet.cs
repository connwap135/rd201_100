﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.IO.Compression;
using System.Windows.Forms;

namespace XKControlLib.Print
{
    public class PrintSet
    {
        public PrintSet(string filename)
        {
            ReportMember.reportFile = filename;
            DataSet ds = new DataSet();
            bool fileExists = System.IO.File.Exists(filename);
            if (fileExists)
            {
                using (ZipStorer zip = ZipStorer.Open(filename, FileAccess.Read))
                {
                    var dir = zip.ReadCentralDir();
                    foreach (var entry in dir)
                    {
                        if (Path.GetFileName(entry.FilenameInZip) == "1.txt")
                        {
                            string tempFile = Path.GetTempFileName();
                            zip.ExtractFile(entry, out byte[] ff);
                            File.WriteAllBytes(tempFile, ff);
                            ds.ReadXml(tempFile);
                            break;
                        }
                    }
                }
            }
            else
            {
                //Assembly ay = Assembly.GetExecutingAssembly();
                //Stream xmlStream = ay.GetManifestResourceStream(filename);
                //ds.ReadXml(xmlStream);

            }
            this.Datas = ds;
        }

        public void ShowDesign()
        {
            AnTemplateInfo info = new AnTemplateInfo
            {
                TemplateInfos = GetTemplate(),
                BiaoTouInfos = GetBiaoTou(),
                BiaoTiInfos = GetBiaoTi(),
                LineInfos = GetLine()
            };
            if (!ReportMember.ExistBiaoTou() && !ReportMember.ExistBiaoTi())
            {
                MessageBox.Show(@"没有'表头','表体'的模板将毫无意义");
            }
            var frm = new frmDesignTemplate(info);
            frm.Show();
        }

        public void ShowDesign(DataTable dtBiaoTou, DataTable dtBiaoTi = null, string reportID = "_report")
        {
            ReportMember.dtBiaoTou = dtBiaoTou;
            ReportMember.dtBiaoTi = dtBiaoTi;
            ReportMember.cReportId = reportID;
            ShowDesign();
        }

        public void ShowPreview(bool v = false)
        {
            AnTemplateInfo info = new AnTemplateInfo
            {
                TemplateInfos = GetTemplate(),
                BiaoTouInfos = GetBiaoTou(),
                BiaoTiInfos = GetBiaoTi(),
                LineInfos = GetLine()
            };
            var frm = new frmPreview(info, v);
            frm.Show();
        }

        public void ShowPreview(DataTable dtBiaoTou, DataTable dtBiaoTi = null, bool v = false)
        {
            ReportMember.dtBiaoTou = dtBiaoTou;
            ReportMember.dtBiaoTi = dtBiaoTi;
            ShowPreview(v);
        }

        public TemplateInfo GetTemplate()
        {
            var dt = Datas.Tables["Template"];
            if (dt != null)
            {
                DataRow dr = dt.Rows[0];
                TemplateInfo mdl = new TemplateInfo
                {
                    A01_TemplateId = int.Parse(dr["A01_TemplateId"].ToString()),
                    A02_cReportId = dr["A02_cReportId"].ToString(),
                    A03_cTemplateName = dr["A03_cTemplateName"].ToString(),
                    A04_cPageName = dr["A04_cPageName"].ToString(),
                    A05_iPageWay = int.Parse(dr["A05_iPageWay"].ToString()),
                    A06_iDataGridLeft = int.Parse(dr["A06_iDataGridLeft"].ToString()),
                    A07_iDataGridTop = int.Parse(dr["A07_iDataGridTop"].ToString()),
                    A08_iDataGridWidth = int.Parse(dr["A08_iDataGridWidth"].ToString()),
                    A09_iDataGridHeight = int.Parse(dr["A09_iDataGridHeight"].ToString()),
                    A10_iDataHeaderHeight = int.Parse(dr["A10_iDataHeaderHeight"].ToString()),
                    A11_iDataRowHeight = int.Parse(dr["A11_iDataRowHeight"].ToString()),
                    A12_bTaoDa = dr["A12_bTaoDa"].ToString().ToLower().Equals("true") ? true : false
                };
                return mdl;
            }
            return null;
        }

        public List<BiaoTouInfo> GetBiaoTou()
        {
            List<BiaoTouInfo> mdls = new List<BiaoTouInfo>();
            var dt = Datas.Tables["BiaoTou"];
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    BiaoTouInfo mdl = new BiaoTouInfo
                    {
                        A01_Id = int.Parse(dr["A01_Id"].ToString()),
                        A02_TemplateId = int.Parse(dr["A02_TemplateId"].ToString()),
                        A03_cColumnName = dr["A03_cColumnName"].ToString(),
                        A04_cColumnByName = dr["A04_cColumnByName"].ToString(),
                        A05_iLeft = int.Parse(dr["A05_iLeft"].ToString()),
                        A06_iTop = int.Parse(dr["A06_iTop"].ToString()),
                        A07_iWidth = int.Parse(dr["A07_iWidth"].ToString()),
                        A08_iHeight = int.Parse(dr["A08_iHeight"].ToString()),
                        A09_iFontSize = int.Parse(dr["A09_iFontSize"].ToString()),
                        A10_bBold = dr["A10_bBold"].ToString().ToLower().Equals("true") ? true : false,
                        A11_bItalic = dr["A11_bItalic"].ToString().ToLower().Equals("true") ? true : false,
                        A12_bUnderLine = dr["A12_bUnderLine"].ToString().ToLower().Equals("true") ? true : false,
                        A13_iNumberDefinition = dr["A13_iNumberDefinition"].ToString(),
                        A14_cNumberFormat = dr["A14_cNumberFormat"].ToString()
                    };
                    mdls.Add(mdl);
                }
            }
            return mdls;
        }

        public List<BiaoTiInfo> GetBiaoTi()
        {
            List<BiaoTiInfo> mdls = new List<BiaoTiInfo>();
            var dt = Datas.Tables["BiaoTi"];
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    BiaoTiInfo mdl = new BiaoTiInfo
                    {
                        A01_Id = int.Parse(dr["A01_Id"].ToString()),
                        A02_TemplateId = int.Parse(dr["A02_TemplateId"].ToString()),
                        A03_cColumnName = dr["A03_cColumnName"].ToString(),
                        A04_cColumnByName = dr["A04_cColumnByName"].ToString(),
                        A05_iLeft = int.Parse(dr["A05_iLeft"].ToString()),
                        A06_iWidth = int.Parse(dr["A06_iWidth"].ToString()),
                        A07_iFontSize = int.Parse(dr["A07_iFontSize"].ToString()),
                        A08_iAlign = int.Parse(dr["A08_iAlign"].ToString()),
                        A09_iNumberDefinition = dr["A09_iNumberDefinition"].ToString(),
                        A10_cNumberFormat = dr["A10_cNumberFormat"].ToString()
                    };
                    mdls.Add(mdl);
                }
            }
            return mdls;
        }

        public List<LineInfo> GetLine()
        {
            List<LineInfo> mdls = new List<LineInfo>();
            var dt = Datas.Tables["LineInfos"];
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    LineInfo mdl = new LineInfo
                    {
                        A01_Id = int.Parse(dr["A01_Id"].ToString()),
                        A02_TemplateId = int.Parse(dr["A02_TemplateId"].ToString()),
                        A03_x1 = int.Parse(dr["A03_x1"].ToString()),
                        A04_y1 = int.Parse(dr["A04_y1"].ToString()),
                        A05_x2 = int.Parse(dr["A05_x2"].ToString()),
                        A06_y2 = int.Parse(dr["A06_y2"].ToString())
                    };
                    mdls.Add(mdl);
                }
            }
            return mdls;
        }

        public static void SaveTemplate(AnTemplateInfo info, string filename = null)
        {
            if (filename == null) { filename = ReportMember.reportFile; }
            DataSet ds = new DataSet
            {
                DataSetName = "PrintSeting"
            };
            DataTable dt = Common.ConvertData.ToDataTable(info.TemplateInfos);
            if (dt != null)
            {
                dt.TableName = "Template";
                ds.Tables.Add(dt);
            }


            var dt1 = Common.ConvertData.ToDataTable(info.BiaoTouInfos);
            if (dt1 != null)
            {
                dt1.TableName = "BiaoTou";
                ds.Tables.Add(dt1);
            }


            var dt2 = Common.ConvertData.ToDataTable(info.BiaoTiInfos);
            if (dt2 != null)
            {
                dt2.TableName = "BiaoTi";
                ds.Tables.Add(dt2);
            }

            var dt3 = Common.ConvertData.ToDataTable(info.LineInfos);
            if (dt3 != null)
            {
                dt3.TableName = "LineInfos";
                ds.Tables.Add(dt3);
            }

            try
            {
                var array = Encoding.UTF8.GetBytes(ds.GetXml());
                using (ZipStorer zip = ZipStorer.Create(filename, "打印模板"))
                {
                    //zip.AddFile(ZipStorer.Compression.Deflate,filename,filename,"");
                    using (MemoryStream stream = new MemoryStream(array))
                    {
                        zip.AddStream(ZipStorer.Compression.Deflate, "1.txt", stream, DateTime.Now, null);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public DataSet Datas { get; set; }
    }
}
