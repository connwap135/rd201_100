﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XKControlLib.Print
{
    public partial class frmGridSize : Form
    {
        private Control dg;

        public frmGridSize()
        {
            InitializeComponent();
        }

        public frmGridSize(Control pnl)
        {
            InitializeComponent();
            this.dg = pnl;
        }

        private void frmGridSize_Load(object sender, EventArgs e)
        {
            txtWidth.Text = dg.Width.ToString();
            txtHeight.Text = dg.Height.ToString();
        }

        private void txtWidth_TextChanged(object sender, EventArgs e)
        {
            if (txtWidth.TextLength == 0) { return; }
            if (dg == null) { return; }
            try
            {
                dg.Width = int.Parse(txtWidth.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
           

        }

        private void txtHeight_TextChanged(object sender, EventArgs e)
        {
            if (txtHeight.TextLength == 0) { return; }
            if (dg == null) { return; }
            try
            {
                dg.Height = int.Parse(txtHeight.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
