﻿namespace XKControlLib.Print
{
    partial class frmPreview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPreview));
            this.PrintPreviewControl1 = new System.Windows.Forms.PrintPreviewControl();
            this.PrintDocument1 = new System.Drawing.Printing.PrintDocument();
            this.TS = new System.Windows.Forms.ToolStrip();
            this.TbtnPrint = new System.Windows.Forms.ToolStripButton();
            this.TbtnFirst = new System.Windows.Forms.ToolStripButton();
            this.TbtnPrevious = new System.Windows.Forms.ToolStripButton();
            this.TbtnNext = new System.Windows.Forms.ToolStripButton();
            this.TbtnLast = new System.Windows.Forms.ToolStripButton();
            this.ToolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.TcboSize = new System.Windows.Forms.ToolStripComboBox();
            this.TBtnCols = new System.Windows.Forms.ToolStripButton();
            this.TbtnExit = new System.Windows.Forms.ToolStripButton();
            this.TlblShow = new System.Windows.Forms.ToolStripLabel();
            this.PrintDialog1 = new System.Windows.Forms.PrintDialog();
            this.TS.SuspendLayout();
            this.SuspendLayout();
            // 
            // PrintPreviewControl1
            // 
            this.PrintPreviewControl1.AutoZoom = false;
            this.PrintPreviewControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PrintPreviewControl1.Location = new System.Drawing.Point(0, 25);
            this.PrintPreviewControl1.Name = "PrintPreviewControl1";
            this.PrintPreviewControl1.Size = new System.Drawing.Size(800, 425);
            this.PrintPreviewControl1.TabIndex = 3;
            this.PrintPreviewControl1.Zoom = 1D;
            this.PrintPreviewControl1.StartPageChanged += new System.EventHandler(this.PrintPreviewControl1_StartPageChanged);
            // 
            // PrintDocument1
            // 
            this.PrintDocument1.EndPrint += new System.Drawing.Printing.PrintEventHandler(this.PrintDocument1_EndPrint);
            this.PrintDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // TS
            // 
            this.TS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TbtnPrint,
            this.TbtnFirst,
            this.TbtnPrevious,
            this.TbtnNext,
            this.TbtnLast,
            this.ToolStripLabel1,
            this.TcboSize,
            this.TBtnCols,
            this.TbtnExit,
            this.TlblShow});
            this.TS.Location = new System.Drawing.Point(0, 0);
            this.TS.Name = "TS";
            this.TS.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.TS.Size = new System.Drawing.Size(800, 25);
            this.TS.TabIndex = 2;
            this.TS.Text = "ToolStrip1";
            this.TS.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.TS_ItemClicked);
            // 
            // TbtnPrint
            // 
            this.TbtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("TbtnPrint.Image")));
            this.TbtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnPrint.Name = "TbtnPrint";
            this.TbtnPrint.Size = new System.Drawing.Size(67, 22);
            this.TbtnPrint.Text = "打印(&P)";
            // 
            // TbtnFirst
            // 
            this.TbtnFirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TbtnFirst.Image = ((System.Drawing.Image)(resources.GetObject("TbtnFirst.Image")));
            this.TbtnFirst.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnFirst.Name = "TbtnFirst";
            this.TbtnFirst.Size = new System.Drawing.Size(23, 22);
            this.TbtnFirst.Text = "首张";
            // 
            // TbtnPrevious
            // 
            this.TbtnPrevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TbtnPrevious.Image = ((System.Drawing.Image)(resources.GetObject("TbtnPrevious.Image")));
            this.TbtnPrevious.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnPrevious.Name = "TbtnPrevious";
            this.TbtnPrevious.Size = new System.Drawing.Size(23, 22);
            this.TbtnPrevious.Text = "上张";
            // 
            // TbtnNext
            // 
            this.TbtnNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TbtnNext.Image = ((System.Drawing.Image)(resources.GetObject("TbtnNext.Image")));
            this.TbtnNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnNext.Name = "TbtnNext";
            this.TbtnNext.Size = new System.Drawing.Size(23, 22);
            this.TbtnNext.Text = "下张";
            // 
            // TbtnLast
            // 
            this.TbtnLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TbtnLast.Image = ((System.Drawing.Image)(resources.GetObject("TbtnLast.Image")));
            this.TbtnLast.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnLast.Name = "TbtnLast";
            this.TbtnLast.Size = new System.Drawing.Size(23, 22);
            this.TbtnLast.Text = "末张";
            // 
            // ToolStripLabel1
            // 
            this.ToolStripLabel1.Name = "ToolStripLabel1";
            this.ToolStripLabel1.Size = new System.Drawing.Size(32, 22);
            this.ToolStripLabel1.Text = "大小";
            // 
            // TcboSize
            // 
            this.TcboSize.Items.AddRange(new object[] {
            "100",
            "75",
            "50",
            "25"});
            this.TcboSize.Name = "TcboSize";
            this.TcboSize.Size = new System.Drawing.Size(75, 25);
            this.TcboSize.Text = "100";
            this.TcboSize.SelectedIndexChanged += new System.EventHandler(this.TcboSize_SelectedIndexChanged);
            // 
            // TBtnCols
            // 
            this.TBtnCols.Image = ((System.Drawing.Image)(resources.GetObject("TBtnCols.Image")));
            this.TBtnCols.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TBtnCols.Name = "TBtnCols";
            this.TBtnCols.Size = new System.Drawing.Size(76, 22);
            this.TBtnCols.Text = "多页预览";
            // 
            // TbtnExit
            // 
            this.TbtnExit.Image = ((System.Drawing.Image)(resources.GetObject("TbtnExit.Image")));
            this.TbtnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnExit.Name = "TbtnExit";
            this.TbtnExit.Size = new System.Drawing.Size(70, 22);
            this.TbtnExit.Text = "关闭(&Q)";
            // 
            // TlblShow
            // 
            this.TlblShow.Name = "TlblShow";
            this.TlblShow.Size = new System.Drawing.Size(74, 22);
            this.TlblShow.Text = "第1张 共1张";
            // 
            // PrintDialog1
            // 
            this.PrintDialog1.UseEXDialog = true;
            // 
            // frmPreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.PrintPreviewControl1);
            this.Controls.Add(this.TS);
            this.Name = "frmPreview";
            this.Text = "打印预览";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPreview_FormClosing);
            this.Load += new System.EventHandler(this.frmPreview_Load);
            this.TS.ResumeLayout(false);
            this.TS.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.PrintPreviewControl PrintPreviewControl1;
        internal System.Drawing.Printing.PrintDocument PrintDocument1;
        internal System.Windows.Forms.ToolStrip TS;
        internal System.Windows.Forms.ToolStripButton TbtnPrint;
        internal System.Windows.Forms.ToolStripButton TbtnFirst;
        internal System.Windows.Forms.ToolStripButton TbtnPrevious;
        internal System.Windows.Forms.ToolStripButton TbtnNext;
        internal System.Windows.Forms.ToolStripButton TbtnLast;
        internal System.Windows.Forms.ToolStripLabel ToolStripLabel1;
        internal System.Windows.Forms.ToolStripComboBox TcboSize;
        internal System.Windows.Forms.ToolStripButton TBtnCols;
        internal System.Windows.Forms.ToolStripButton TbtnExit;
        internal System.Windows.Forms.ToolStripLabel TlblShow;
        internal System.Windows.Forms.PrintDialog PrintDialog1;
    }
}