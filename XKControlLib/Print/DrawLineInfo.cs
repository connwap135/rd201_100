﻿namespace XKControlLib.Print
{
    public class DrawLineInfo
    {
        public DrawLineInfo()
        {
        }

        public int x1 { get; set; }
        public int y1 { get; internal set; }
        public int x2 { get; internal set; }
        public int y2 { get; internal set; }
    }
}