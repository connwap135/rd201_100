﻿namespace XKControlLib
{
    public struct StructRect
    {
        public int left;
        public int top;
        public int right;
        public int bottom;
    }
}