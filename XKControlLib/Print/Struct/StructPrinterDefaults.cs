﻿using System;

namespace XKControlLib
{
    public struct StructPrinterDefaults
    {
        public string pDatatype;
        public IntPtr pDevMode;
        public int DesiredAccess;
    }
}