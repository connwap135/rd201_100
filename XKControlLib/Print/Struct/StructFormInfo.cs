﻿namespace XKControlLib
{
    public struct StructFormInfo
    {
        public int Flags;
        public string pName;
        public StructSize Size;
        public StructRect ImageableArea;
    }
}