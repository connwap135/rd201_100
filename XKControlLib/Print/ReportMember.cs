﻿using System.Data;

namespace XKControlLib.Print
{
    public static class ReportMember
    {
        public static string cReportId { get; set; } = "_Report";
        public static string reportFile { get; set; }
        public static DataTable dtBiaoTou { get; set; }
        public static DataTable dtBiaoTi { get; set; }
        public static bool ExistBiaoTou()
        {
            return dtBiaoTou != null;
        }
        public static bool ExistBiaoTi()
        {
            return dtBiaoTi != null;
        }
    }
}
