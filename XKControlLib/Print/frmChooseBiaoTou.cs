﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XKControlLib.Print
{
    public partial class frmChooseBiaoTou : Form
    {
        private List<BiaoTouInfo> BK = new List<BiaoTouInfo>();
        public frmChooseBiaoTou()
        {
            InitializeComponent();
        }
        public List<BiaoTouInfo> BiaoTouInfos = GetBiaoTouInfoCollection();

        private static List<BiaoTouInfo> GetBiaoTouInfoCollection()
        {
            List<BiaoTouInfo> mdls = new List<BiaoTouInfo>();
            if (!ReportMember.ExistBiaoTou()) { return mdls; }
            var dtBiaoTou = ReportMember.dtBiaoTou;
            foreach (DataColumn col in dtBiaoTou.Columns)
            {
                BiaoTouInfo mdl = new BiaoTouInfo
                {
                    A03_cColumnName = col.ColumnName,
                    A04_cColumnByName = col.ColumnName + ":",
                    A05_iLeft = Rnd() + 10,
                    A06_iTop = Rnd() + 10,
                    A07_iWidth = 100,
                    A08_iHeight = 25,
                    A09_iFontSize = 9,
                    A13_iNumberDefinition = "4",
                    A14_cNumberFormat = "",
                    A15_bShow = false,
                    A16_bDouble = col.DataType == typeof(double)
                };
                var lbl = mdl.lbl;
                lbl.AutoSize = false;
                mdls.Add(mdl);
            }
            return mdls;
        }

        private static int Rnd()
        {
            return new Random().Next(100);
        }

        private void RefreshData()
        {
            dg1.AutoGenerateColumns = false;
            BiaoTouInfo.PropertyName PN = BiaoTouInfo.PN;
            this.cl_项目名称.DataPropertyName = PN.A03_cColumnName;
            this.cl_项目别名.DataPropertyName = PN.A04_cColumnByName;
            this.cl_显示.DataPropertyName = PN.A15_bShow;
            dg1.DataSource = BiaoTouInfos;
        }

        private void BackUp()
        {
            BK.Clear();
            foreach (BiaoTouInfo mdl in BiaoTouInfos)
            {
                var m = new BiaoTouInfo();
                m.A03_cColumnName = mdl.A03_cColumnName;
                m.A04_cColumnByName = mdl.A04_cColumnByName;
                m.A13_iNumberDefinition = mdl.A13_iNumberDefinition;
                m.A14_cNumberFormat = mdl.A14_cNumberFormat;
                m.A15_bShow = mdl.A15_bShow;
                BK.Add(m);
            }
        }

        private void ComeBack()
        {
            for (int i = 0; i < BiaoTouInfos.Count; i++)
            {
                var mdl = BiaoTouInfos[i];
                var mdl2 = BK[i];
                mdl.A15_bShow = mdl2.A15_bShow;
                mdl.A04_cColumnByName = mdl2.A04_cColumnByName;
                mdl.A03_cColumnName = mdl2.A03_cColumnName;
                mdl.A14_cNumberFormat = mdl2.A14_cNumberFormat;
                mdl.A13_iNumberDefinition = mdl2.A13_iNumberDefinition;
            }
        }

        private void frmChooseBiaoTou_Load(object sender, EventArgs e)
        {
            BackUp();
            RefreshData();
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (var mdl in BiaoTouInfos)
            {
                mdl.A15_bShow = true;
            }
            dg1.Refresh();
        }

        private void btnSelectNo_Click(object sender, EventArgs e)
        {
            foreach (var mdl in BiaoTouInfos)
            {
                mdl.A15_bShow = false;
            }
            dg1.Refresh();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ComeBack();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void dg1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dg1.CurrentRow == null) { return; }
            BiaoTouInfo mdl = (BiaoTouInfo)dg1.CurrentRow.DataBoundItem;
            this.cboDefinition.Enabled = mdl.A16_bDouble;
            this.cboFormat.Enabled = mdl.A16_bDouble;
            this.cboDefinition.Text = mdl.A13_iNumberDefinition;
            if (string.IsNullOrEmpty(mdl.A14_cNumberFormat))
            {
                this.cboFormat.Text = "无";
            }
            else
            {
                this.cboFormat.Text = mdl.A14_cNumberFormat;
            }
        }

        private void cboDefinition_TextChanged(object sender, EventArgs e)
        {
            if (dg1.CurrentRow == null) { return; }
            BiaoTouInfo mdl = (BiaoTouInfo)dg1.CurrentRow.DataBoundItem;
            if (int.TryParse(cboDefinition.Text, out int result))
            {
                mdl.A13_iNumberDefinition = cboDefinition.Text;
            }
            else
            {
                mdl.A13_iNumberDefinition = "4";
            }
        }

        private void cboFormat_TextChanged(object sender, EventArgs e)
        {
            if (dg1.CurrentRow == null) { return; }
            BiaoTouInfo mdl = (BiaoTouInfo)dg1.CurrentRow.DataBoundItem;
            if (cboFormat.Text == "无")
            {
                mdl.A14_cNumberFormat = "";
            }
            else
            {
                mdl.A14_cNumberFormat = cboFormat.Text;
            }
        }
    }
}
