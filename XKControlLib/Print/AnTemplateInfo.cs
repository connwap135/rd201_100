﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XKControlLib.Print
{
    public class AnTemplateInfo
    {
        public TemplateInfo TemplateInfos { get; set; }
        public List<BiaoTouInfo> BiaoTouInfos { get; set; }
        public List<BiaoTiInfo> BiaoTiInfos { get; set; }
        public List<LineInfo> LineInfos { get; set; }
    }
}
