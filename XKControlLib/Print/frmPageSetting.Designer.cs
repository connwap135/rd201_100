﻿namespace XKControlLib.Print
{
    partial class frmPageSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlPage = new System.Windows.Forms.Panel();
            this.txtDataHeaderHeight = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.Grp1 = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.chkTaoDa = new System.Windows.Forms.CheckBox();
            this.txtDataRowHeight = new System.Windows.Forms.TextBox();
            this.rdo2 = new System.Windows.Forms.RadioButton();
            this.rdo1 = new System.Windows.Forms.RadioButton();
            this.cboPages = new System.Windows.Forms.ComboBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.Grp1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlPage
            // 
            this.pnlPage.BackColor = System.Drawing.Color.White;
            this.pnlPage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPage.Location = new System.Drawing.Point(15, 22);
            this.pnlPage.Name = "pnlPage";
            this.pnlPage.Size = new System.Drawing.Size(95, 99);
            this.pnlPage.TabIndex = 0;
            // 
            // txtDataHeaderHeight
            // 
            this.txtDataHeaderHeight.Location = new System.Drawing.Point(205, 62);
            this.txtDataHeaderHeight.Name = "txtDataHeaderHeight";
            this.txtDataHeaderHeight.Size = new System.Drawing.Size(100, 21);
            this.txtDataHeaderHeight.TabIndex = 33;
            this.txtDataHeaderHeight.TextChanged += new System.EventHandler(this.txtDataHeaderHeight_TextChanged);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(164, 67);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(35, 12);
            this.Label4.TabIndex = 39;
            this.Label4.Text = "列高:";
            // 
            // Grp1
            // 
            this.Grp1.Controls.Add(this.pnlPage);
            this.Grp1.Location = new System.Drawing.Point(12, 12);
            this.Grp1.Name = "Grp1";
            this.Grp1.Size = new System.Drawing.Size(137, 150);
            this.Grp1.TabIndex = 38;
            this.Grp1.TabStop = false;
            this.Grp1.Text = "预览";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(241, 132);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 37;
            this.btnCancel.Text = "取消(&C)";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(156, 132);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 36;
            this.btnOK.Text = "确定(&O)";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // chkTaoDa
            // 
            this.chkTaoDa.AutoSize = true;
            this.chkTaoDa.Location = new System.Drawing.Point(168, 109);
            this.chkTaoDa.Name = "chkTaoDa";
            this.chkTaoDa.Size = new System.Drawing.Size(72, 16);
            this.chkTaoDa.TabIndex = 35;
            this.chkTaoDa.Text = "是否套打";
            this.chkTaoDa.UseVisualStyleBackColor = true;
            this.chkTaoDa.CheckedChanged += new System.EventHandler(this.chkTaoDa_CheckedChanged);
            // 
            // txtDataRowHeight
            // 
            this.txtDataRowHeight.Location = new System.Drawing.Point(205, 85);
            this.txtDataRowHeight.Name = "txtDataRowHeight";
            this.txtDataRowHeight.Size = new System.Drawing.Size(100, 21);
            this.txtDataRowHeight.TabIndex = 34;
            this.txtDataRowHeight.TextChanged += new System.EventHandler(this.txtDataHeaderHeight_TextChanged);
            // 
            // rdo2
            // 
            this.rdo2.AutoSize = true;
            this.rdo2.Location = new System.Drawing.Point(258, 43);
            this.rdo2.Name = "rdo2";
            this.rdo2.Size = new System.Drawing.Size(47, 16);
            this.rdo2.TabIndex = 32;
            this.rdo2.TabStop = true;
            this.rdo2.Text = "横向";
            this.rdo2.UseVisualStyleBackColor = true;
            this.rdo2.CheckedChanged += new System.EventHandler(this.rdo1_CheckedChanged);
            // 
            // rdo1
            // 
            this.rdo1.AutoSize = true;
            this.rdo1.Checked = true;
            this.rdo1.Location = new System.Drawing.Point(205, 43);
            this.rdo1.Name = "rdo1";
            this.rdo1.Size = new System.Drawing.Size(47, 16);
            this.rdo1.TabIndex = 31;
            this.rdo1.TabStop = true;
            this.rdo1.Text = "纵向";
            this.rdo1.UseVisualStyleBackColor = true;
            this.rdo1.CheckedChanged += new System.EventHandler(this.rdo1_CheckedChanged);
            // 
            // cboPages
            // 
            this.cboPages.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPages.FormattingEnabled = true;
            this.cboPages.Location = new System.Drawing.Point(205, 21);
            this.cboPages.Name = "cboPages";
            this.cboPages.Size = new System.Drawing.Size(100, 20);
            this.cboPages.TabIndex = 27;
            this.cboPages.TextChanged += new System.EventHandler(this.cboPages_TextChanged);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(164, 89);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(35, 12);
            this.Label3.TabIndex = 28;
            this.Label3.Text = "行高:";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(164, 45);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(35, 12);
            this.Label2.TabIndex = 29;
            this.Label2.Text = "方向:";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(164, 24);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(35, 12);
            this.Label1.TabIndex = 30;
            this.Label1.Text = "纸型:";
            // 
            // frmPageSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 172);
            this.Controls.Add(this.txtDataHeaderHeight);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Grp1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.chkTaoDa);
            this.Controls.Add(this.txtDataRowHeight);
            this.Controls.Add(this.rdo2);
            this.Controls.Add(this.rdo1);
            this.Controls.Add(this.cboPages);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Name = "frmPageSetting";
            this.Text = "页面设置";
            this.Load += new System.EventHandler(this.frmPageSetting_Load);
            this.Grp1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Panel pnlPage;
        internal System.Windows.Forms.TextBox txtDataHeaderHeight;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.GroupBox Grp1;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOK;
        internal System.Windows.Forms.CheckBox chkTaoDa;
        internal System.Windows.Forms.TextBox txtDataRowHeight;
        internal System.Windows.Forms.RadioButton rdo2;
        internal System.Windows.Forms.RadioButton rdo1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        private System.Windows.Forms.ComboBox cboPages;
    }
}