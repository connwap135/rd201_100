﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XKControlLib.Common
{
    public class FormatNumber : ICustomFormatter, IFormatProvider
    {
        string ICustomFormatter.Format(string format, object arg, IFormatProvider provider)
        {
            if (format == null)
            {
                if (arg is IFormattable)
                {
                    return ((IFormattable)arg).ToString(format, provider);
                }
                return arg.ToString();
            }
            else
            {
                if (format == "大写金额")
                {
                    return ConvertData.UpperMoney(arg.ToString());
                }
                else
                {
                    if (arg is IFormattable)
                    {
                        return ((IFormattable)arg).ToString(format, provider);
                    }
                    return arg.ToString();
                }
            }
        }

        object IFormatProvider.GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
            {
                return this;
            }
            return null;
        }
    }
}
