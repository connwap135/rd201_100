﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace XKControlLib.Common
{
    public class ConvertData
    {

        public static double Todbl(object obj)
        {
            if (obj == DBNull.Value)
            {
                return 0;
            }
            else if (!double.TryParse(obj.ToString(), out double result))
            {
                return 0;
            }
            else
            {
                double.TryParse(obj.ToString(), out double result1);
                return result1;
            }
        }

        public static string Tostr(object obj, bool bTrim = true)
        {
            if (obj == DBNull.Value)
            {
                return "";
            }
            else
            {
                if (bTrim)
                {
                    return obj.ToString().Trim();
                }
                else
                {
                    return obj.ToString();
                }
            }
        }

        /// <summary>
        /// 人民币大写
        /// </summary>
        /// <param name="Money"></param>
        /// <returns></returns>
        public static string UpperMoney(string Money)
        {
            decimal number = decimal.Parse(Money);
            var s = number.ToString("#L#E#D#C#K#E#D#C#J#E#D#C#I#E#D#C#H#E#D#C#G#E#D#C#F#E#D#C#.0B0A");
            var d = Regex.Replace(s, @"((?<=-|^)[^1-9]*)|((?'z'0)[0A-E]*((?=[1-9])|(?'-z'(?=[F-L\.]|$))))|((?'b'[F-L])(?'z'0)[0A-L]*((?=[1-9])|(?'-z'(?=[\.]|$))))", "${b}${z}");
            var r = Regex.Replace(d, ".", m => "负元空零壹贰叁肆伍陆柒捌玖空空空空空空空分角拾佰仟万亿兆京垓秭穰"[m.Value[0] - '-'].ToString());
            return r;
        }

        public static DataTable ToDataTable(DataView dv)
        {
            return dv.Table;
        }

        public static DataTable ToDataTable(Array array)
        {
            if (array != null)
            {
                if (array.Length != 0)
                {
                    return ToTable(array, array.GetType());
                }
            }
            return null;
        }

        public static DataTable ToDataTable(IList list)
        {
            if (list != null)
            {
                if (list.Count != 0)
                {
                    return ToTable(list, list[0].GetType());
                }
            }
            return null;
        }

        public static DataTable ToDataTable(object obj)
        {
            if (obj != null)
            {
                Type type = obj.GetType();
                var dt = ToTable(type);
                var Pros = type.GetProperties();
                var dr = dt.NewRow();
                ToDataRow(obj,ref dr, Pros);
                dt.Rows.Add(dr);
                return dt;

            }
            return null;
        }
         
        public static DataTable ToDataTable(DataGridView dgv)
        {
            DataTable dt = new DataTable();
            // 列强制转换
            for (int count = 0; count < dgv.Columns.Count; count++)
            {
                DataColumn dc = new DataColumn(dgv.Columns[count].Name.ToString());
                dt.Columns.Add(dc);
            }
            // 循环行
            for (int count = 0; count < dgv.Rows.Count; count++)
            {
                DataRow dr = dt.NewRow();
                for (int countsub = 0; countsub < dgv.Columns.Count; countsub++)
                {
                    dr[countsub] = Convert.ToString(dgv.Rows[count].Cells[countsub].Value);
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }


        public static IList<T> ToList<T>(DataTable table)
        {
            if (table == null)
            {
                return null;
            }
            List<DataRow> rows = new List<DataRow>();

            foreach (DataRow row in table.Rows)
            {
                rows.Add(row);
            }

            return ConvertTo<T>(rows);
        }

        private static IList<T> ConvertTo<T>(List<DataRow> rows)
        {
            IList<T> list = null;

            if (rows != null)
            {
                list = new List<T>();

                foreach (DataRow row in rows)
                {
                    T item = CreateItem<T>(row);
                    list.Add(item);
                }
            }

            return list;
        }

        private static T CreateItem<T>(DataRow row)
        {
            T obj = default;
            if (row != null)
            {
                obj = Activator.CreateInstance<T>();

                foreach (DataColumn column in row.Table.Columns)
                {
                    PropertyInfo prop = obj.GetType().GetProperty(column.ColumnName);
                    try
                    {
                        object value = row[column.ColumnName];
                        prop.SetValue(obj, value, null);
                    }
                    catch
                    {  //You can log something here     
                       //throw;    
                    }
                }
            }

            return obj;
        }

        private static DataTable ToTable(IList list, Type type)
        {
            var Arr = new object[list.Count];
            list.CopyTo(Arr, 0);
            return ToTable(Arr, type);
        }

        private static DataTable ToTable(Array array, Type type)
        {
            var dt = ToTable(type);
            var Pros = type.GetProperties();
            foreach (var obj in array)
            {
                var dr = dt.NewRow();
                ToDataRow(obj,ref dr, Pros);
                dt.Rows.Add(dr);
            }
            return dt;
        }

        private static void ToDataRow(object obj,ref DataRow dr, PropertyInfo[] pros)
        {
            foreach (PropertyInfo p in pros)
            {
                try
                {
                    if (dr.Table.Columns.Contains(p.Name))
                    {
                        var value = p.GetValue(obj, null);
                        if (value == null && p.PropertyType == typeof(DateTime))
                        {
                            dr[p.Name] = DBNull.Value;
                        }
                        else
                        {
                            dr[p.Name] = value;
                        }
                    }

                }
                catch (Exception)
                {
                }
            }
        }

        private static DataTable ToTable(Type type)
        {
            DataTable dt = new DataTable();
            foreach (PropertyInfo Pro in type.GetProperties())
            {
                var col = new DataColumn(Pro.Name, Pro.PropertyType);
                dt.Columns.Add(col);

            }
            return dt;
        }

        private static DataTable ToTable(DataGridViewColumnCollection cols)
        {
            DataTable dt = new DataTable();
            foreach (DataGridViewColumn col in cols)
            {
                var cl = new DataColumn();
                cl.ColumnName = col.Name;
                cl.Caption = col.HeaderText;
                if (col.ValueType != null)
                {
                    cl.DataType = col.ValueType;
                }
                dt.Columns.Add(cl);
            }
            return dt;
        }

        private static void ToDataRow(object obj, DataRow dr)
        {
            var Pros = obj.GetType().GetProperties();
            ToDataRow(obj,ref dr, Pros);
        }
    }
}
