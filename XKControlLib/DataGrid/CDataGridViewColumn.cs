﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XKControlLib;
namespace XKControlLib.DataGrid
{
    public partial class CDataGridTextColumn : DataGridViewColumn
    {
        public CDataGridTextColumn() : base(new CDataGridViewCell())
        {  
            this.TextBoxType=TextBoxType.txt;
        }
        public TextBoxType TextBoxType { get; set; } 

        public event EventHandler ZoomClick;
        public event EventHandler EndEdit;

        public void OnZoomClick(EventArgs e)
        {
            ZoomClick?.Invoke(this, e);
        }

        public void OnEndEdit(EventArgs e)
        {
            EndEdit?.Invoke(this, e);
        }
    }


    public class CDataGridZoomColumn : CDataGridTextColumn
    {
        public CDataGridZoomColumn() : base()
        {
            this.TextBoxType = TextBoxType.txtZoom;
            base.EndEdit += CDataGridZoomColumn_EndEdit;
        }
        public new event EventHandler EndEdit;
        private void CDataGridZoomColumn_EndEdit(object sender, EventArgs e)
        {
            EndEdit?.Invoke(this, e);
        }
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class CDataGridDateColumn : CDataGridTextColumn
    {
        public CDataGridDateColumn() : base()
        {
            this.TextBoxType = TextBoxType.txtDate;
        }
    }
}
