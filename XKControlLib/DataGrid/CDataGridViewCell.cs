﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XKControlLib.DataGrid
{
    public class CDataGridViewCell: DataGridViewTextBoxCell
    {
        public override void InitializeEditingControl(int rowIndex, object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
        {
            base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
            CDataGridViewEditingControl ctl = DataGridView.EditingControl as CDataGridViewEditingControl;
            try
            {
                ctl.Text = Value is null || Value==DBNull.Value ? "" : Value.ToString();
            }
            catch (Exception)
            {
            }
           
        }

        public override Type EditType => typeof(CDataGridViewEditingControl);
    }

}
