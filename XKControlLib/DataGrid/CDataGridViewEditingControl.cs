﻿using System;
using System.Windows.Forms;
namespace XKControlLib.DataGrid
{
    public class CDataGridViewEditingControl : CTextBox, IDataGridViewEditingControl
    {
        public CDataGridViewEditingControl()
        {
            InitializeComponent();
            this.TextChanged += CDataGridViewEditingControl_TextChanged;
        }

        public DataGridView EditingControlDataGridView { get; set; }

        public object EditingControlFormattedValue { get => this.Text; set => this.Text= value.ToString(); }

        public int EditingControlRowIndex { get; set; }

        public bool EditingControlValueChanged { get; set; }

        public Cursor EditingPanelCursor => base.Cursor;

        public bool RepositionEditingControlOnValueChange => false;

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
           
        }

        public bool EditingControlWantsInputKey(Keys keyData, bool dataGridViewWantsInputKey)
        {
            return false;
        }

        public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return this.Text;
        }

        public void PrepareEditingControlForEdit(bool selectAll)
        {

        }

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.PicDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picZoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pic)).BeginInit();
            this.SuspendLayout();
            // 
            // Pic
            // 
            this.Pic.Location = new System.Drawing.Point(92, -2);
            // 
            // CDataGridViewEditingControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.Name = "CDataGridViewEditingControl";
            this.EndEdit += new System.EventHandler(this.CDataGridViewEditingControl_EndEdit);
            this.ZoomClick += new System.EventHandler(this.CDataGridViewEditingControl_ZoomClick);
            this.VisibleChanged += new System.EventHandler(this.CDataGridViewEditingControl_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.PicDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picZoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void CDataGridViewEditingControl_VisibleChanged(object sender, System.EventArgs e)
        {
            try
            {
                
                CDataGridTextColumn col = EditingControlDataGridView.Columns[EditingControlDataGridView.CurrentCell.ColumnIndex] as CDataGridTextColumn;
                this.TextBoxType = col.TextBoxType;
                this.Dock = DockStyle.Fill;
                this.BorderStyle = BorderStyle.None;
                this.BackColor = System.Drawing.Color.White;
                this.Caption = "";
            }
            catch (Exception)
            {

            }
        }

        private void CDataGridViewEditingControl_TextChanged(object sender, System.EventArgs e)
        {
            EditingControlValueChanged = true;
            this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
        }

        private void CDataGridViewEditingControl_ZoomClick(object sender, System.EventArgs e)
        {
            CDataGridTextColumn col = EditingControlDataGridView.Columns[EditingControlDataGridView.CurrentCell.ColumnIndex] as CDataGridTextColumn;
            EditingControlDataGridView.CurrentCell.Value = this.Text;
            col.OnZoomClick(e);
        }

        private void CDataGridViewEditingControl_EndEdit(object sender, System.EventArgs e)
        {
            CDataGridTextColumn col = EditingControlDataGridView.Columns[EditingControlDataGridView.CurrentCell.ColumnIndex] as CDataGridTextColumn;
            EditingControlDataGridView.CurrentCell.Value = this.Text;
            col.OnEndEdit(e);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                //case Keys.Up:
                //    EditingControlDataGridView.EndEdit();
                //    SendKeys.SendWait("{Up}");
                //    break;
                //case Keys.Down:
                //    EditingControlDataGridView.EndEdit();
                //    SendKeys.SendWait("{Down}");
                //    break;
                case Keys.Left:
                    if (this.txt.SelectionStart == 0)
                    {
                        SendKeys.SendWait("+{tab}");
                        return true;
                    }
                    break;
                case Keys.Right:
                    if (this.txt.SelectionStart == this.Text.Length)
                    {
                        SendKeys.SendWait("{tab}");
                        return true;
                    }
                    break;
                default:
                    break;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}