﻿namespace XKControlLib.DataGrid
{
    partial class frmDataGridSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNext = new System.Windows.Forms.Button();
            this.btnSelectNo = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnBottom = new System.Windows.Forms.Button();
            this.dg = new System.Windows.Forms.DataGridView();
            this.cl_显示 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cl_原名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cl_别名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cl_宽度 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnTop = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dg)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(338, 71);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(76, 23);
            this.btnNext.TabIndex = 18;
            this.btnNext.Text = "下移(&N)";
            this.btnNext.UseVisualStyleBackColor = true;
            // 
            // btnSelectNo
            // 
            this.btnSelectNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectNo.Location = new System.Drawing.Point(338, 148);
            this.btnSelectNo.Name = "btnSelectNo";
            this.btnSelectNo.Size = new System.Drawing.Size(76, 23);
            this.btnSelectNo.TabIndex = 21;
            this.btnSelectNo.Text = "全清(&D)";
            this.btnSelectNo.UseVisualStyleBackColor = true;
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectAll.Location = new System.Drawing.Point(338, 122);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(76, 23);
            this.btnSelectAll.TabIndex = 20;
            this.btnSelectAll.Text = "全选(&A)";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            // 
            // btnBottom
            // 
            this.btnBottom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBottom.Location = new System.Drawing.Point(338, 97);
            this.btnBottom.Name = "btnBottom";
            this.btnBottom.Size = new System.Drawing.Size(76, 23);
            this.btnBottom.TabIndex = 19;
            this.btnBottom.Text = "置底(&B)";
            this.btnBottom.UseVisualStyleBackColor = true;
            // 
            // dg
            // 
            this.dg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dg.BackgroundColor = System.Drawing.Color.White;
            this.dg.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cl_显示,
            this.cl_原名,
            this.cl_别名,
            this.cl_宽度});
            this.dg.Location = new System.Drawing.Point(13, 12);
            this.dg.Name = "dg";
            this.dg.RowHeadersVisible = false;
            this.dg.RowTemplate.Height = 20;
            this.dg.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dg.Size = new System.Drawing.Size(318, 324);
            this.dg.TabIndex = 15;
            // 
            // cl_显示
            // 
            this.cl_显示.DataPropertyName = "bShow";
            this.cl_显示.HeaderText = "显示";
            this.cl_显示.Name = "cl_显示";
            this.cl_显示.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cl_显示.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cl_显示.Width = 55;
            // 
            // cl_原名
            // 
            this.cl_原名.DataPropertyName = "cColumnName";
            this.cl_原名.HeaderText = "原名";
            this.cl_原名.Name = "cl_原名";
            this.cl_原名.ReadOnly = true;
            // 
            // cl_别名
            // 
            this.cl_别名.DataPropertyName = "cColumnByName";
            this.cl_别名.HeaderText = "别名";
            this.cl_别名.Name = "cl_别名";
            // 
            // cl_宽度
            // 
            this.cl_宽度.DataPropertyName = "iWidth";
            this.cl_宽度.HeaderText = "宽度";
            this.cl_宽度.Name = "cl_宽度";
            this.cl_宽度.Width = 60;
            // 
            // btnPrevious
            // 
            this.btnPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrevious.Location = new System.Drawing.Point(338, 45);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(76, 23);
            this.btnPrevious.TabIndex = 17;
            this.btnPrevious.Text = "上移(&P)";
            this.btnPrevious.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(338, 189);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 23);
            this.btnClose.TabIndex = 22;
            this.btnClose.Text = "关闭(&C)";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnTop
            // 
            this.btnTop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTop.Location = new System.Drawing.Point(338, 19);
            this.btnTop.Name = "btnTop";
            this.btnTop.Size = new System.Drawing.Size(76, 23);
            this.btnTop.TabIndex = 16;
            this.btnTop.Text = "置顶(&T)";
            this.btnTop.UseVisualStyleBackColor = true;
            // 
            // frmDataGridSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 345);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnSelectNo);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.btnBottom);
            this.Controls.Add(this.dg);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnTop);
            this.Name = "frmDataGridSetting";
            this.Text = "表格设置";
            ((System.ComponentModel.ISupportInitialize)(this.dg)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnNext;
        internal System.Windows.Forms.Button btnSelectNo;
        internal System.Windows.Forms.Button btnSelectAll;
        internal System.Windows.Forms.Button btnBottom;
        internal System.Windows.Forms.DataGridView dg;
        internal System.Windows.Forms.DataGridViewCheckBoxColumn cl_显示;
        internal System.Windows.Forms.DataGridViewTextBoxColumn cl_原名;
        internal System.Windows.Forms.DataGridViewTextBoxColumn cl_别名;
        internal System.Windows.Forms.DataGridViewTextBoxColumn cl_宽度;
        internal System.Windows.Forms.Button btnPrevious;
        internal System.Windows.Forms.Button btnClose;
        internal System.Windows.Forms.Button btnTop;
    }
}