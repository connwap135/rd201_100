﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace XKControlLib.DataGrid
{
    public partial class frmDataGridSetting : Form
    {
        private List<ColumnInfo> ColumnInfos=new List<ColumnInfo>();

        public frmDataGridSetting(DataGridView cDataGridView)
        {
            InitializeComponent();
            ColumnInfos = ToColumnInfoCollection(cDataGridView.Columns);
            this.dg.AllowUserToAddRows = false;
            this.dg.AllowUserToDeleteRows = false;
            this.dg.AutoGenerateColumns = false;
            this.dg.DataSource = ColumnInfos;

            dg.CellEnter += Dg_CellEnter;
            dg.DataError += Dg_DataError;
            btnTop.Click += Btn_Click;
            btnPrevious.Click += Btn_Click;
            btnNext.Click += Btn_Click;
            btnBottom.Click += Btn_Click;
            btnSelectAll.Click += Btn_Click;
            btnSelectNo.Click += Btn_Click;
            btnClose.Click += Btn_Click;
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            CButton btn = sender as CButton;
            switch (btn.Name)
            {
                case "btnTop":
                    var dr = dg.CurrentRow;
                    if (dr != null)
                    {
                        var ColumnInfo =(ColumnInfo) dr.DataBoundItem;
                        int index = ColumnInfos.IndexOf(ColumnInfo);
                        if (index != 0)
                        {
                            ColumnInfos.Remove(ColumnInfo);
                            ColumnInfos.Insert(0, ColumnInfo);
                            ColumnInfo.Column.DisplayIndex = 0;
                            dg.DataSource = null;
                            dg.DataSource = ColumnInfos;
                        }
                    }
                    break;
                case "btnPrevious":
                    var dr1 = dg.CurrentRow;
                    if (dr1 != null)
                    {
                        var ColumnInfo = (ColumnInfo)dr1.DataBoundItem;
                        int index = ColumnInfos.IndexOf(ColumnInfo);
                        if (index != 0)
                        {
                            ColumnInfos.Remove(ColumnInfo);
                            ColumnInfos.Insert(index - 1, ColumnInfo);
                            ColumnInfo.Column.DisplayIndex = index - 1;
                            dg.DataSource = null;
                            dg.DataSource = ColumnInfos;
                            Place(ColumnInfo);
                        }
                    }
                    break;
                case "btnNext":
                    var dr2 = dg.CurrentRow;
                    if (dr2 != null)
                    {
                        var ColumnInfo = (ColumnInfo)dr2.DataBoundItem;
                        int index = ColumnInfos.IndexOf(ColumnInfo);
                        if (index != ColumnInfos.Count - 1)
                        {
                            ColumnInfos.Remove(ColumnInfo);
                            ColumnInfos.Insert(index +1, ColumnInfo);
                            ColumnInfo.Column.DisplayIndex = index + 1;
                            dg.DataSource = null;
                            dg.DataSource = ColumnInfos;
                            Place(ColumnInfo);
                        }
                    }
                    break;
                case "btnBottom":
                    var dr3 = dg.CurrentRow;
                    if (dr3 != null)
                    {
                        var ColumnInfo = (ColumnInfo)dr3.DataBoundItem;
                        int index = ColumnInfos.IndexOf(ColumnInfo);
                        if (index != ColumnInfos.Count - 1)
                        {
                            ColumnInfos.Remove(ColumnInfo);
                            ColumnInfos.Add( ColumnInfo);
                            ColumnInfo.Column.DisplayIndex = ColumnInfos.Count - 1;
                            dg.DataSource = null;
                            dg.DataSource = ColumnInfos;
                            Place(ColumnInfo);
                        }
                    }
                    break;
                case "btnSelectAll":
                    foreach (DataGridViewRow item in dg.Rows)
                    {
                        var ColumnInfo = (ColumnInfo)item.DataBoundItem;
                        ColumnInfo.BShow = true;
                    }
                    dg.DataSource = null;
                    dg.DataSource = ColumnInfos;
                    break;
                case "btnSelectNo":
                    foreach (DataGridViewRow item in dg.Rows)
                    {
                        var ColumnInfo = (ColumnInfo)item.DataBoundItem;
                        ColumnInfo.BShow = false;
                    }
                    dg.DataSource = null;
                    dg.DataSource = ColumnInfos;
                    break;
                case "btnClose":
                default:
                    this.Close();
                    break;
            }
        }

        private void Dg_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
           MessageBox.Show(e.Exception.Message);
        }

        private void Dg_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dg.CurrentRow != null)
            {
                dg.CurrentRow.Selected = true;
            }
        }

        private List<ColumnInfo> ToColumnInfoCollection(DataGridViewColumnCollection columns)
        {
            var ColumnInfos = new List<ColumnInfo>();
            foreach (DataGridViewColumn item in columns)
            {
                var ColumnInfo = new ColumnInfo
                {
                    Column = item
                };
                ColumnInfos.Add(ColumnInfo);
            }
            return ColumnInfos.OrderBy(x => x.IDisplayIndex).ToList();
        }

        private void Place(ColumnInfo mdl)
        {
            foreach (DataGridViewRow dr in dg.Rows)
            {
                if(dr.DataBoundItem is ColumnInfo)
                {
                    dg.CurrentCell = dr.Cells[dg.CurrentCell.ColumnIndex];
                }
            }
        }


    }

     public class ColumnInfo
    {
        public string CColumnName { get; set; }

        private string _CColumnByName;
        public string CColumnByName
        {
            get => _CColumnByName;
            set
            {
                _CColumnByName = value;
                _Column.HeaderText = value;
            }
        }

        private int _IWidth;
        public int IWidth {
            get =>_IWidth;
            set
            {
                _IWidth = value;
                _Column.Width = value;
            }
        }

        private bool _BShow;
        public bool BShow {
            get =>_BShow;
            set {
                _BShow = value;
                _Column.Visible = value;
            }
        }

        private int _IDisplayIndex;
        public int IDisplayIndex {
            get =>_IDisplayIndex;
            set
            {
                _IDisplayIndex = value;
                _Column.DisplayIndex = value;
            }
        }

        private DataGridViewColumn _Column;
        public DataGridViewColumn Column {
            get=>_Column;
            set
            {
                _Column = value;
                CColumnName = value.Name;
                CColumnByName = value.HeaderText;
                IWidth = value.Width;
                BShow = value.Visible;
                IDisplayIndex = value.DisplayIndex;
            }
        }
    }
}
