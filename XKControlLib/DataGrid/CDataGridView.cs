﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XKControlLib.DataGrid
{
    public partial class CDataGridView : DataGridView
    {
        public CDataGridView()
        {
            InitializeComponent();
            this.ColumnAdded += CDataGridView_ColumnAdded;
        }

        private void CDataGridView_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            int tmpHeight = this.ColumnHeadersHeight;
            if (tmpHeight <= 17) { tmpHeight = 30; }
            if (ColumnHeadersVisible)
            {
                this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
                this.ColumnHeadersHeight = tmpHeight;
            }  
        }

        public CDataGridView(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }

        protected override void OnCellEndEdit(DataGridViewCellEventArgs e)
        {
            RefreshRow(e.RowIndex);
            base.OnCellEndEdit(e);
        }

        protected override void OnRowPostPaint(DataGridViewRowPostPaintEventArgs e)
        {
            if (this.RowHeadersVisible)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, this.RowHeadersWidth, e.RowBounds.Height);
                var strFormat = new StringFormat
                {
                    LineAlignment = StringAlignment.Center,
                    Alignment = StringAlignment.Far
                };
                if (e.State == (DataGridViewElementStates.Selected | DataGridViewElementStates.Visible | DataGridViewElementStates.Displayed))
                {
                    e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, new SolidBrush(RowHeadersDefaultCellStyle.SelectionForeColor), rect, strFormat);
                }
                else
                {
                    e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, new SolidBrush(RowHeadersDefaultCellStyle.ForeColor), rect, strFormat);
                }
            }
            base.OnRowPostPaint(e);
        }

        private void RefreshRow(int rowIndex)
        {
            DataGridViewRow dr = this.Rows[rowIndex];
            if (dr != null)
            {
                if (dr.Selected)
                {
                    dr.Selected = false;
                    dr.Selected = true;
                }
                else
                {
                    dr.Selected = true;
                    dr.Selected = false;
                }
            }
        }

        protected override void OnCellClick(DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                this.EditMode = DataGridViewEditMode.EditOnKeystrokeOrF2;
                this.EndEdit();
            }
            else
            {
                if (this.EditMode != DataGridViewEditMode.EditOnEnter)
                {
                    this.EditMode = DataGridViewEditMode.EditOnEnter;
                    this.EndEdit();
                }
            }
            base.OnCellClick(e);
        }

        protected override void OnDataError(bool displayErrorDialogIfNoHandler, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(e.Exception.ToString(), "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            base.OnDataError(displayErrorDialogIfNoHandler, e);
        }

        public void RebindDataSource()
        {
            var obj = new BindingSource(this.DataSource, null);
            this.DataSource = null;
            this.DataSource = obj;
        }

        public void ChangeColumnNameAsHeader()
        {
            foreach (DataGridViewColumn item in this.Columns)
            {
                item.Name = item.HeaderText;
            }
        }

        public void SettingColumns()
        {
            var frm = new frmDataGridSetting(this);
            frm.ShowDialog();
        }

    }
}
