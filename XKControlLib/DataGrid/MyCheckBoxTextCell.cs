﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Qc2StockApp.eXControl
{
    public class MyCheckBoxTextCell : DataGridViewCell
    {
        public MyCheckBoxTextCell() : base() { }

        private static Type defaultEditType = typeof(MyCheckBoxTextControl);
        private static Type defaultValueType = typeof(bool);

        public override Type EditType => defaultEditType;
        public override Type ValueType
        {
            get
            {
                Type valueType = base.ValueType;
                if (valueType != null)
                {
                    return valueType;
                }
                return defaultValueType;
            }
        }
        /// <summary>
        /// 单元格边框颜色
        /// </summary>
        private Color CellBorderColor { get { return Color.FromArgb(172, 168, 153); } }

        protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
        {
            var check = (Boolean)value;
            if (paintParts == DataGridViewPaintParts.Background || paintParts == DataGridViewPaintParts.All)
            {
                graphics.FillRectangle(new SolidBrush(cellStyle.BackColor), cellBounds);
            }
            if (paintParts == DataGridViewPaintParts.Border || paintParts == DataGridViewPaintParts.All)
            {
                graphics.DrawRectangle(new Pen(CellBorderColor), cellBounds);
            }
            if (paintParts == DataGridViewPaintParts.SelectionBackground || Selected)
            {
                graphics.FillRectangle(new SolidBrush(cellStyle.SelectionBackColor), cellBounds);
            }
            var col = OwningColumn as MyCheckBoxTextColumn;
            if (col != null && !string.IsNullOrEmpty(col.Text))
            {
                graphics.DrawString(col.Text, cellStyle.Font, new SolidBrush(Selected ?
                    cellStyle.SelectionForeColor : cellStyle.ForeColor),
                    new Point(cellBounds.X + 25, cellBounds.Y + cellBounds.Height / 4));
            }
            CheckBoxRenderer.DrawCheckBox(graphics, new Point(cellBounds.X + 4, cellBounds.Y + cellBounds.Height / 4), CheckState);
            base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
        }

        /// <summary>
        /// <summary>
        /// 当前复选框的状态
        /// </summary>
        private CheckBoxState CheckState { set; get; }

        protected override void OnMouseDown(DataGridViewCellMouseEventArgs e)
        {
            var check = (bool)Value;
            CheckState = check ? CheckBoxState.CheckedPressed : CheckBoxState.UncheckedPressed;
            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(DataGridViewCellMouseEventArgs e)
        {
            var check = (bool)Value;
            Value = !check;
            SetValue(RowIndex, Value);
            CheckState = check ? CheckBoxState.CheckedNormal : CheckBoxState.UncheckedNormal;
            base.OnMouseUp(e);
        }



        public override object DefaultNewRowValue
        {
            get
            {
                return true;
            }
        }
    }
}