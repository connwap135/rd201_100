﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Qc2StockApp.eXControl
{
   public class MyCheckBoxTextColumn : DataGridViewColumn
    {
        public MyCheckBoxTextColumn()
            : base()
        {
            CellTemplate = new MyCheckBoxTextCell();
        }

        public override DataGridViewCell CellTemplate
        {
            get
            {
                return base.CellTemplate;
            }
            set
            {
                if (value != null && !value.GetType().IsAssignableFrom(typeof(MyCheckBoxTextCell)))
                {
                    throw new Exception("这个列里面必须绑定MyDataGridViewCheckBoxCell");
                }
                base.CellTemplate = value;
            }
        }

        public override object Clone()
        {
            MyCheckBoxTextColumn col = (MyCheckBoxTextColumn)base.Clone();
            col.Text = Text;
            return col;
        }

        public string Text { set; get; }


    }

}
