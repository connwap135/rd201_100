﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XKControlLib.ToolStrip
{
    public partial class CToolStrip : System.Windows.Forms.ToolStrip
    {
        public CToolStrip()
        {
            InitializeComponent();
            this.Renderer = new ProToolstripRenderer(false);
        }

        public CToolStrip(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        protected override void OnItemClicked(ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "TbtnAdd":
                    if (State!=ToolStripState.None)
                    {
                        return;
                    }
                    else
                    {
                        State = ToolStripState.Adding;
                    }
                    break;
                case "TbtnChange":
                    if (State != ToolStripState.None)
                    {
                        return;
                    }
                    else
                    {
                        State = ToolStripState.Changing;
                    }
                    break;
                case "TbtnSave":
                    if (State == ToolStripState.None)
                    {
                        return;
                    }
                    break;
                case "TbtnCancel":
                    if (State == ToolStripState.None)
                    {
                        return;
                    }
                    break;
                default:
                    break;
            }
            base.OnItemClicked(e);
        }

        #region("属性")
        [Category("自定义")]
        public ToolStripItem T01_PrintPreview => TbtnPrintPreview;
        [Category("自定义")]
        public ToolStripItem T02_Export => TbtnExport;
        [Category("自定义")]
        public ToolStripItem T03_Add => TbtnAdd;
        [Category("自定义")]
        public ToolStripItem T04_Change => TbtnChange;
        [Category("自定义")]
        public ToolStripItem T05_Delete => TbtnDelete;
        [Category("自定义")]
        public ToolStripItem T06_Save => TbtnSave;
        [Category("自定义")]
        public ToolStripItem T07_Cancel => TbtnCancel;
        [Category("自定义")]
        public ToolStripItem T08_First => TbtnFirst;
        [Category("自定义")]
        public ToolStripItem T09_Previous => TbtnPrevious;
        [Category("自定义")]
        public ToolStripItem T10_Next => TbtnNext;
        [Category("自定义")]
        public ToolStripItem T11_Last => TbtnLast;
        [Category("自定义")]
        public ToolStripItem T12_Place => TbtnPlace;
        [Category("自定义")]
        public ToolStripItem T13_Filter => TbtnFilter;
        [Category("自定义")]
        public ToolStripItem T14_AddRow => TbtnAddRow;
        [Category("自定义")]
        public ToolStripItem T15_InsertRow => TbtnInsertRow;
        [Category("自定义")]
        public ToolStripItem T16_DeleteRow => TbtnDeleteRow;
        [Category("自定义")]
        public ToolStripItem T17_Check => TbtnCheck;
        [Category("自定义")]
        public ToolStripItem T18_BianGeng => TbtnBianGeng;
        [Category("自定义")]
        public ToolStripItem T19_Close => TbtnClose;
        [Category("自定义")]
        public ToolStripItem T20_Refresh => TbtnRefresh;
        [Category("自定义")]
        public ToolStripItem T21_Exit => TbtnExit;
        [Category("自定义")]
        public ToolStripItem TDefine1 => TbtnDefine1;
        [Category("自定义")]
        public ToolStripItem TDefine2 => TbtnDefine2;
        [Category("自定义")]
        public ToolStripItem TDefine3 => TbtnDefine3;
        #endregion


        public void ShowAllItem()
        {
            foreach (ToolStripItem item in this.Items)
            {
                item.Visible = true;
            }
        }

        public void HideAllItem()
        {
            foreach (ToolStripItem item in this.Items)
            {
                item.Visible = false;
            }
        }

        private ToolStripState _State = ToolStripState.None;
        [Category("自定义")]
        [Description("状态")]
        public ToolStripState State {
            get =>_State;
            set
            {
                _State = value;
                switch (value)
                {
                    case ToolStripState.None:
                        this.T03_Add.Enabled = true;
                        this.T04_Change.Enabled = true;
                        this.T05_Delete.Enabled = true;
                        this.T01_PrintPreview.Enabled = true;
                        this.T02_Export.Enabled = true;
                        this.T08_First.Enabled = true;
                        this.T09_Previous.Enabled = true;
                        this.T10_Next.Enabled = true;
                        this.T11_Last.Enabled = true;
                        this.T12_Place.Enabled = true;
                        this.T13_Filter.Enabled = true;
                        this.T14_AddRow.Enabled = false;
                        this.T15_InsertRow.Enabled = false;
                        this.T16_DeleteRow.Enabled = false;
                        this.T17_Check.Enabled = true;
                        this.T18_BianGeng.Enabled = true;
                        this.T19_Close.Enabled = true;
                        this.T20_Refresh.Enabled = true;
                        this.T21_Exit.Enabled = true;
                        break;
                    case ToolStripState.Adding:
                        this.T03_Add.Enabled = true;
                        this.T04_Change.Enabled = false;
                        this.T05_Delete.Enabled = false;
                        this.T01_PrintPreview.Enabled = false;
                        this.T02_Export.Enabled = false;
                        this.T08_First.Enabled = false;
                        this.T09_Previous.Enabled = false;
                        this.T10_Next.Enabled = false;
                        this.T11_Last.Enabled = false;
                        this.T12_Place.Enabled = false;
                        this.T13_Filter.Enabled = false;
                        this.T14_AddRow.Enabled = true;
                        this.T15_InsertRow.Enabled = true;
                        this.T16_DeleteRow.Enabled = true;
                        this.T17_Check.Enabled = false;
                        this.T18_BianGeng.Enabled = false;
                        this.T19_Close.Enabled = false;
                        this.T20_Refresh.Enabled = false;
                        this.T21_Exit.Enabled = false;
                        break;
                    case ToolStripState.Changing:
                        this.T03_Add.Enabled = false;
                        this.T04_Change.Enabled = true;
                        this.T05_Delete.Enabled = false;
                        this.T01_PrintPreview.Enabled = false;
                        this.T02_Export.Enabled = false;
                        this.T08_First.Enabled = false;
                        this.T09_Previous.Enabled = false;
                        this.T10_Next.Enabled = false;
                        this.T11_Last.Enabled = false;
                        this.T12_Place.Enabled = false;
                        this.T13_Filter.Enabled = false;
                        this.T14_AddRow.Enabled = true;
                        this.T15_InsertRow.Enabled = true;
                        this.T16_DeleteRow.Enabled = true;
                        this.T17_Check.Enabled = false;
                        this.T18_BianGeng.Enabled = false;
                        this.T19_Close.Enabled = false;
                        this.T20_Refresh.Enabled = false;
                        this.T21_Exit.Enabled = false;
                        break;
                    default:
                        break;
                }
            }
        }

        private bool _Checked;
        [Browsable(false)]
        [DefaultValue(false)]
        public bool Checked {
            get => _Checked;
            set
            {
                _Checked = value;
               T17_Check.Text = value ? "反审" : "审核";
            }
        }

        private bool _Closed;
        [Browsable(false)]
        public bool Closed
        {
            get => _Closed;
            set
            {
                _Closed = value;
                T19_Close.Text = value ? "打开" : "关闭";
            }
        }
    }

    public enum ToolStripState
    {
        [Description("正常")]
        None,
        [Description("正在新增")]
        Adding,
        [Description("正在修改")]
        Changing
    }
}
