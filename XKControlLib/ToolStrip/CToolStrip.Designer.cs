﻿namespace XKControlLib.ToolStrip
{
    partial class CToolStrip
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CToolStrip));
            this.TbtnAdd = new System.Windows.Forms.ToolStripButton();
            this.TbtnChange = new System.Windows.Forms.ToolStripButton();
            this.TbtnDelete = new System.Windows.Forms.ToolStripButton();
            this.TbtnDefine1 = new System.Windows.Forms.ToolStripButton();
            this.TbtnDefine2 = new System.Windows.Forms.ToolStripButton();
            this.TbtnDefine3 = new System.Windows.Forms.ToolStripButton();
            this.TbtnSave = new System.Windows.Forms.ToolStripButton();
            this.TbtnCancel = new System.Windows.Forms.ToolStripButton();
            this.TbtnFirst = new System.Windows.Forms.ToolStripButton();
            this.TbtnPrevious = new System.Windows.Forms.ToolStripButton();
            this.TbtnNext = new System.Windows.Forms.ToolStripButton();
            this.TbtnLast = new System.Windows.Forms.ToolStripButton();
            this.TbtnRefresh = new System.Windows.Forms.ToolStripButton();
            this.TbtnExit = new System.Windows.Forms.ToolStripButton();
            this.TbtnPrintPreview = new System.Windows.Forms.ToolStripButton();
            this.TbtnExport = new System.Windows.Forms.ToolStripButton();
            this.TbtnPlace = new System.Windows.Forms.ToolStripButton();
            this.TbtnFilter = new System.Windows.Forms.ToolStripButton();
            this.TbtnAddRow = new System.Windows.Forms.ToolStripButton();
            this.TbtnInsertRow = new System.Windows.Forms.ToolStripButton();
            this.TbtnDeleteRow = new System.Windows.Forms.ToolStripButton();
            this.TbtnCheck = new System.Windows.Forms.ToolStripButton();
            this.TbtnBianGeng = new System.Windows.Forms.ToolStripButton();
            this.TbtnClose = new System.Windows.Forms.ToolStripButton();
            this.SuspendLayout();
            // 
            // TbtnAdd
            // 
            this.TbtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("TbtnAdd.Image")));
            this.TbtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnAdd.Name = "TbtnAdd";
            this.TbtnAdd.Size = new System.Drawing.Size(67, 22);
            this.TbtnAdd.Text = "新增(&A)";
            // 
            // TbtnChange
            // 
            this.TbtnChange.Image = ((System.Drawing.Image)(resources.GetObject("TbtnChange.Image")));
            this.TbtnChange.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnChange.Name = "TbtnChange";
            this.TbtnChange.Size = new System.Drawing.Size(67, 20);
            this.TbtnChange.Text = "修改(&M)";
            // 
            // TbtnDelete
            // 
            this.TbtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("TbtnDelete.Image")));
            this.TbtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnDelete.Name = "TbtnDelete";
            this.TbtnDelete.Size = new System.Drawing.Size(67, 20);
            this.TbtnDelete.Text = "删除(&D)";
            // 
            // TbtnDefine1
            // 
            this.TbtnDefine1.Image = ((System.Drawing.Image)(resources.GetObject("TbtnDefine1.Image")));
            this.TbtnDefine1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnDefine1.Name = "TbtnDefine1";
            this.TbtnDefine1.Size = new System.Drawing.Size(79, 20);
            this.TbtnDefine1.Text = "自定义项1";
            // 
            // TbtnDefine2
            // 
            this.TbtnDefine2.Image = ((System.Drawing.Image)(resources.GetObject("TbtnDefine2.Image")));
            this.TbtnDefine2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnDefine2.Name = "TbtnDefine2";
            this.TbtnDefine2.Size = new System.Drawing.Size(79, 20);
            this.TbtnDefine2.Text = "自定义项2";
            // 
            // TbtnDefine3
            // 
            this.TbtnDefine3.Image = ((System.Drawing.Image)(resources.GetObject("TbtnDefine3.Image")));
            this.TbtnDefine3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnDefine3.Name = "TbtnDefine3";
            this.TbtnDefine3.Size = new System.Drawing.Size(79, 20);
            this.TbtnDefine3.Text = "自定义项3";
            // 
            // TbtnSave
            // 
            this.TbtnSave.Image = ((System.Drawing.Image)(resources.GetObject("TbtnSave.Image")));
            this.TbtnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnSave.Name = "TbtnSave";
            this.TbtnSave.Size = new System.Drawing.Size(67, 20);
            this.TbtnSave.Text = "保存(&S)";
            // 
            // TbtnCancel
            // 
            this.TbtnCancel.Image = ((System.Drawing.Image)(resources.GetObject("TbtnCancel.Image")));
            this.TbtnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnCancel.Name = "TbtnCancel";
            this.TbtnCancel.Size = new System.Drawing.Size(67, 20);
            this.TbtnCancel.Text = "放弃(&C)";
            // 
            // TbtnFirst
            // 
            this.TbtnFirst.Image = ((System.Drawing.Image)(resources.GetObject("TbtnFirst.Image")));
            this.TbtnFirst.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnFirst.Name = "TbtnFirst";
            this.TbtnFirst.Size = new System.Drawing.Size(49, 20);
            this.TbtnFirst.Text = "首张";
            // 
            // TbtnPrevious
            // 
            this.TbtnPrevious.Image = ((System.Drawing.Image)(resources.GetObject("TbtnPrevious.Image")));
            this.TbtnPrevious.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnPrevious.Name = "TbtnPrevious";
            this.TbtnPrevious.Size = new System.Drawing.Size(49, 20);
            this.TbtnPrevious.Text = "上张";
            // 
            // TbtnNext
            // 
            this.TbtnNext.Image = ((System.Drawing.Image)(resources.GetObject("TbtnNext.Image")));
            this.TbtnNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnNext.Name = "TbtnNext";
            this.TbtnNext.Size = new System.Drawing.Size(49, 20);
            this.TbtnNext.Text = "下张";
            // 
            // TbtnLast
            // 
            this.TbtnLast.Image = ((System.Drawing.Image)(resources.GetObject("TbtnLast.Image")));
            this.TbtnLast.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnLast.Name = "TbtnLast";
            this.TbtnLast.Size = new System.Drawing.Size(49, 20);
            this.TbtnLast.Text = "末张";
            // 
            // TbtnRefresh
            // 
            this.TbtnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("TbtnRefresh.Image")));
            this.TbtnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnRefresh.Name = "TbtnRefresh";
            this.TbtnRefresh.Size = new System.Drawing.Size(67, 20);
            this.TbtnRefresh.Text = "刷新(&R)";
            // 
            // TbtnExit
            // 
            this.TbtnExit.Image = ((System.Drawing.Image)(resources.GetObject("TbtnExit.Image")));
            this.TbtnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnExit.Name = "TbtnExit";
            this.TbtnExit.Size = new System.Drawing.Size(67, 20);
            this.TbtnExit.Text = "退出(&Q)";
            // 
            // TbtnPrintPreview
            // 
            this.TbtnPrintPreview.Image = ((System.Drawing.Image)(resources.GetObject("TbtnPrintPreview.Image")));
            this.TbtnPrintPreview.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnPrintPreview.Name = "TbtnPrintPreview";
            this.TbtnPrintPreview.Size = new System.Drawing.Size(67, 20);
            this.TbtnPrintPreview.Text = "打印(&P)";
            // 
            // TbtnExport
            // 
            this.TbtnExport.Image = ((System.Drawing.Image)(resources.GetObject("TbtnExport.Image")));
            this.TbtnExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnExport.Name = "TbtnExport";
            this.TbtnExport.Size = new System.Drawing.Size(67, 20);
            this.TbtnExport.Text = "导出(&E)";
            // 
            // TbtnPlace
            // 
            this.TbtnPlace.Image = ((System.Drawing.Image)(resources.GetObject("TbtnPlace.Image")));
            this.TbtnPlace.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnPlace.Name = "TbtnPlace";
            this.TbtnPlace.Size = new System.Drawing.Size(67, 20);
            this.TbtnPlace.Text = "定位(&G)";
            // 
            // TbtnFilter
            // 
            this.TbtnFilter.Image = ((System.Drawing.Image)(resources.GetObject("TbtnFilter.Image")));
            this.TbtnFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnFilter.Name = "TbtnFilter";
            this.TbtnFilter.Size = new System.Drawing.Size(67, 20);
            this.TbtnFilter.Text = "过滤(&F)";
            // 
            // TbtnAddRow
            // 
            this.TbtnAddRow.Image = ((System.Drawing.Image)(resources.GetObject("TbtnAddRow.Image")));
            this.TbtnAddRow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnAddRow.Name = "TbtnAddRow";
            this.TbtnAddRow.Size = new System.Drawing.Size(67, 20);
            this.TbtnAddRow.Text = "增行(&N)";
            // 
            // TbtnInsertRow
            // 
            this.TbtnInsertRow.Image = ((System.Drawing.Image)(resources.GetObject("TbtnInsertRow.Image")));
            this.TbtnInsertRow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnInsertRow.Name = "TbtnInsertRow";
            this.TbtnInsertRow.Size = new System.Drawing.Size(67, 20);
            this.TbtnInsertRow.Text = "插行(&I)";
            // 
            // TbtnDeleteRow
            // 
            this.TbtnDeleteRow.Image = ((System.Drawing.Image)(resources.GetObject("TbtnDeleteRow.Image")));
            this.TbtnDeleteRow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnDeleteRow.Name = "TbtnDeleteRow";
            this.TbtnDeleteRow.Size = new System.Drawing.Size(67, 20);
            this.TbtnDeleteRow.Text = "删行(&O)";
            // 
            // TbtnCheck
            // 
            this.TbtnCheck.Image = ((System.Drawing.Image)(resources.GetObject("TbtnCheck.Image")));
            this.TbtnCheck.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnCheck.Name = "TbtnCheck";
            this.TbtnCheck.Size = new System.Drawing.Size(49, 20);
            this.TbtnCheck.Text = "审核";
            // 
            // TbtnBianGeng
            // 
            this.TbtnBianGeng.Image = ((System.Drawing.Image)(resources.GetObject("TbtnBianGeng.Image")));
            this.TbtnBianGeng.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnBianGeng.Name = "TbtnBianGeng";
            this.TbtnBianGeng.Size = new System.Drawing.Size(49, 20);
            this.TbtnBianGeng.Text = "变更";
            // 
            // TbtnClose
            // 
            this.TbtnClose.Image = ((System.Drawing.Image)(resources.GetObject("TbtnClose.Image")));
            this.TbtnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TbtnClose.Name = "TbtnClose";
            this.TbtnClose.Size = new System.Drawing.Size(49, 20);
            this.TbtnClose.Text = "关闭";
            // 
            // CToolStrip
            // 
            this.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TbtnAdd,
            this.TbtnChange,
            this.TbtnDelete,
            this.TbtnSave,
            this.TbtnCancel,
            this.TbtnFirst,
            this.TbtnPrevious,
            this.TbtnNext,
            this.TbtnLast,
            this.TbtnRefresh,
            this.TbtnExit,
            this.TbtnPrintPreview,
            this.TbtnExport,
            this.TbtnPlace,
            this.TbtnFilter,
            this.TbtnAddRow,
            this.TbtnInsertRow,
            this.TbtnDeleteRow,
            this.TbtnCheck,
            this.TbtnBianGeng,
            this.TbtnClose,
            this.TbtnDefine1,
            this.TbtnDefine2,
            this.TbtnDefine3});
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripButton TbtnAdd;
        private System.Windows.Forms.ToolStripButton TbtnChange;
        private System.Windows.Forms.ToolStripButton TbtnDelete;
        private System.Windows.Forms.ToolStripButton TbtnDefine1;
        private System.Windows.Forms.ToolStripButton TbtnDefine2;
        private System.Windows.Forms.ToolStripButton TbtnDefine3;
        private System.Windows.Forms.ToolStripButton TbtnSave;
        private System.Windows.Forms.ToolStripButton TbtnCancel;
        private System.Windows.Forms.ToolStripButton TbtnFirst;
        private System.Windows.Forms.ToolStripButton TbtnPrevious;
        private System.Windows.Forms.ToolStripButton TbtnNext;
        private System.Windows.Forms.ToolStripButton TbtnLast;
        private System.Windows.Forms.ToolStripButton TbtnRefresh;
        private System.Windows.Forms.ToolStripButton TbtnExit;
        private System.Windows.Forms.ToolStripButton TbtnPrintPreview;
        private System.Windows.Forms.ToolStripButton TbtnExport;
        private System.Windows.Forms.ToolStripButton TbtnPlace;
        private System.Windows.Forms.ToolStripButton TbtnFilter;
        private System.Windows.Forms.ToolStripButton TbtnAddRow;
        private System.Windows.Forms.ToolStripButton TbtnInsertRow;
        private System.Windows.Forms.ToolStripButton TbtnDeleteRow;
        private System.Windows.Forms.ToolStripButton TbtnCheck;
        private System.Windows.Forms.ToolStripButton TbtnBianGeng;
        private System.Windows.Forms.ToolStripButton TbtnClose;
    }
}
