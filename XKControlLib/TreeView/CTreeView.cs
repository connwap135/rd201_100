﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace XKControlLib
{
    public partial class CTreeView : TreeView
    {
        public CTreeView()
        {
            InitializeComponent();
        }

        public CTreeView(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <summary>
        /// 展开节点
        /// </summary>
        /// <param name="nodeName">节点名称</param>
        public void ExpandNodes(string nodeName)
        {
            var first = GetNodes().Where(x=>x.Text.Equals(nodeName)).First();
            if (first != null) { first.Expand(); }
        }

        public TreeNode GetNode_byLevelOne(string text)
        {
            foreach (TreeNode item in this.Nodes)
            {
                if (item.Text.Equals(text)) { return item; }
            }
            return null;
        }

        public List<TreeNode> GetNodes(TreeNode ParentNode)
        {
            var Nodes = new List<TreeNode>();
            AppendNodes(Nodes, ParentNode);
            return Nodes;
        }

        public List<TreeNode> GetNodes()
        {
            var Nodes = new List<TreeNode>();
            foreach (TreeNode node in this.Nodes)
            {
                Nodes.Add(node);
                AppendNodes(Nodes, node);
            }
            return Nodes;
        }

        protected void AppendNodes(List<TreeNode> Nodes,TreeNode ParentNode)
        {
            foreach (TreeNode n in ParentNode.Nodes)
            {
                Nodes.Add(n);
                AppendNodes(Nodes, n);
            }
        }

        public void FillGradeInfo(List<GradeInfo> GradeInfos,bool bShowCode = true)
        {
            var Code = string.Empty;
            if (this.SelectedNode != null)
            {
                Code = this.SelectedNode.Text;
            }
            this.Nodes.Clear();
            var node = new TreeNode
            {
                Tag = "All",
                Text = "所有分类" 
            };
            this.Nodes.Add(node);
            AddNode(node, GradeInfos, 1, bShowCode);
            foreach (TreeNode n in GetNodes())
            {
                if (Code.Equals(n.Text))
                {
                    this.SelectedNode = n;
                }
            }
            if(this.SelectedNode is null)
            {
                this.SelectedNode = this.Nodes[0];
            }
        }

        protected void AddNode(TreeNode node, List<GradeInfo> gradeInfos, int v, bool bShowCode)
        {
            try
            {
                var cPart = node.Tag.ToString();
                if (cPart == "All") { cPart = ""; }
                var mdls = gradeInfos.Where(x => x.IGrade == v).Where(x => x.CCode.StartsWith(cPart)).ToList();
                
                foreach (var item in mdls)
                {
                    var n = new TreeNode
                    {
                        Tag = item.CCode,
                        Text = (bShowCode ? "(" + item.CCode + ")" : "").ToString() + item.CName
                    };
                    node.Nodes.Add(n);
                    AddNode(n, gradeInfos, item.IGrade + 1, bShowCode);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
           
        }
    }

    /// <summary>
    /// 节点信息
    /// </summary>
    public class GradeInfo
    {
        public string CCode { get; set; }
        public string CName { get; set; }
        public int IGrade { get; set; }
    }
}
