﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace XKControlLib
{
    [DefaultEvent("NaviItemClicked")]
    [Designer(typeof(MyControlDesigner))]
    public partial class NaviBar : UserControl
    {

        [Description("隐藏按钮点击事件")]
        public event EventHandler HideBtnClick;

        public delegate void delegate_NaviItemClickedEventArgs(object sender, ToolStripItemClickedEventArgs e);
        public delegate void delegate_TreeItemSelectEventArgs(object sender, TreeViewEventArgs e);
        [Description("导航按钮点击事件")]
        public event delegate_NaviItemClickedEventArgs NaviItemClicked;
        public event delegate_TreeItemSelectEventArgs TreeItemClick;




        public NaviBar()
        {
            InitializeComponent();
            this.HideButton.Click += HideButton_Click;
            tlstrpNaviButtons.ItemAdded += TlstrpNaviButtons_ItemAdded;
            tlstrpNaviButtons.ItemClicked += TlstrpNaviButtons_ItemClicked;
            treeView1.AfterSelect += TreeView1_AfterSelect;
        }

        private void TreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeItemClick(sender, e);
        }

        [DefaultValue(false)]
        public bool Renderer
        {
            get => _renderer; set
            {
                _renderer = value;
                if (value)
                {
                    tlstrpNaviButtons.Renderer = new ProToolstripRenderer(false);
                    tlsNaviHelper.Renderer = new ProToolstripRenderer(false);
                }
                else
                {
                    tlstrpNaviButtons.Renderer = new ProToolstripRenderer(true);
                    tlsNaviHelper.Renderer = new ProToolstripRenderer(true);
                }
                this.Refresh();
            }
        }


        private string _text;
        private bool _renderer;

        [Category("外观")]
        [DefaultValue("")]
        [Browsable(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Localizable(true)]
        public override string Text
        {
            get => _text;
            set
            {
                _text = value;
                toolTitle.Text = _text;
                this.tlsNaviHelper.Text = _text;
            }
        }

        private void TlstrpNaviButtons_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            foreach (var item in this.tlstrpNaviButtons.Items)
            {
                if (item != e.ClickedItem && item is ToolStripButton)
                {
                    (item as ToolStripButton).Checked = false;
                }
            }
            NaviItemClicked?.Invoke(sender, e);
        }

        private void TlstrpNaviButtons_ItemAdded(object sender, ToolStripItemEventArgs e)
        {

            if (!(e.Item is ToolStripButton aa)) { return; }
            if (aa.Image == null || aa.Text == null) { throw new Exception("Image属性和Text属性是必须的"); }
            aa.BackColor = Color.Transparent;
            aa.BackgroundImageLayout = ImageLayout.Stretch;
            aa.CheckOnClick = true;
            aa.Font = new Font("Tahoma", 9.75F, FontStyle.Bold);
            aa.ImageAlign = ContentAlignment.MiddleLeft;
            aa.ImageScaling = ToolStripItemImageScaling.None;
            aa.ImageTransparentColor = Color.Transparent;
            aa.Margin = new Padding(0);
            aa.Padding = new Padding(3);
            aa.Size = new Size(198, 42);
            aa.AutoToolTip = false;
            aa.Tag = aa.Text;
            aa.TextAlign = ContentAlignment.MiddleLeft;
            if (tlstrpNaviButtons.Items.Count > 0)
            {
                int btnHeight = tlstrpNaviButtons.Items[0].Height * tlstrpNaviButtons.Items.Count;
                if (splitContainer1.Height - btnHeight > splitContainer1.Width * 1.618f)
                {
                    splitContainer1.Panel2MinSize = btnHeight;
                    splitContainer1.SplitterDistance = splitContainer1.Height - btnHeight;
                }
            }
        }

        private void HideButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            HideBtnClick?.Invoke(this, e);
        }

        public void TreeShow()
        {
            splitContainer1.Panel1Collapsed = false;
        }

        public void TreeHide()
        {
            splitContainer1.Panel1Collapsed = true;
        }

        public void TreeShowChanged()
        {
            if (splitContainer1.Panel1Collapsed)
            {
                splitContainer1.Panel1Collapsed = false;
            }
            else
            {
                splitContainer1.Panel1Collapsed = true;
            }
        }

        protected override AccessibleObject CreateAccessibilityInstance()
        {
            return base.CreateAccessibilityInstance();
        }

        [Category("杂项")]
        [Browsable(true)]
        public ToolStripButton HideButton => this.tsbtnPin;

        [Category("杂项")]
        [Browsable(true)]
        public System.Windows.Forms.ToolStrip NaviBtn => tlstrpNaviButtons;

        [Category("杂项")]
        [Browsable(true)]
        public TreeView NaviTree => treeView1;

        public override Color BackColor
        {
            get => base.BackColor;
            set
            {
                base.BackColor = value;
                treeView1.BackColor = value;
                splitContainer1.BackColor = value;
                tlsNaviHelper.BackColor = value;
                tlstrpNaviButtons.BackColor = value;
            }
        }
    }
}
