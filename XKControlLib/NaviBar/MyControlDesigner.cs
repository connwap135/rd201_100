﻿using System;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace XKControlLib
{
    public class MyControlDesigner : ControlDesigner
    {
        //public override DesignerVerbCollection Verbs => base.Verbs;



        public override DesignerVerbCollection Verbs
        {
            get
            {
                DesignerVerbCollection verbs = new DesignerVerbCollection();
                var d = new DockingAttribute();
                DesignerVerb dv1 = new DesignerVerb("Display Component Name", new EventHandler(this.ShowComponentName));
                DesignerVerb dv2 = new DesignerVerb("Dock", aaf);
                verbs.Add(dv1);
                verbs.Add(dv2);
                return verbs;
            }
        }

        private void aaf(object sender, EventArgs e)
        {
            if (Control is NaviBar r)
            {
                if (r.Dock == DockStyle.Fill)
                {
                    r.Dock = DockStyle.None;
                }
                else
                {
                    r.Dock = DockStyle.Fill;
                }
                
                r.Refresh();
            }
        }

        private void ShowComponentName(object sender, EventArgs e)
        {
            if (Control is NaviBar r)
            {
                r.Dock = DockStyle.Left;
                r.Refresh();
            }

            if (this.Component != null)
                MessageBox.Show(this.Component.Site.Name, "Designer Component's Name");
        }


        //public override void DoDefaultAction()
        //{
        //    MessageBox.Show("The DoDefaultAction method of an IDesigner implementation was invoked.", "Information");
        //}

    }
}
