﻿using System.ComponentModel;
using System.Windows.Forms;
namespace XKControlLib
{
    partial class NaviBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NaviBar));
            this.tsbtnPin = new System.Windows.Forms.ToolStripButton();
            this.btnInbox = new System.Windows.Forms.ToolStripButton();
            this.btnSearch = new System.Windows.Forms.ToolStripButton();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.lstViewImageList = new System.Windows.Forms.ImageList(this.components);
            this.tlsNaviHelper = new System.Windows.Forms.ToolStrip();
            this.toolTitle = new System.Windows.Forms.ToolStripLabel();
            this.tlstrpNaviButtons = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tlsNaviHelper.SuspendLayout();
            this.tlstrpNaviButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsbtnPin
            // 
            this.tsbtnPin.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnPin.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnPin.Image")));
            this.tsbtnPin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnPin.Margin = new System.Windows.Forms.Padding(0);
            this.tsbtnPin.Name = "tsbtnPin";
            this.tsbtnPin.Size = new System.Drawing.Size(23, 25);
            this.tsbtnPin.Text = ">>";
            this.tsbtnPin.ToolTipText = "隐藏面板";
            // 
            // btnInbox
            // 
            this.btnInbox.AutoToolTip = false;
            this.btnInbox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnInbox.Checked = true;
            this.btnInbox.CheckOnClick = true;
            this.btnInbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btnInbox.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnInbox.Image = global::XKControlLib.Properties.Resources.send32;
            this.btnInbox.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInbox.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnInbox.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.btnInbox.Margin = new System.Windows.Forms.Padding(0);
            this.btnInbox.Name = "btnInbox";
            this.btnInbox.Size = new System.Drawing.Size(198, 36);
            this.btnInbox.Tag = "Inbox";
            this.btnInbox.Text = "Inbox";
            this.btnInbox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSearch
            // 
            this.btnSearch.AutoToolTip = false;
            this.btnSearch.BackColor = System.Drawing.Color.Transparent;
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSearch.CheckOnClick = true;
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnSearch.Image = global::XKControlLib.Properties.Resources.find32;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnSearch.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.btnSearch.Margin = new System.Windows.Forms.Padding(0);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Padding = new System.Windows.Forms.Padding(3);
            this.btnSearch.Size = new System.Drawing.Size(198, 42);
            this.btnSearch.Tag = "Search";
            this.btnSearch.Text = "Search";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // treeView1
            // 
            this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.ImageKey = "folder-open-4.ico";
            this.treeView1.ImageList = this.lstViewImageList;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.Size = new System.Drawing.Size(200, 246);
            this.treeView1.StateImageList = this.lstViewImageList;
            this.treeView1.TabIndex = 5;
            // 
            // lstViewImageList
            // 
            this.lstViewImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("lstViewImageList.ImageStream")));
            this.lstViewImageList.Tag = "lstViewImageList";
            this.lstViewImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.lstViewImageList.Images.SetKeyName(0, "folder-open.ico");
            this.lstViewImageList.Images.SetKeyName(1, "folder-open-4.ico");
            this.lstViewImageList.Images.SetKeyName(2, "File.ico");
            // 
            // tlsNaviHelper
            // 
            this.tlsNaviHelper.BackColor = System.Drawing.Color.Transparent;
            this.tlsNaviHelper.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tlsNaviHelper.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnPin,
            this.toolTitle});
            this.tlsNaviHelper.Location = new System.Drawing.Point(0, 0);
            this.tlsNaviHelper.Name = "tlsNaviHelper";
            this.tlsNaviHelper.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tlsNaviHelper.Size = new System.Drawing.Size(200, 25);
            this.tlsNaviHelper.TabIndex = 6;
            this.tlsNaviHelper.Text = "toolStrip1";
            // 
            // toolTitle
            // 
            this.toolTitle.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolTitle.Name = "toolTitle";
            this.toolTitle.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.toolTitle.Size = new System.Drawing.Size(60, 22);
            this.toolTitle.Text = "Title";
            this.toolTitle.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            // 
            // tlstrpNaviButtons
            // 
            this.tlstrpNaviButtons.AutoSize = false;
            this.tlstrpNaviButtons.BackColor = System.Drawing.Color.White;
            this.tlstrpNaviButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlstrpNaviButtons.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tlstrpNaviButtons.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnInbox,
            this.btnSearch});
            this.tlstrpNaviButtons.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.tlstrpNaviButtons.Location = new System.Drawing.Point(0, 0);
            this.tlstrpNaviButtons.Name = "tlstrpNaviButtons";
            this.tlstrpNaviButtons.Size = new System.Drawing.Size(200, 124);
            this.tlstrpNaviButtons.TabIndex = 7;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 23);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
            this.splitContainer1.Panel1MinSize = 0;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tlstrpNaviButtons);
            this.splitContainer1.Panel2MinSize = 60;
            this.splitContainer1.Size = new System.Drawing.Size(200, 371);
            this.splitContainer1.SplitterDistance = 246;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 2;
            // 
            // NaviBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.tlsNaviHelper);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "NaviBar";
            this.Size = new System.Drawing.Size(200, 396);
            this.tlsNaviHelper.ResumeLayout(false);
            this.tlsNaviHelper.PerformLayout();
            this.tlstrpNaviButtons.ResumeLayout(false);
            this.tlstrpNaviButtons.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal ToolStripButton tsbtnPin;
        internal ToolStripButton btnInbox;
        internal ToolStripButton btnSearch;
        internal TreeView treeView1;
        internal System.Windows.Forms.ToolStrip tlsNaviHelper;
        internal System.Windows.Forms.ToolStrip tlstrpNaviButtons;
        internal ToolStripButton toolStripButton1;
        internal SplitContainer splitContainer1;
        internal ToolStripLabel toolTitle;
        internal ImageList lstViewImageList;
    }
}
