Imports System.Collections.Generic
Imports System.Text
Imports System.Runtime.InteropServices

Namespace RFIDLIB
    Public Delegate Sub RFIDLIB_EVENT_CALLBACK(wparam As UIntPtr, lparam As UIntPtr)
    Public Enum COMMTYPE
        USB
        COM
    End Enum

    Class Rfidlib_def
        ' Air protocol id
        Public Const RFID_APL_UNKNOWN_ID As Integer = 0
        Public Const RFID_APL_ISO15693_ID As Integer = 1
        Public Const RFID_APL_ISO14443A_ID As Integer = 2
        ' Tag type id
        Public Const RFID_UNKNOWN_PICC_ID As Integer = 0
        Public Const RFID_ISO15693_PICC_ICODE_SLI_ID As Integer = 1
        Public Const RFID_ISO15693_PICC_TI_HFI_PLUS_ID As Integer = 2
        Public Const RFID_ISO15693_PICC_ST_M24LRXX_ID As Integer = 3
        ' ST M24 serial 
        Public Const RFID_ISO15693_PICC_FUJ_MB89R118C_ID As Integer = 4
        Public Const RFID_ISO15693_PICC_ST_M24LR64_ID As Integer = 5
        Public Const RFID_ISO15693_PICC_ST_M24LR16E_ID As Integer = 6
        Public Const RFID_ISO15693_PICC_ICODE_SLIX_ID As Integer = 7
        Public Const RFID_ISO15693_PICC_TIHFI_STANDARD_ID As Integer = 8
        Public Const RFID_ISO15693_PICC_TIHFI_PRO_ID As Integer = 9

        '查询类型
        ''' <summary>
        ''' 新查询，在Inventory之前使所有标签进入Ready 状态
        ''' </summary>
        Public Const AI_TYPE_NEW As Integer = 1

        ''' <summary>
        ''' 继续查询，处于Quiet状态的标签不会被读到
        ''' </summary>
        Public Const AI_TYPE_CONTINUE As Integer = 2

        ' 标签数据报告定位

        ''' <summary>
        ''' 光标位置不变，取出当前的记录。
        ''' </summary>
        Public Const RFID_NO_SEEK As Integer = 0

        ''' <summary>
        ''' 光标定位到第一条，取出当前的记录
        ''' </summary>
        Public Const RFID_SEEK_FIRST As Integer = 1

        ''' <summary>
        ''' 光标定位到下一条，取出当前的记录
        ''' </summary>
        Public Const RFID_SEEK_NEXT As Integer = 2

        ''' <summary>
        ''' 光标定位到最后一条，取出当前的记录
        ''' </summary>
        Public Const RFID_SEEK_LAST As Integer = 3

        '
        '        usb enum information
        '        

        Public Const HID_ENUM_INF_TYPE_SERIALNUM As Integer = 1
        Public Const HID_ENUM_INF_TYPE_DRIVERPATH As Integer = 2




        '
        '        * Open connection string 
        '        

        Public Const CONNSTR_NAME_RDTYPE As String = "RDType"
        Public Const CONNSTR_NAME_COMMTYPE As String = "CommType"

        Public Const CONNSTR_NAME_COMMTYPE_COM As String = "COM"
        Public Const CONNSTR_NAME_COMMTYPE_USB As String = "USB"
        Public Const CONNSTR_NAME_COMMTYPE_NET As String = "NET"

        'HID Param
        Public Const CONNSTR_NAME_HIDADDRMODE As String = "AddrMode"
        Public Const CONNSTR_NAME_HIDSERNUM As String = "SerNum"
        'COM Param
        Public Const CONNSTR_NAME_COMNAME As String = "COMName"
        Public Const CONNSTR_NAME_COMBARUD As String = "BaudRate"
        Public Const CONNSTR_NAME_COMFRAME As String = "Frame"
        Public Const CONNSTR_NAME_BUSADDR As String = "BusAddr"
        'TCP,UDP
        Public Const CONNSTR_NAME_REMOTEIP As String = "RemoteIP"
        Public Const CONNSTR_NAME_REMOTEPORT As String = "RemotePort"
        Public Const CONNSTR_NAME_LOCALIP As String = "LocalIP"



        '
        '* Get loaded reader driver option 
        '

        Public Const LOADED_RDRDVR_OPT_CATALOG As String = "CATALOG"
        Public Const LOADED_RDRDVR_OPT_NAME As String = "NAME"
        Public Const LOADED_RDRDVR_OPT_ID As String = "ID"
        Public Const LOADED_RDRDVR_OPT_COMMTYPESUPPORTED As String = "COMM_TYPE_SUPPORTED"

        '
        '* Reader driver type
        '

        ''' <summary>
        ''' 读卡器
        ''' </summary>
        Public Const RDRDVR_TYPE_READER As String = "Reader"

        ''' <summary>
        ''' 会议签到门
        ''' </summary>
        Public Const RDRDVR_TYPE_MTGATE As String = "MTGate"

        ''' <summary>
        ''' 图书馆安全门
        ''' </summary>
        Public Const RDRDVR_TYPE_LSGATE As String = "LSGate"

        ' supported product type 


        Public Const COMMTYPE_COM_EN As Byte = &H1
        Public Const COMMTYPE_USB_EN As Byte = &H2
        Public Const COMMTYPE_NET_EN As Byte = &H4


    End Class
    Class Rfidlib_aip_iso15693
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function ISO15693_GetLibVersion(buf As StringBuilder, nSize As UInt32) As UInt32
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function ISO15693_CreateInvenParam(hInvenParamSpecList As UIntPtr, AntennaID As Byte, en_afi As Byte, afi As Byte, slot_type As Byte) As UIntPtr
        End Function
        '<DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall)>
        'Public Shared Function ISO15693_ParseTagDataReport(hTagReport As UIntPtr, ByRef aip_id As UInteger, ByRef tag_id As UInteger, ByRef ant_id As UInteger, ByRef dsfid As Byte, ByVal uid As Byte()) As Integer
        'End Function
        Public Declare Function ISO15693_ParseTagDataReport Lib "rfidlib_aip_iso15693.dll" Alias "ISO15693_ParseTagDataReport" (ByVal hTagReport As UIntPtr, ByRef aip_id As UInteger, ByRef tag_id As UInteger, ByRef ant_id As UInteger, ByRef dsfid As Byte, ByVal uid As Byte()) As Integer

        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function ISO15693_Connect(hr As UIntPtr, tagType As UInt32, address_mode As Byte, uid As Byte(), ByRef ht As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function ISO15693_ReadSingleBlock(hr As UIntPtr, ht As UIntPtr, readSecSta As Byte, blkAddr As UInt32, bufBlockDat As Byte(), nSize As UInt32,
   ByRef bytesBlkDatRead As UInt32) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function ISO15693_WriteSingleBlock(hr As UIntPtr, ht As UIntPtr, blkAddr As UInt32, newBlkData As Byte(), bytesToWrite As UInt32) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function ISO15693_LockBlock(hr As UIntPtr, ht As UIntPtr, blkAddr As UInt32) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function ISO15693_ReadMultiBlocks(hr As UIntPtr, ht As UIntPtr, readSecSta As Byte, blkAddr As UInt32, numOfBlksToRead As UInt32, ByRef numOfBlksRead As UInt32,
   bufBlocks As Byte(), nSize As UInt32, ByRef bytesBlkDatRead As UInt32) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function ISO15693_WriteMultipleBlocks(hr As UIntPtr, ht As UIntPtr, blkAddr As UInt32, numOfBlks As UInt32, newBlksData As Byte(), bytesToWrite As UInt32) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function ISO15693_WriteAFI(hr As UIntPtr, ht As UIntPtr, afi As Byte) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function ISO15693_LockAFI(hr As UIntPtr, ht As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function ISO15693_WriteDSFID(hr As UIntPtr, ht As UIntPtr, dsfid As Byte) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function ISO15693_LockDSFID(hr As UIntPtr, ht As UIntPtr) As Integer
        End Function
        ' out: tag uid 
        ' out:DSFID 
        ' out:AFI 
        ' out: block size 
        ' out:number of total blocks 
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function ISO15693_GetSystemInfo(hr As UIntPtr, ht As UIntPtr, uid As Byte(), ByRef dsfid As Byte, ByRef afi As Byte, ByRef blkSize As UInt32,
   ByRef numOfBloks As UInt32, ByRef icRef As Byte) As Integer
            ' out:ic reference 
        End Function
        ' in: size of the buffer 
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function ISO15693_GetBlockSecStatus(hr As UIntPtr, ht As UIntPtr, blkAddr As UInt32, numOfBlks As UInt32, bufBlkSecs As Byte(), nSize As UInt32,
   ByRef bytesSecRead As UInt32) As Integer
            'out:number of block status byte copied
        End Function


    End Class
#Region "Reader"
    Class Rfidlib_reader

        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_GetLibVersion(buf As StringBuilder, nSize As UInt32) As UInt32
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_LoadReaderDrivers(drvpath As String) As Integer
        End Function
        ''' <summary>
        ''' 获取已加载的设备驱动的数量
        ''' </summary>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_GetLoadedReaderDriverCount() As UInt32
        End Function
        ''' <summary>
        ''' 获取已加载的设备驱动的参数值，包含的参数有驱动类别，名称，ID，支持的接口类型。驱动类别分为Reader(读卡器类),MTGate(会议签到门类)，LSGate(图书馆安全门类)
        ''' </summary>
        ''' <param name="idx"></param>
        ''' <param name="[option]"></param>
        ''' <param name="valueBuffer"></param>
        ''' <param name="nSize"></param>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_GetLoadedReaderDriverOpt(idx As UInt32, [option] As String, valueBuffer As StringBuilder, ByRef nSize As UInt32) As Integer
        End Function

        ''' <summary>
        ''' 枚举已连接计算机的USB设备，需要传入设备驱动的名称。
        ''' </summary>
        ''' <param name="DevId"></param>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function HID_Enum(DevId As String) As UInt32
        End Function
        ''' <summary>
        ''' 获取已枚举到的USB设备的信息，如系列号，驱动路径等。
        ''' </summary>
        ''' <param name="indx"></param>
        ''' <param name="infType"></param>
        ''' <param name="infBuffer"></param>
        ''' <param name="nSize"></param>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function HID_GetEnumItem(indx As UInt32, infType As Byte, infBuffer As StringBuilder, ByRef nSize As UInt32) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function COMPort_Enum() As UInt32
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function COMPort_GetEnumItem(idx As UInt32, nameBuf As StringBuilder, nSize As UInt32) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_Open(connStr As String, ByRef hrOut As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_Close(hr As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_CreateInvenParamSpecList() As UIntPtr
        End Function
        ''' <summary>
        ''' 寻找标签
        ''' </summary>
        ''' <param name="hr"></param>
        ''' <param name="AIType"></param>
        ''' <param name="AntennaCoun"></param>
        ''' <param name="AntennaIDs"></param>
        ''' <param name="InvenParamSpecList"></param>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_TagInventory(hr As UIntPtr, AIType As Byte, AntennaCoun As Byte, AntennaIDs As Byte(), InvenParamSpecList As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_GetTagDataReportCount(hr As UIntPtr) As UInt32
        End Function
        ''' <summary>
        ''' 获取Inventory的标签数据报告数据节点
        ''' </summary>
        ''' <param name="hr"></param>
        ''' <param name="seek"></param>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_GetTagDataReport(hr As UIntPtr, seek As Byte) As UIntPtr
        End Function
        ''' <summary>
        ''' 使能在Inventory过程中异步输出读到的标签信息报告
        ''' </summary>
        ''' <param name="hr"></param>
        ''' <param name="type">1 Windows 消息，2 回调函数</param>
        ''' <param name="msg"></param>
        ''' <param name="hwnd"></param>
        ''' <param name="cb"></param>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_EnableAsyncTagReportOutput(hr As UIntPtr, type As Byte, msg As UInt32, hwnd As UIntPtr, <[In]()> cb As RFIDLIB_EVENT_CALLBACK) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_DisableAsyncTagReportOutput(hr As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_TagDisconnect(hr As UIntPtr, hTag As UIntPtr) As Integer
        End Function
        Public Declare Function RDR_DisconnectAllTags Lib "rfidlib_reader.dll" Alias "RDR_DisconnectAllTags" (ByVal hr As UIntPtr) As Integer
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_SetAcessAntenna(hr As UIntPtr, AntennaID As Byte) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_OpenRFTransmitter(hr As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_CloseRFTransmitter(hr As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_SetCommuImmeTimeout(hr As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_ResetCommuImmeTimeout(hr As UIntPtr) As Integer
        End Function
        ''' <summary>
        ''' 获取天线接口数量
        ''' </summary>
        ''' <param name="hr"></param>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_GetAntennaInterfaceCount(hr As UIntPtr) As UInt32
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_GetReaderInfor(hr As UIntPtr, Type As Byte, buffer As StringBuilder, ByRef nSize As UInt32) As Integer
        End Function

        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_ExeSpecialControlCmd(hr As UIntPtr, cmd As String, parameters As String, result As StringBuilder, ByRef nSize As UInt32) As Integer
        End Function

        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_ConfigBlockWrite(hr As UIntPtr, cfgno As UInt32, cfgdata As Byte(), nSize As UInt32, mask As UInt32) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_ConfigBlockRead(hr As UIntPtr, cfgno As UInteger, cfgbuff As Byte(), nSize As UInteger) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_ConfigBlockSave(hr As UIntPtr, cfgno As UInt32) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_CreateRS485Node(hr As UIntPtr, busAddr As UInt32, ByRef ohrRS485Node As UIntPtr) As Integer
        End Function
        ''' <summary>
        ''' 获取支持的空中接口协议
        ''' </summary>
        ''' <param name="hr"></param>
        ''' <param name="index"></param>
        ''' <param name="AIPType"></param>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_GetSupportedAirInterfaceProtocol(hr As UIntPtr, index As UInt32, ByRef AIPType As UInt32) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_GetAirInterfaceProtName(hr As UIntPtr, AIPType As UInt32, namebuf As StringBuilder, nSize As UInt32) As Integer
        End Function

        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_GetOutputCount(hr As UIntPtr, ByRef nSize As Byte) As Integer
        End Function

        ''' <summary>
        ''' 获取输出端口名称
        ''' </summary>
        ''' <param name="hr">读卡器句柄</param>
        ''' <param name="idxOut">输出端口编号,从1开始。</param>
        ''' <param name="bufName">字符缓冲用于接收输出端口名称</param>
        ''' <param name="nSize">字符缓冲区的最大字符数</param>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_GetOutputName(hr As UIntPtr, idxOut As Byte, bufName As StringBuilder, ByRef nSize As UInt32) As Integer
        End Function

        ''' <summary>
        ''' 创建RDR_SetOutput的输出端口的操作集合参数
        ''' </summary>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_CreateSetOutputOperations() As UIntPtr
        End Function

        ''' <summary>
        ''' 往输出端口的操作集合增加一个输出端口操作
        ''' </summary>
        ''' <param name="hOperas">输出端口的操作集合的句柄，由RDR_CreateSetOutputOperations创建</param>
        ''' <param name="outNo">输出端口编号，从1开始</param>
        ''' <param name="outMode">输出模式。
        ''' 0	使用读卡器默认配置
        ''' 1   输出端口一直开
        ''' 2   输出端口一直关
        ''' 3   输出端口闪烁
        ''' </param>
        ''' <param name="outFrequency">闪烁频率，只有outMode =3时才有效。</param>
        ''' <param name="outActiveDuration">输出端口激活时间，只有outMode =3时才有效。</param>
        ''' <param name="outInactiveDuration">输出端口停顿时间，只有outMode =3而且outFrequency>1时才有效。</param>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_AddOneOutputOperation(hOperas As UIntPtr, outNo As Byte, outMode As Byte, outFrequency As UInteger, outActiveDuration As UInteger, outInactiveDuration As UInteger) As Integer
        End Function

        ''' <summary>
        ''' 控制输出端口输出
        ''' </summary>
        ''' <param name="hr">读卡器句柄</param>
        ''' <param name="outputOpers">输出端口的操作集合的句柄，由RDR_CreateSetOutputOperations创建</param>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_SetOutput(hr As UIntPtr, outputOpers As UIntPtr) As Integer
        End Function

        ''' <summary>
        ''' 使设备恢复出厂设置
        ''' </summary>
        ''' <param name="hr"></param>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_LoadFactoryDefault(hr As UIntPtr) As Integer
        End Function

        ''' <summary>
        ''' 重启设备
        ''' </summary>
        ''' <param name="hr"></param>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_SystemReset(hr As UIntPtr) As Integer
        End Function

        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_CreateSetGetConfigItemList() As UIntPtr
        End Function

        ''' <summary>
        ''' 设置读卡器配置
        ''' </summary>
        ''' <param name="hr">读卡器句柄</param>
        ''' <param name="CfgItemSettingData">配置项集合句柄，由RDR_CreateSetGetConfigItemList创建。</param>
        ''' <returns></returns>
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_SetConfig(hr As UIntPtr, CfgItemSettingData As UIntPtr) As Integer
        End Function

        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function RDR_GetConfig(hr As UIntPtr, CfgItemSettingData As UIntPtr) As Integer
        End Function

    End Class


#End Region
    Class Rfidlib_data_node
        <DllImport("rfidlib_data_node.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function DNODE_GetLibVersion(buf As StringBuilder, nSize As UInt32) As UInt32
        End Function
        ''' <summary>
        ''' 释放顶层数据节点，子节点由父节点自动销毁
        ''' </summary>
        ''' <param name="dn"></param>
        ''' <returns></returns>
        <DllImport("rfidlib_data_node.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)>
        Public Shared Function DNODE_Destroy(dn As UIntPtr) As Integer
        End Function
    End Class

    Public Class TagReportEvent
        Inherits Object
        Public aipType As UInteger
        Public tagType As UInteger
        Public antID As UInteger
        Public dsfid As Byte
        Public uid As Byte()
        Private Sub New()
            aipType = 0
            tagType = 0
            antID = 0
            dsfid = 0
            uid = New Byte(7) {}
        End Sub

    End Class

    Public Class CReaderDriverInf
        ''' <summary>
        ''' 驱动类别
        ''' </summary>
        Public m_catalog As String
        ''' <summary>
        ''' 名称
        ''' </summary>
        Public m_name As String

        ''' <summary>
        ''' ID
        ''' </summary>
        Public m_productType As String
        ''' <summary>
        ''' 支持的接口类型
        ''' </summary>
        Public m_commTypeSupported As UInt32
    End Class

    Public Class CSupportedAirProtocol
        Public m_ID As UInteger
        Public m_name As String
        Public m_en As Boolean
    End Class
End Namespace