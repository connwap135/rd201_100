﻿Public Class RFIDEventArgs
    Inherits EventArgs

    ''' <summary>
    ''' 返回获取到的标签ID
    ''' </summary>
    Public Property UID As String
    Public Property UIDHEX As Byte()
    Public Property TagId As UInteger
    Public Property AntId As UInteger
    Public Property DSFID As Byte
    Public Property AipId As UInteger
End Class

Public Class RFIDStatusEventArgs
    Inherits EventArgs
    Property Txt As String
    'Property CurrentDrv As String
    Property UID As String
    Property ConnStringList As List(Of Dictionary(Of String, String))



End Class
