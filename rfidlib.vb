Imports System.Collections.Generic
Imports System.Text
Imports System.Runtime.InteropServices

Namespace RFIDLIB
    Public Delegate Sub RFIDLIB_EVENT_CALLBACK(wparam As UIntPtr, lparam As UIntPtr)
    Class rfidlib_def
        ' Air protocol id
        Public Const RFID_APL_UNKNOWN_ID As Integer = 0
        Public Const RFID_APL_ISO15693_ID As Integer = 1
        Public Const RFID_APL_ISO14443A_ID As Integer = 2
        ' Tag type id
        Public Const RFID_UNKNOWN_PICC_ID As Integer = 0
        Public Const RFID_ISO15693_PICC_ICODE_SLI_ID As Integer = 1
        Public Const RFID_ISO15693_PICC_TI_HFI_PLUS_ID As Integer = 2
        Public Const RFID_ISO15693_PICC_ST_M24LRXX_ID As Integer = 3
        ' ST M24 serial 
        Public Const RFID_ISO15693_PICC_FUJ_MB89R118C_ID As Integer = 4
        Public Const RFID_ISO15693_PICC_ST_M24LR64_ID As Integer = 5
        Public Const RFID_ISO15693_PICC_ST_M24LR16E_ID As Integer = 6
        Public Const RFID_ISO15693_PICC_ICODE_SLIX_ID As Integer = 7
        Public Const RFID_ISO15693_PICC_TIHFI_STANDARD_ID As Integer = 8
        Public Const RFID_ISO15693_PICC_TIHFI_PRO_ID As Integer = 9


        'Inventory type 
        Public Const AI_TYPE_NEW As Integer = 1
        ' new antenna inventory  (reset RF power)
        Public Const AI_TYPE_CONTINUE As Integer = 2
        ' continue antenna inventory ;

        ' Move position 
        Public Const RFID_NO_SEEK As Integer = 0
        ' No seeking 
        Public Const RFID_SEEK_FIRST As Integer = 1
        ' Seek first
        Public Const RFID_SEEK_NEXT As Integer = 2
        ' Seek next 
        Public Const RFID_SEEK_LAST As Integer = 3
        ' Seek last
        '
        '        usb enum information
        '        

        Public Const HID_ENUM_INF_TYPE_SERIALNUM As Integer = 1
        Public Const HID_ENUM_INF_TYPE_DRIVERPATH As Integer = 2




        '
        '        * Open connection string 
        '        

        Public Const CONNSTR_NAME_RDTYPE As String = "RDType"
        Public Const CONNSTR_NAME_COMMTYPE As String = "CommType"

        Public Const CONNSTR_NAME_COMMTYPE_COM As String = "COM"
        Public Const CONNSTR_NAME_COMMTYPE_USB As String = "USB"
        Public Const CONNSTR_NAME_COMMTYPE_NET As String = "NET"

        'HID Param
        Public Const CONNSTR_NAME_HIDADDRMODE As String = "AddrMode"
        Public Const CONNSTR_NAME_HIDSERNUM As String = "SerNum"
        'COM Param
        Public Const CONNSTR_NAME_COMNAME As String = "COMName"
        Public Const CONNSTR_NAME_COMBARUD As String = "BaudRate"
        Public Const CONNSTR_NAME_COMFRAME As String = "Frame"
        Public Const CONNSTR_NAME_BUSADDR As String = "BusAddr"
        'TCP,UDP
        Public Const CONNSTR_NAME_REMOTEIP As String = "RemoteIP"
        Public Const CONNSTR_NAME_REMOTEPORT As String = "RemotePort"
        Public Const CONNSTR_NAME_LOCALIP As String = "LocalIP"



        '
        '* Get loaded reader driver option 
        '

        Public Const LOADED_RDRDVR_OPT_CATALOG As String = "CATALOG"
        Public Const LOADED_RDRDVR_OPT_NAME As String = "NAME"
        Public Const LOADED_RDRDVR_OPT_ID As String = "ID"
        Public Const LOADED_RDRDVR_OPT_COMMTYPESUPPORTED As String = "COMM_TYPE_SUPPORTED"

        '
        '* Reader driver type
        '

        Public Const RDRDVR_TYPE_READER As String = "Reader"
        ' general reader
        Public Const RDRDVR_TYPE_MTGATE As String = "MTGate"
        ' meeting gate
        Public Const RDRDVR_TYPE_LSGATE As String = "LSGate"
        ' Library secure gate

        ' supported product type 


        Public Const COMMTYPE_COM_EN As Byte = &H1
        Public Const COMMTYPE_USB_EN As Byte = &H2
        Public Const COMMTYPE_NET_EN As Byte = &H4


    End Class
    Class rfidlib_aip_iso15693
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_GetLibVersion(buf As StringBuilder, nSize As UInt32) As UInt32
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_CreateInvenParam(hInvenParamSpecList As UIntPtr, AntennaID As Byte, en_afi As Byte, afi As Byte, slot_type As Byte) As UIntPtr
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_ParseTagDataReport(hTagReport As UIntPtr, ByRef aip_id As UInt32, ByRef tag_id As UInt32, ByRef ant_id As UInt32, ByRef dsfid As Byte, uid As Byte()) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_Connect(hr As UIntPtr, tagType As UInt32, address_mode As Byte, uid As Byte(), ByRef ht As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_ReadSingleBlock(hr As UIntPtr, ht As UIntPtr, readSecSta As Byte, blkAddr As UInt32, bufBlockDat As Byte(), nSize As UInt32, _
   ByRef bytesBlkDatRead As UInt32) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_WriteSingleBlock(hr As UIntPtr, ht As UIntPtr, blkAddr As UInt32, newBlkData As Byte(), bytesToWrite As UInt32) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_LockBlock(hr As UIntPtr, ht As UIntPtr, blkAddr As UInt32) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_ReadMultiBlocks(hr As UIntPtr, ht As UIntPtr, readSecSta As Byte, blkAddr As UInt32, numOfBlksToRead As UInt32, ByRef numOfBlksRead As UInt32, _
   bufBlocks As Byte(), nSize As UInt32, ByRef bytesBlkDatRead As UInt32) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_WriteMultipleBlocks(hr As UIntPtr, ht As UIntPtr, blkAddr As UInt32, numOfBlks As UInt32, newBlksData As Byte(), bytesToWrite As UInt32) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_WriteAFI(hr As UIntPtr, ht As UIntPtr, afi As Byte) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_LockAFI(hr As UIntPtr, ht As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_WriteDSFID(hr As UIntPtr, ht As UIntPtr, dsfid As Byte) As Integer
        End Function
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_LockDSFID(hr As UIntPtr, ht As UIntPtr) As Integer
        End Function
        ' out: tag uid 
        ' out:DSFID 
        ' out:AFI 
        ' out: block size 
        ' out:number of total blocks 
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_GetSystemInfo(hr As UIntPtr, ht As UIntPtr, uid As Byte(), ByRef dsfid As Byte, ByRef afi As Byte, ByRef blkSize As UInt32, _
   ByRef numOfBloks As UInt32, ByRef icRef As Byte) As Integer
            ' out:ic reference 
        End Function
        ' in: size of the buffer 
        <DllImport("rfidlib_aip_iso15693.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ISO15693_GetBlockSecStatus(hr As UIntPtr, ht As UIntPtr, blkAddr As UInt32, numOfBlks As UInt32, bufBlkSecs As Byte(), nSize As UInt32, _
   ByRef bytesSecRead As UInt32) As Integer
            'out:number of block status byte copied
        End Function


    End Class
    Class rfidlib_reader

        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_GetLibVersion(buf As StringBuilder, nSize As UInt32) As UInt32
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_LoadReaderDrivers(drvpath As String) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_GetLoadedReaderDriverCount() As UInt32
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_GetLoadedReaderDriverOpt(idx As UInt32, [option] As String, valueBuffer As StringBuilder, ByRef nSize As UInt32) As Integer
        End Function

        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function HID_Enum(DevId As String) As UInt32
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function HID_GetEnumItem(indx As UInt32, infType As Byte, infBuffer As StringBuilder, ByRef nSize As UInt32) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function COMPort_Enum() As UInt32
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function COMPort_GetEnumItem(idx As UInt32, nameBuf As StringBuilder, nSize As UInt32) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_Open(connStr As String, ByRef hrOut As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_Close(hr As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_CreateInvenParamSpecList() As UIntPtr
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_TagInventory(hr As UIntPtr, AIType As Byte, AntennaCoun As Byte, AntennaIDs As Byte(), InvenParamSpecList As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_GetTagDataReportCount(hr As UIntPtr) As UInt32
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_GetTagDataReport(hr As UIntPtr, seek As Byte) As UIntPtr
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_EnableAsyncTagReportOutput(hr As UIntPtr, type As Byte, msg As UInt32, hwnd As UIntPtr, <[In]()> cb As RFIDLIB_EVENT_CALLBACK) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_DisableAsyncTagReportOutput(hr As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_TagDisconnect(hr As UIntPtr, hTag As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_SetAcessAntenna(hr As UIntPtr, AntennaID As Byte) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_OpenRFTransmitter(hr As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_CloseRFTransmitter(hr As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_SetCommuImmeTimeout(hr As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_ResetCommuImmeTimeout(hr As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_GetAntennaInterfaceCount(hr As UIntPtr) As UInt32
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_GetReaderInfor(hr As UIntPtr, Type As Byte, buffer As StringBuilder, ByRef nSize As UInt32) As Integer
        End Function

        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_ExeSpecialControlCmd(hr As UIntPtr, cmd As String, parameters As String, result As StringBuilder, ByRef nSize As UInt32) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_ConfigBlockWrite(hr As UIntPtr, cfgno As UInt32, cfgdata As Byte(), nSize As UInt32, mask As UInt32) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_ConfigBlockRead(hr As UIntPtr, cfgno As UInt32, cfgbuff As Byte(), nSize As UInt32) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_ConfigBlockSave(hr As UIntPtr, cfgno As UInt32) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_CreateRS485Node(hr As UIntPtr, busAddr As UInt32, ByRef ohrRS485Node As UIntPtr) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_GetSupportedAirInterfaceProtocol(hr As UIntPtr, index As UInt32, ByRef AIPType As UInt32) As Integer
        End Function
        <DllImport("rfidlib_reader.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function RDR_GetAirInterfaceProtName(hr As UIntPtr, AIPType As UInt32, namebuf As StringBuilder, nSize As UInt32) As Integer
        End Function

    End Class
    Class rfidlib_data_node
        <DllImport("rfidlib_data_node.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function DNODE_GetLibVersion(buf As StringBuilder, nSize As UInt32) As UInt32
        End Function
        <DllImport("rfidlib_data_node.dll", CharSet:=CharSet.Ansi, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function DNODE_Destroy(dn As UIntPtr) As Integer
        End Function
    End Class
End Namespace