﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form2))
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.DrvBtn = New System.Windows.Forms.ToolStripSplitButton()
        Me.StatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.resetLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TabsContent = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.my_value = New System.Windows.Forms.Label()
        Me.Label_my = New System.Windows.Forms.Label()
        Me.count_value = New System.Windows.Forms.Label()
        Me.sum_value = New System.Windows.Forms.Label()
        Me.md_value = New System.Windows.Forms.Label()
        Me.mo_value = New System.Windows.Forms.Label()
        Me.Label_count = New System.Windows.Forms.Label()
        Me.Label_sum = New System.Windows.Forms.Label()
        Me.Label_md = New System.Windows.Forms.Label()
        Me.Label_mo = New System.Windows.Forms.Label()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Rfid1 = New RFIDLIB.RfidControl()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.Enabled = False
        Me.TextBox1.Location = New System.Drawing.Point(13, 10)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(267, 21)
        Me.TextBox1.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Column1"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 120
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Column2"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Column3"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Column4"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Column5"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Column6"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(799, 58)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(11, 12)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "0"
        Me.Label1.Visible = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DrvBtn, Me.StatusLabel1, Me.resetLabel, Me.TabsContent})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 539)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(784, 23)
        Me.StatusStrip1.TabIndex = 7
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'DrvBtn
        '
        Me.DrvBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.DrvBtn.DropDownButtonWidth = 40
        Me.DrvBtn.Image = CType(resources.GetObject("DrvBtn.Image"), System.Drawing.Image)
        Me.DrvBtn.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.DrvBtn.Name = "DrvBtn"
        Me.DrvBtn.Size = New System.Drawing.Size(91, 21)
        Me.DrvBtn.Text = "RD100"
        '
        'StatusLabel1
        '
        Me.StatusLabel1.Name = "StatusLabel1"
        Me.StatusLabel1.Size = New System.Drawing.Size(56, 18)
        Me.StatusLabel1.Text = "刷新设备"
        '
        'resetLabel
        '
        Me.resetLabel.Margin = New System.Windows.Forms.Padding(60, 3, 0, 2)
        Me.resetLabel.Name = "resetLabel"
        Me.resetLabel.Size = New System.Drawing.Size(80, 18)
        Me.resetLabel.Text = "设备没声音？"
        '
        'TabsContent
        '
        Me.TabsContent.Name = "TabsContent"
        Me.TabsContent.Size = New System.Drawing.Size(482, 18)
        Me.TabsContent.Spring = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label2.Location = New System.Drawing.Point(150, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(0, 19)
        Me.Label2.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("宋体", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label3.Location = New System.Drawing.Point(313, 51)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(0, 19)
        Me.Label3.TabIndex = 9
        '
        'Button1
        '
        Me.Button1.Enabled = False
        Me.Button1.Location = New System.Drawing.Point(13, 47)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(65, 23)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "切换产品"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(716, 11)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LinkLabel1.Size = New System.Drawing.Size(29, 12)
        Me.LinkLabel1.TabIndex = 11
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "更多"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 251)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowTemplate.Height = 23
        Me.DataGridView1.Size = New System.Drawing.Size(784, 288)
        Me.DataGridView1.TabIndex = 12
        '
        'Column1
        '
        Me.Column1.HeaderText = "芯片ID"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.HeaderText = "序列号"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.HeaderText = "工单号"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.HeaderText = "物品名称"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.HeaderText = "扫描时间"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.HeaderText = "网络处理状态"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(750, 36)
        Me.Button2.Name = "Button2"
        Me.Button2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Button2.Size = New System.Drawing.Size(27, 23)
        Me.Button2.TabIndex = 13
        Me.Button2.Text = "..."
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.my_value)
        Me.Panel1.Controls.Add(Me.Label_my)
        Me.Panel1.Controls.Add(Me.count_value)
        Me.Panel1.Controls.Add(Me.sum_value)
        Me.Panel1.Controls.Add(Me.md_value)
        Me.Panel1.Controls.Add(Me.mo_value)
        Me.Panel1.Controls.Add(Me.Label_count)
        Me.Panel1.Controls.Add(Me.Label_sum)
        Me.Panel1.Controls.Add(Me.Label_md)
        Me.Panel1.Controls.Add(Me.Label_mo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 205)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(784, 46)
        Me.Panel1.TabIndex = 14
        '
        'my_value
        '
        Me.my_value.AutoSize = True
        Me.my_value.Location = New System.Drawing.Point(713, 13)
        Me.my_value.Name = "my_value"
        Me.my_value.Size = New System.Drawing.Size(0, 12)
        Me.my_value.TabIndex = 9
        '
        'Label_my
        '
        Me.Label_my.AutoSize = True
        Me.Label_my.Location = New System.Drawing.Point(641, 13)
        Me.Label_my.Name = "Label_my"
        Me.Label_my.Size = New System.Drawing.Size(77, 12)
        Me.Label_my.TabIndex = 8
        Me.Label_my.Text = "此处扫描量："
        '
        'count_value
        '
        Me.count_value.AutoSize = True
        Me.count_value.Location = New System.Drawing.Point(534, 14)
        Me.count_value.Name = "count_value"
        Me.count_value.Size = New System.Drawing.Size(0, 12)
        Me.count_value.TabIndex = 7
        '
        'sum_value
        '
        Me.sum_value.AutoSize = True
        Me.sum_value.Location = New System.Drawing.Point(404, 14)
        Me.sum_value.Name = "sum_value"
        Me.sum_value.Size = New System.Drawing.Size(0, 12)
        Me.sum_value.TabIndex = 6
        '
        'md_value
        '
        Me.md_value.AutoSize = True
        Me.md_value.Location = New System.Drawing.Point(220, 14)
        Me.md_value.Name = "md_value"
        Me.md_value.Size = New System.Drawing.Size(0, 12)
        Me.md_value.TabIndex = 5
        '
        'mo_value
        '
        Me.mo_value.AutoSize = True
        Me.mo_value.Location = New System.Drawing.Point(66, 14)
        Me.mo_value.Name = "mo_value"
        Me.mo_value.Size = New System.Drawing.Size(0, 12)
        Me.mo_value.TabIndex = 4
        '
        'Label_count
        '
        Me.Label_count.AutoSize = True
        Me.Label_count.Location = New System.Drawing.Point(475, 14)
        Me.Label_count.Name = "Label_count"
        Me.Label_count.Size = New System.Drawing.Size(53, 12)
        Me.Label_count.TabIndex = 3
        Me.Label_count.Text = "投入量："
        '
        'Label_sum
        '
        Me.Label_sum.AutoSize = True
        Me.Label_sum.Location = New System.Drawing.Point(344, 14)
        Me.Label_sum.Name = "Label_sum"
        Me.Label_sum.Size = New System.Drawing.Size(53, 12)
        Me.Label_sum.TabIndex = 2
        Me.Label_sum.Text = "订单量："
        '
        'Label_md
        '
        Me.Label_md.AutoSize = True
        Me.Label_md.Location = New System.Drawing.Point(148, 14)
        Me.Label_md.Name = "Label_md"
        Me.Label_md.Size = New System.Drawing.Size(65, 12)
        Me.Label_md.TabIndex = 1
        Me.Label_md.Text = "物品名称："
        '
        'Label_mo
        '
        Me.Label_mo.AutoSize = True
        Me.Label_mo.Location = New System.Drawing.Point(6, 14)
        Me.Label_mo.Name = "Label_mo"
        Me.Label_mo.Size = New System.Drawing.Size(53, 12)
        Me.Label_mo.TabIndex = 0
        Me.Label_mo.Text = "工单号："
        '
        'DataGridView2
        '
        Me.DataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView2.Location = New System.Drawing.Point(0, 12)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.RowTemplate.Height = 23
        Me.DataGridView2.Size = New System.Drawing.Size(784, 113)
        Me.DataGridView2.TabIndex = 15
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(287, 8)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 16
        Me.Button3.Text = "查询模式"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label4.Location = New System.Drawing.Point(0, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(89, 12)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "历史扫描记录："
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.TextBox1)
        Me.Panel2.Controls.Add(Me.Button3)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Button2)
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.LinkLabel1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(784, 80)
        Me.Panel2.TabIndex = 18
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.DataGridView2)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(0, 80)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(784, 125)
        Me.Panel3.TabIndex = 19
        '
        'Rfid1
        '
        Me.Rfid1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Rfid1.CausesValidation = False
        Me.Rfid1.Connstr = Nothing
        Me.Rfid1.Location = New System.Drawing.Point(836, 0)
        Me.Rfid1.Name = "Rfid1"
        Me.Rfid1.Size = New System.Drawing.Size(32, 29)
        Me.Rfid1.TabIndex = 5
        Me.Rfid1.TabStop = False
        Me.Rfid1.Visible = False
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 562)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Rfid1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "Form2"
        Me.Text = "Form2"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Rfid1 As RFIDLIB.RfidControl
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents LinkLabel1 As LinkLabel
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Button2 As Button
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Panel1 As Panel
    Friend WithEvents count_value As Label
    Friend WithEvents sum_value As Label
    Friend WithEvents md_value As Label
    Friend WithEvents mo_value As Label
    Friend WithEvents Label_count As Label
    Friend WithEvents Label_sum As Label
    Friend WithEvents Label_md As Label
    Friend WithEvents Label_mo As Label
    Friend WithEvents DataGridView2 As DataGridView
    Friend WithEvents Button3 As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents my_value As Label
    Friend WithEvents Label_my As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents DrvBtn As ToolStripSplitButton
    Friend WithEvents StatusLabel1 As ToolStripStatusLabel
    Friend WithEvents resetLabel As ToolStripStatusLabel
    Friend WithEvents TabsContent As ToolStripStatusLabel
End Class
