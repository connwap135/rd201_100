﻿Imports System.IO
Imports System.Net
Imports System.Text

Public Class HDialog
    Private parentId As Integer
    Private data As List(Of WorkOrder)
    Private dt As DataTable

    Public Sub New(parentId As Integer)
        InitializeComponent()
        Me.parentId = parentId
    End Sub

    Private Sub HDialog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim client As New WebClient()
        client.QueryString.Add("pid", parentId)
        Dim IOStream = client.OpenRead(New Uri(Urls.API_ORDER))
        Using stream = New StreamReader(IOStream, Encoding.UTF8)
            Dim json As String = stream.ReadToEnd
            Console.WriteLine(json)

            data = JsonClass.DeserializeJsonToObject(Of List(Of WorkOrder))(json)
            Console.WriteLine(data.Count)

            TreeView1.Nodes.Clear()
            For Each wo In data
                TreeView1.Nodes.Add(wo.moName)
            Next

        End Using
    End Sub

    Private Sub TreeView1_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles TreeView1.AfterSelect
        LoadWoData(e.Node.Text)
        DataGridView1.Select()
    End Sub
    Private Sub LoadWoData(ByVal v As String)
        Dim list = data.FirstOrDefault(Function(x) x.moName.Equals(v))
        Dim result = New List(Of Sx)
        For Each v In list.modelList
            Dim item As New Sx With {
                .工单 = list.moName,
                .产品 = v
            }
            result.Add(item)
        Next
        DataGridView1.DataSource = result
        dt = GetDgvToTable(DataGridView1)
        DataGridView1.DataSource = dt
    End Sub
    Class Sx
        Property 工单 As String
        Property 产品 As String
    End Class

    Private Sub DataGridView1_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellEnter
        If e.RowIndex >= 0 Then
            DataGridView1.Rows(e.RowIndex).Selected = True
        End If
    End Sub
    Private Sub CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        DialogResult = DialogResult.OK
        Dim frm As Form2 = Me.Owner
        frm.mo = DataGridView1.Rows(e.RowIndex).Cells(0).Value.ToString
        frm.md = DataGridView1.Rows(e.RowIndex).Cells(1).Value.ToString
        Me.Close()
    End Sub

    Private Sub CellPreviewKeyDown(sender As Object, e As PreviewKeyDownEventArgs) Handles DataGridView1.PreviewKeyDown
        Dim dgv = TryCast(sender, DataGridView)
        Select Case e.KeyData
            Case Keys.Enter
                Dim eb As New DataGridViewCellEventArgs(dgv.CurrentCell.ColumnIndex, dgv.CurrentCell.RowIndex)
                CellDoubleClick(sender, eb)
        End Select
    End Sub
    Private Sub DataGridView1_RowPostPaint(sender As Object, e As DataGridViewRowPostPaintEventArgs) Handles DataGridView1.RowPostPaint
        Dim dgv = TryCast(sender, DataGridView)
        Dim SolidBrush = New SolidBrush(dgv.RowHeadersDefaultCellStyle.ForeColor)
        If dgv.RowHeadersVisible Then
            Dim rec As New Rectangle(e.RowBounds.Left, e.RowBounds.Top, dgv.RowHeadersWidth, e.RowBounds.Height)
            Dim StrFormat As New StringFormat
            StrFormat.LineAlignment = StringAlignment.Center
            StrFormat.Alignment = StringAlignment.Far
            e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, SolidBrush, rec, StrFormat)
        End If
    End Sub
    Public Function GetDgvToTable(dgv As DataGridView) As DataTable
        'dgv.Sort(dgv.Columns(0), System.ComponentModel.ListSortDirection.Descending)
        Dim dt As DataTable = New DataTable
        For count As Integer = 0 To dgv.Columns.Count - 1
            Dim dc As DataColumn = New DataColumn(dgv.Columns(count).Name.ToString)
            dt.Columns.Add(dc)
        Next
        For count As Integer = 0 To dgv.Rows.Count - 1
            Dim dr As DataRow = dt.NewRow
            For countsub As Integer = 0 To dgv.Columns.Count - 1
                dr(countsub) = Convert.ToString(dgv.Rows(count).Cells(countsub).Value)
            Next
            dt.Rows.Add(dr)
        Next
        Return dt
    End Function
    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged

        dt.DefaultView.RowFilter = "(工单 like '%" & TextBox1.Text & "%') or (产品 like '%" & TextBox1.Text & "%')"

    End Sub
End Class