﻿Imports System.IO
Imports Newtonsoft.Json
Public Class JsonClass
#Region "JSON序列和反序列"
    Public Shared Function DeserializeJsonToObject(Of T As Class)(ByVal json As String) As T
        Try
            Dim serializer As New JsonSerializer
            Dim reader As New StringReader(json)
            Return TryCast(serializer.Deserialize(New JsonTextReader(reader), GetType(T)), T)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function SerializeObjectToJson(ByVal obj As Object) As String
        Dim json As String = String.Empty
        Try
            Dim jsSetting As JsonSerializerSettings = New JsonSerializerSettings
            jsSetting.NullValueHandling = NullValueHandling.Ignore
            json = JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.None, jsSetting)
        Catch ex As Exception
        End Try
        Return json
    End Function
#End Region

End Class
