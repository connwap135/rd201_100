﻿Public Class Urls
    Private Shared ReadOnly HTTP As String = "http://"
    Private Shared ReadOnly HTTPS As String = "https://"
    Private Shared ReadOnly URL_SPLITTER As String = "/"
    Private Shared ReadOnly URL_UNDERLINE As String = "_"
    Private Shared ReadOnly HOST As String = "192.168.1.240:5000"

    Public Shared ReadOnly HOST_IP As String = "192.168.1.240"
    Public Shared ReadOnly URL_API_HOST As String = HTTP + HOST + URL_SPLITTER
    Public Shared API_PSAPI As String = URL_API_HOST + "api/psapi"
    Public Shared API_PDAPI As String = URL_API_HOST + "API/PDAPI"
    Public Shared API_LOGIN As String = URL_API_HOST + "ES/GET"
    Public Shared API_ORDER As String = URL_API_HOST + "WO/GET"
    Public Shared API_PSDETAILS As String = URL_API_HOST + "ps/GetDetails"
    ''' <summary>
    ''' 扫描记录
    ''' </summary>
    Public Shared API_SCAN_RECORD As String = URL_API_HOST + "PD/GET"
    Public Shared API_SOFT_UPDATE As String = "http://192.168.1.238:88/wxproject/zhifuappxml.aspx?id=8"


End Class
