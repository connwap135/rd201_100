﻿Imports System.Globalization
Imports System.Diagnostics
Imports Microsoft.Win32

Public Class UpdateForm

    Private _timer As Timers.Timer
    Public Sub New(Optional ByVal remindLater As Boolean = False)
        If Not remindLater Then
            InitializeComponent()
            Text = AutoUpdater.DialogTitle
            labelUpdate.Text = String.Format("发现一个新版本的{0}!", AutoUpdater.DialogTitle)
            labelDescription.Text = String.Format("发现{0}有个新版本为 {1}. 当前版本为 {2}. 建议更新到最新版本,是否继续?", AutoUpdater.AppTitle, AutoUpdater.CurrentVersion, AutoUpdater.InstalledVersion)
        End If
    End Sub

    Private Sub UpdateForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        webBrowser.Navigate(AutoUpdater.ChangeLogURL)
    End Sub

    Private Sub buttonUpdate_Click(sender As System.Object, e As System.EventArgs) Handles buttonUpdate.Click
        If AutoUpdater.OpenDownloadPage Then
            Dim processStartInfo = New ProcessStartInfo(AutoUpdater.DownloadURL)
            Process.Start(processStartInfo)
        Else
            Dim downloadDialog = New DownloadUpdateDialog(AutoUpdater.DownloadURL)
            Try
                downloadDialog.ShowDialog()
            Catch generatedExceptionName As System.Reflection.TargetInvocationException
            End Try
        End If
    End Sub

    Private Sub buttonSkip_Click(sender As System.Object, e As System.EventArgs) Handles buttonSkip.Click
        Dim updateKey As RegistryKey = Registry.CurrentUser.CreateSubKey(AutoUpdater.RegistryLocation)
        If updateKey IsNot Nothing Then
            updateKey.SetValue("version", AutoUpdater.CurrentVersion.ToString())
            updateKey.SetValue("skip", 1)
            updateKey.Close()
        End If
    End Sub

    Public Sub SetTimer(remindLater As DateTime)
        Dim timeSpan As TimeSpan = remindLater - DateTime.Now
        _timer = New System.Timers.Timer() With { _
         .Interval = CInt(Math.Truncate(timeSpan.TotalMilliseconds)) _
        }
        AddHandler _timer.Elapsed, AddressOf TimerElapsed
        _timer.Start()
    End Sub

    Private Sub TimerElapsed(sender As Object, e As System.Timers.ElapsedEventArgs)
        _timer.Stop()
        AutoUpdater.Start(0)
    End Sub

    Private Sub buttonRemindLater_Click(sender As System.Object, e As System.EventArgs) Handles buttonRemindLater.Click
        If AutoUpdater.LetUserSelectRemindLater Then
            Dim remindLaterForm = New RemindLaterForm()

            Dim dialogResult__1 = remindLaterForm.ShowDialog()

            If dialogResult__1.Equals(Windows.Forms.DialogResult.OK) Then
                AutoUpdater.RemindLaterTimeSpan = remindLaterForm.RemindLaterFormat
                AutoUpdater.RemindLaterAt = remindLaterForm.RemindLaterAt
            ElseIf dialogResult__1.Equals(Windows.Forms.DialogResult.Abort) Then
                Dim downloadDialog = New DownloadUpdateDialog(AutoUpdater.DownloadURL)

                Try
                    downloadDialog.ShowDialog()
                Catch generatedExceptionName As System.Reflection.TargetInvocationException
                    Return
                End Try
                Return
            Else
                DialogResult = Windows.Forms.DialogResult.None
                Return
            End If
        End If

        Dim updateKey As RegistryKey = Registry.CurrentUser.CreateSubKey(AutoUpdater.RegistryLocation)
        If updateKey IsNot Nothing Then
            updateKey.SetValue("version", AutoUpdater.CurrentVersion)
            updateKey.SetValue("skip", 0)
            Dim remindLaterDateTime As DateTime = DateTime.Now
            Select Case AutoUpdater.RemindLaterTimeSpan
                Case AutoUpdater.RemindLaterFormat.Days
                    remindLaterDateTime = DateTime.Now + TimeSpan.FromDays(AutoUpdater.RemindLaterAt)
                    Exit Select
                Case AutoUpdater.RemindLaterFormat.Hours
                    remindLaterDateTime = DateTime.Now + TimeSpan.FromHours(AutoUpdater.RemindLaterAt)
                    Exit Select
                Case AutoUpdater.RemindLaterFormat.Minutes
                    remindLaterDateTime = DateTime.Now + TimeSpan.FromMinutes(AutoUpdater.RemindLaterAt)
                    Exit Select

            End Select
            updateKey.SetValue("remindlater", remindLaterDateTime.ToString(CultureInfo.CreateSpecificCulture("en-US")))
            SetTimer(remindLaterDateTime)
            updateKey.Close()
        End If
    End Sub
End Class