﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RemindLaterForm
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.buttonOK = New System.Windows.Forms.Button()
        Me.comboBoxRemindLater = New System.Windows.Forms.ComboBox()
        Me.radioButtonNo = New System.Windows.Forms.RadioButton()
        Me.radioButtonYes = New System.Windows.Forms.RadioButton()
        Me.labelDescription = New System.Windows.Forms.Label()
        Me.labelTitle = New System.Windows.Forms.Label()
        Me.pictureBoxIcon = New System.Windows.Forms.PictureBox()
        CType(Me.pictureBoxIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'buttonOK
        '
        Me.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.buttonOK.Image = Global.AutoUpdaterDotNET.Resources.clock_go
        Me.buttonOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.buttonOK.Location = New System.Drawing.Point(346, 164)
        Me.buttonOK.Name = "buttonOK"
        Me.buttonOK.Size = New System.Drawing.Size(74, 32)
        Me.buttonOK.TabIndex = 13
        Me.buttonOK.Text = "OK"
        Me.buttonOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.buttonOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.buttonOK.UseVisualStyleBackColor = True
        '
        'comboBoxRemindLater
        '
        Me.comboBoxRemindLater.FormattingEnabled = True
        Me.comboBoxRemindLater.Items.AddRange(New Object() {"After 30 minutes", "After 12 hours", "After 1 day", "After 2 days", "After 4 days", "After 8 days", "After 10 days"})
        Me.comboBoxRemindLater.Location = New System.Drawing.Point(288, 114)
        Me.comboBoxRemindLater.Name = "comboBoxRemindLater"
        Me.comboBoxRemindLater.Size = New System.Drawing.Size(132, 20)
        Me.comboBoxRemindLater.TabIndex = 12
        '
        'radioButtonNo
        '
        Me.radioButtonNo.AutoSize = True
        Me.radioButtonNo.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.radioButtonNo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radioButtonNo.Location = New System.Drawing.Point(80, 139)
        Me.radioButtonNo.Name = "radioButtonNo"
        Me.radioButtonNo.Size = New System.Drawing.Size(268, 19)
        Me.radioButtonNo.TabIndex = 11
        Me.radioButtonNo.Text = "No, download updates now (recommended)"
        Me.radioButtonNo.UseVisualStyleBackColor = True
        '
        'radioButtonYes
        '
        Me.radioButtonYes.AutoSize = True
        Me.radioButtonYes.Checked = True
        Me.radioButtonYes.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radioButtonYes.Location = New System.Drawing.Point(80, 114)
        Me.radioButtonYes.Name = "radioButtonYes"
        Me.radioButtonYes.Size = New System.Drawing.Size(197, 16)
        Me.radioButtonYes.TabIndex = 10
        Me.radioButtonYes.TabStop = True
        Me.radioButtonYes.Text = "Yes, please remind me later :"
        Me.radioButtonYes.UseVisualStyleBackColor = True
        '
        'labelDescription
        '
        Me.labelDescription.AutoSize = True
        Me.labelDescription.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.labelDescription.Location = New System.Drawing.Point(77, 37)
        Me.labelDescription.MaximumSize = New System.Drawing.Size(340, 0)
        Me.labelDescription.Name = "labelDescription"
        Me.labelDescription.Size = New System.Drawing.Size(323, 36)
        Me.labelDescription.TabIndex = 9
        Me.labelDescription.Text = "You should download updates now. This only takes few minutes depending on your in" & _
    "ternet connection and ensures you have latest version of the application."
        '
        'labelTitle
        '
        Me.labelTitle.AutoSize = True
        Me.labelTitle.Font = New System.Drawing.Font("Segoe UI", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.labelTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.labelTitle.Location = New System.Drawing.Point(76, 9)
        Me.labelTitle.Name = "labelTitle"
        Me.labelTitle.Size = New System.Drawing.Size(280, 19)
        Me.labelTitle.TabIndex = 7
        Me.labelTitle.Text = "Do you want to download updates later?"
        '
        'pictureBoxIcon
        '
        Me.pictureBoxIcon.Image = Global.AutoUpdaterDotNET.Resources.clock_go_32
        Me.pictureBoxIcon.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.pictureBoxIcon.Location = New System.Drawing.Point(9, 9)
        Me.pictureBoxIcon.Name = "pictureBoxIcon"
        Me.pictureBoxIcon.Size = New System.Drawing.Size(61, 48)
        Me.pictureBoxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pictureBoxIcon.TabIndex = 8
        Me.pictureBoxIcon.TabStop = False
        '
        'RemindLaterForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(446, 211)
        Me.Controls.Add(Me.buttonOK)
        Me.Controls.Add(Me.comboBoxRemindLater)
        Me.Controls.Add(Me.radioButtonNo)
        Me.Controls.Add(Me.radioButtonYes)
        Me.Controls.Add(Me.labelDescription)
        Me.Controls.Add(Me.pictureBoxIcon)
        Me.Controls.Add(Me.labelTitle)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "RemindLaterForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Remind me later for update"
        CType(Me.pictureBoxIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents buttonOK As System.Windows.Forms.Button
    Private WithEvents comboBoxRemindLater As System.Windows.Forms.ComboBox
    Private WithEvents radioButtonNo As System.Windows.Forms.RadioButton
    Private WithEvents radioButtonYes As System.Windows.Forms.RadioButton
    Private WithEvents labelDescription As System.Windows.Forms.Label
    Private WithEvents pictureBoxIcon As System.Windows.Forms.PictureBox
    Private WithEvents labelTitle As System.Windows.Forms.Label
End Class
