﻿Public Class RemindLaterForm
    Public Property RemindLaterAt() As Integer
        Get
            Return m_RemindLaterAt
        End Get
        Private Set(value As Integer)
            m_RemindLaterAt = Value
        End Set
    End Property
    Private m_RemindLaterAt As Integer

    Public Property RemindLaterFormat() As AutoUpdater.RemindLaterFormat
        Get
            Return m_RemindLaterFormat
        End Get
        Private Set(value As AutoUpdater.RemindLaterFormat)
            m_RemindLaterFormat = Value
        End Set
    End Property
    Private m_RemindLaterFormat As AutoUpdater.RemindLaterFormat

    Private Sub RemindLaterForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        comboBoxRemindLater.SelectedIndex = 0
        radioButtonYes.Checked = True
    End Sub

    Private Sub buttonOK_Click(sender As System.Object, e As System.EventArgs) Handles buttonOK.Click
        If radioButtonYes.Checked Then
            Select Case comboBoxRemindLater.SelectedIndex
                Case 0
                    RemindLaterFormat = AutoUpdater.RemindLaterFormat.Minutes
                    RemindLaterAt = 30
                    Exit Select
                Case 1
                    RemindLaterFormat = AutoUpdater.RemindLaterFormat.Hours
                    RemindLaterAt = 12
                    Exit Select
                Case 2
                    RemindLaterFormat = AutoUpdater.RemindLaterFormat.Days
                    RemindLaterAt = 1
                    Exit Select
                Case 3
                    RemindLaterFormat = AutoUpdater.RemindLaterFormat.Days
                    RemindLaterAt = 2
                    Exit Select
                Case 4
                    RemindLaterFormat = AutoUpdater.RemindLaterFormat.Days
                    RemindLaterAt = 4
                    Exit Select
                Case 5
                    RemindLaterFormat = AutoUpdater.RemindLaterFormat.Days
                    RemindLaterAt = 8
                    Exit Select
                Case 6
                    RemindLaterFormat = AutoUpdater.RemindLaterFormat.Days
                    RemindLaterAt = 10
                    Exit Select
            End Select
            DialogResult = Windows.Forms.DialogResult.OK
        Else
            DialogResult = Windows.Forms.DialogResult.Abort
        End If
    End Sub

    Private Sub radioButtonYes_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles radioButtonYes.CheckedChanged
        comboBoxRemindLater.Enabled = radioButtonYes.Checked
    End Sub
End Class