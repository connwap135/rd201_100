﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UpdateForm
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pictureBoxIcon = New System.Windows.Forms.PictureBox()
        Me.labelReleaseNotes = New System.Windows.Forms.Label()
        Me.labelDescription = New System.Windows.Forms.Label()
        Me.labelUpdate = New System.Windows.Forms.Label()
        Me.webBrowser = New System.Windows.Forms.WebBrowser()
        Me.buttonUpdate = New System.Windows.Forms.Button()
        Me.buttonSkip = New System.Windows.Forms.Button()
        Me.buttonRemindLater = New System.Windows.Forms.Button()
        CType(Me.pictureBoxIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pictureBoxIcon
        '
        Me.pictureBoxIcon.Image = Global.AutoUpdaterDotNET.Resources.update
        Me.pictureBoxIcon.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.pictureBoxIcon.Location = New System.Drawing.Point(12, 12)
        Me.pictureBoxIcon.Name = "pictureBoxIcon"
        Me.pictureBoxIcon.Size = New System.Drawing.Size(70, 66)
        Me.pictureBoxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pictureBoxIcon.TabIndex = 16
        Me.pictureBoxIcon.TabStop = False
        '
        'labelReleaseNotes
        '
        Me.labelReleaseNotes.AutoSize = True
        Me.labelReleaseNotes.Font = New System.Drawing.Font("Segoe UI", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.labelReleaseNotes.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.labelReleaseNotes.Location = New System.Drawing.Point(91, 88)
        Me.labelReleaseNotes.Name = "labelReleaseNotes"
        Me.labelReleaseNotes.Size = New System.Drawing.Size(102, 17)
        Me.labelReleaseNotes.TabIndex = 15
        Me.labelReleaseNotes.Text = "Release Notes :"
        '
        'labelDescription
        '
        Me.labelDescription.AutoSize = True
        Me.labelDescription.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.labelDescription.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.labelDescription.Location = New System.Drawing.Point(91, 48)
        Me.labelDescription.MaximumSize = New System.Drawing.Size(550, 0)
        Me.labelDescription.Name = "labelDescription"
        Me.labelDescription.Size = New System.Drawing.Size(480, 15)
        Me.labelDescription.TabIndex = 14
        Me.labelDescription.Text = "{0} {1} is now available. You have version {2} installed. Would you like to downl" & _
    "oad it now?"
        '
        'labelUpdate
        '
        Me.labelUpdate.AutoSize = True
        Me.labelUpdate.Font = New System.Drawing.Font("Segoe UI", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.labelUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.labelUpdate.Location = New System.Drawing.Point(91, 12)
        Me.labelUpdate.MaximumSize = New System.Drawing.Size(560, 0)
        Me.labelUpdate.Name = "labelUpdate"
        Me.labelUpdate.Size = New System.Drawing.Size(227, 19)
        Me.labelUpdate.TabIndex = 13
        Me.labelUpdate.Text = "A new version of {0} is available!"
        '
        'webBrowser
        '
        Me.webBrowser.Location = New System.Drawing.Point(94, 118)
        Me.webBrowser.Margin = New System.Windows.Forms.Padding(2)
        Me.webBrowser.MinimumSize = New System.Drawing.Size(23, 23)
        Me.webBrowser.Name = "webBrowser"
        Me.webBrowser.Size = New System.Drawing.Size(538, 432)
        Me.webBrowser.TabIndex = 9
        '
        'buttonUpdate
        '
        Me.buttonUpdate.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.buttonUpdate.Image = Global.AutoUpdaterDotNET.Resources.download
        Me.buttonUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.buttonUpdate.Location = New System.Drawing.Point(478, 568)
        Me.buttonUpdate.Margin = New System.Windows.Forms.Padding(2)
        Me.buttonUpdate.Name = "buttonUpdate"
        Me.buttonUpdate.Size = New System.Drawing.Size(153, 28)
        Me.buttonUpdate.TabIndex = 11
        Me.buttonUpdate.Text = "更新"
        Me.buttonUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.buttonUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.buttonUpdate.UseVisualStyleBackColor = True
        '
        'buttonSkip
        '
        Me.buttonSkip.DialogResult = System.Windows.Forms.DialogResult.Abort
        Me.buttonSkip.Image = Global.AutoUpdaterDotNET.Resources.hand_point
        Me.buttonSkip.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.buttonSkip.Location = New System.Drawing.Point(95, 568)
        Me.buttonSkip.Margin = New System.Windows.Forms.Padding(2)
        Me.buttonSkip.Name = "buttonSkip"
        Me.buttonSkip.Size = New System.Drawing.Size(153, 28)
        Me.buttonSkip.TabIndex = 10
        Me.buttonSkip.Text = "跳过该版本"
        Me.buttonSkip.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.buttonSkip.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.buttonSkip.UseVisualStyleBackColor = True
        '
        'buttonRemindLater
        '
        Me.buttonRemindLater.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.buttonRemindLater.Image = Global.AutoUpdaterDotNET.Resources.clock_go
        Me.buttonRemindLater.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.buttonRemindLater.Location = New System.Drawing.Point(321, 568)
        Me.buttonRemindLater.Margin = New System.Windows.Forms.Padding(2)
        Me.buttonRemindLater.Name = "buttonRemindLater"
        Me.buttonRemindLater.Size = New System.Drawing.Size(153, 28)
        Me.buttonRemindLater.TabIndex = 12
        Me.buttonRemindLater.Text = "延迟更新"
        Me.buttonRemindLater.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.buttonRemindLater.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.buttonRemindLater.UseVisualStyleBackColor = True
        '
        'UpdateForm
        '
        Me.AcceptButton = Me.buttonUpdate
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(653, 610)
        Me.Controls.Add(Me.pictureBoxIcon)
        Me.Controls.Add(Me.labelReleaseNotes)
        Me.Controls.Add(Me.labelDescription)
        Me.Controls.Add(Me.labelUpdate)
        Me.Controls.Add(Me.webBrowser)
        Me.Controls.Add(Me.buttonUpdate)
        Me.Controls.Add(Me.buttonSkip)
        Me.Controls.Add(Me.buttonRemindLater)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "UpdateForm"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "UpdateForm"
        CType(Me.pictureBoxIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents pictureBoxIcon As System.Windows.Forms.PictureBox
    Private WithEvents labelReleaseNotes As System.Windows.Forms.Label
    Private WithEvents labelDescription As System.Windows.Forms.Label
    Private WithEvents labelUpdate As System.Windows.Forms.Label
    Private WithEvents webBrowser As System.Windows.Forms.WebBrowser
    Private WithEvents buttonUpdate As System.Windows.Forms.Button
    Private WithEvents buttonSkip As System.Windows.Forms.Button
    Private WithEvents buttonRemindLater As System.Windows.Forms.Button
End Class
