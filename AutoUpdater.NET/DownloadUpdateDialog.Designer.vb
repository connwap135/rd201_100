<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DownloadUpdateDialog
    Inherits System.Windows.Forms.Form
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.labelInformation = New System.Windows.Forms.Label()
        Me.progressBar = New System.Windows.Forms.ProgressBar()
        Me.pictureBoxIcon = New System.Windows.Forms.PictureBox()
        CType(Me.pictureBoxIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'labelInformation
        '
        Me.labelInformation.AutoSize = True
        Me.labelInformation.Font = New System.Drawing.Font("Segoe UI", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.labelInformation.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.labelInformation.Location = New System.Drawing.Point(71, 12)
        Me.labelInformation.Name = "labelInformation"
        Me.labelInformation.Size = New System.Drawing.Size(148, 19)
        Me.labelInformation.TabIndex = 5
        Me.labelInformation.Text = "Downloading Update..."
        '
        'progressBar
        '
        Me.progressBar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.progressBar.Location = New System.Drawing.Point(71, 42)
        Me.progressBar.Name = "progressBar"
        Me.progressBar.Size = New System.Drawing.Size(270, 23)
        Me.progressBar.TabIndex = 4
        '
        'pictureBoxIcon
        '
        Me.pictureBoxIcon.Image = Global.AutoUpdaterDotNET.Resources.download_32
        Me.pictureBoxIcon.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.pictureBoxIcon.Location = New System.Drawing.Point(12, 12)
        Me.pictureBoxIcon.Name = "pictureBoxIcon"
        Me.pictureBoxIcon.Size = New System.Drawing.Size(53, 53)
        Me.pictureBoxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pictureBoxIcon.TabIndex = 3
        Me.pictureBoxIcon.TabStop = False
        '
        'DownloadUpdateDialog
        '
        Me.ClientSize = New System.Drawing.Size(356, 86)
        Me.Controls.Add(Me.labelInformation)
        Me.Controls.Add(Me.progressBar)
        Me.Controls.Add(Me.pictureBoxIcon)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "DownloadUpdateDialog"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Software Update"
        CType(Me.pictureBoxIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents labelInformation As System.Windows.Forms.Label
    Private WithEvents progressBar As System.Windows.Forms.ProgressBar
    Private WithEvents pictureBoxIcon As System.Windows.Forms.PictureBox

End Class
